#!/bin/bash

BASE_DIR="SOURCE_CODE_FOLDER"
DESTINATION_BASE_DIR="DOCKER_MANAGER_FOLDER"

cp $BASE_DIR"main/target/main-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase0/phase.jar"

cp $BASE_DIR"infrastructure/target/infrastructure-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"infrastructure/phase.jar"

cp $BASE_DIR"phase1/main/target/main-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase1/phase.jar"

cp $BASE_DIR"phase1/post/target/post-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase1post/phase.jar"

cp $BASE_DIR"phase2a/main/target/main-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase2a/phase.jar"

cp $BASE_DIR"phase2b/main/target/main-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase2b/phase.jar"

cp $BASE_DIR"phase2c/main/target/main-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase2c/phase.jar"

cp $BASE_DIR"phase3/pre/target/pre-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase3pre/phase.jar"

cp $BASE_DIR"phase3/main/target/main-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"phase3/phase.jar"

cp $BASE_DIR"entry-point/target/entry-point-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"entry-point/phase.jar"

cp -r $BASE_DIR"api/target/scalatra-api.war" $DESTINATION_BASE_DIR"webapp/ROOT.war"

cp $BASE_DIR"asplos/target/asplos-0.1-SNAPSHOT-allinone.jar" $DESTINATION_BASE_DIR"test_actor/phase.jar"
cp $BASE_DIR"asplos/src/main/resources/asplos.xml" $DESTINATION_BASE_DIR"test_actor/asplos.xml"

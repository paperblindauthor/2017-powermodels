#!/bin/bash

DOCKER_DIR=""

cd $DOCKER_DIR

./scripts_linux/copy.sh
docker-compose build
./scripts_linux/start_dns.sh

docker-compose up redisinternal redisexternal infrastructure &
sleep 20
docker-compose up phase0 phase1 phase1post &
sleep 20
docker-compose up phase2a &
sleep 20
docker-compose up entrypoint &
sleep 10
docker-compose up webapp

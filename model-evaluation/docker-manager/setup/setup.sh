#!/bin/bash

if [[ -f "./environment.sh" ]]; then
    source environment.sh

    # Generate run.sh
    envsubst < ./setupfiles/run.sh > run.sh
    chmod +x run.sh
    sudo mv run.sh $MARC_DOCKER_COMMAND_HOST

    # Install and setup supervisor
    if [ ! -d /etc/supervisor/conf.d/ ]; then
        sudo apt-get install supervisor -y
    fi
    envsubst < ./setupfiles/supervisor.conf > docker-compose-marc.conf
    sudo mv docker-compose-marc.conf /etc/supervisor/conf.d/
    sudo service supervisor restart

else
    echo "No environment.sh file found."
fi

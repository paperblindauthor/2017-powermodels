#!/bin/bash

cd ${MARC_DOCKER_PATH_HOST}

sudo ifconfig eth0:1 172.17.42.1 netmask 255.255.255.0 up
docker rm dnsdock
docker run -d -v /var/run/docker.sock:/var/run/docker.sock --name dnsdock -p 172.17.42.1:53:53/udp tonistiigi/dnsdock [--opts]

docker-compose up redisinternal redisexternal infrastructure &
sleep 20
docker-compose up phase0 phase1 phase1post &
sleep 20
docker-compose up phase2a phase2b phase2c &
sleep 20
docker-compose up phase3pre entrypoint &
sleep 10
docker-compose up webapp
package it.necst.marc.entrypoint.actors

import akka.actor.ActorRef

/**
 * Created by andrea on 30/10/15.
 */
sealed trait Message

//loadbalancer entrypoint
@SerialVersionUID(1234l) case class UserRequest(xmlString: String,phaseRequested: String,email: String,actor: ActorRef = null) extends Message

case class FreeActor(reference: ActorRef) extends Message


@SerialVersionUID(1234l) case class ComputationSuccess(uri: String) extends Message

@SerialVersionUID(1234l) case class ComputationTerminatedWithErrors(uri:String) extends Message

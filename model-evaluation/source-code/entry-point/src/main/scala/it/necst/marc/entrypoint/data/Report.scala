package it.necst.marc.entrypoint.data

import it.necst.marc.data.AbstractReport

/**
 * Created by andrea on 19/08/15.
 */
case class Report(content:Option[MathStats], error:Option[String]) extends AbstractReport(content, error)

case class MathStats() extends it.necst.marc.data.MathStats


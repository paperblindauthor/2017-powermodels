package it.necst.marc.entrypoint.data.internal

/**
  * Created by andrea on 01/12/15.
  */

object NoFreeActorsException extends Exception

object UnknownConfigurationException extends Exception

object ConfigurationsMapException extends Exception

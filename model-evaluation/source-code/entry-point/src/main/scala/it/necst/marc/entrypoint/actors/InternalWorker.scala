package it.necst.marc.entrypoint.actors

import akka.actor._
import java.net.URI
import com.redis.RedisClient
import it.necst.marc.data.AbstractConfiguration
import it.necst.marc.data.support.{MarcLogger, OptionalTag, ErrorStack, NamesPhases}
import it.necst.marc.entrypoint.computation.ConfigurationParser
import it.necst.marc.entrypoint.data.Report
import it.necst.marc.entrypoint.data.internal.UnknownConfigurationException
import it.necst.marc.infrastructure.support._
import it.necst.marc.infrastructure.actors.messages._
import rapture.json._
import rapture.json.jsonBackends.jawn._

import scala.concurrent.duration._
import scala.xml._
/**
 * Created by andrea on 19/08/15.
 */
object InternalWorker {

  def props(phasesLoadBalancers: Map[NamesPhases.Value,String],
            timeoutJobCFGReceiveState: Int,
            timeoutWaitResultState: Int,
            beforePhasesList: List[NamesPhases.Value],
            configurationsMap: Map[NamesPhases.Value,List[NamesPhases.Value]],
            mapLoadBalancer: Map[NamesPhases.Value,ActorSelection]):Props = Props(new InternalWorker(phasesLoadBalancers,
                                                                        timeoutJobCFGReceiveState,
                                                                        timeoutWaitResultState,
                                                                        beforePhasesList,
                                                                        configurationsMap,
                                                                        mapLoadBalancer))

}

class InternalWorker(val phasesLoadBalancers: Map[NamesPhases.Value,String],
                     val timeoutJobCFGReceiveState: Int,
                     val timeoutWaitResultState: Int,
                     val beforePhasesList: List[NamesPhases.Value],
                     val configurationsMap: Map[NamesPhases.Value,List[NamesPhases.Value]],
                     val mapLoadBalancer: Map[NamesPhases.Value,ActorSelection]) extends Actor{

  private val logger = MarcLogger(getClass.getName)


  private var waitConfirmationJobCFG = 0
  private var currentJob: Job = _
  private var currentComputationUnit: String = _
  private var currentDataSet: String = _
  private var currentDictionary: it.necst.marc.phase1.post.data.ResultDictionary = _
  private var currentUri: URI = _
  private var currentPhase: NamesPhases.Value = _
  private var loadBalancerRef: ActorRef = _
  private var redis: RedisClient = _
  private var loadBalancerExternalRedis: ActorSelection = _
  private var currentConfigurations: Map[NamesPhases.Value,Option[AbstractConfiguration]] = _
  private var currentEmail: String = _
  private var error: Option[String] = None
  private var actorCaller: ActorRef = _
  private var waitingAnswers: Int = 0

  override def preStart(): Unit ={
    //TODO remove this
    /*currentConfigurations = Map()
    def createActorRef(toBeProcessed: Map[NamesPhases.Value,String],temporaryMap: Map[NamesPhases.Value,ActorSelection]):Map[NamesPhases.Value,ActorSelection] = {
      if(toBeProcessed.isEmpty) temporaryMap
      else{
        val actorRefLoadBalancer = context.actorSelection(toBeProcessed.head._2)
        val newMap = temporaryMap + (toBeProcessed.head._1 -> actorRefLoadBalancer)
        createActorRef(toBeProcessed.tail,newMap)
      }

    }

    mapLoadBalancer = createActorRef(phasesLoadBalancers,Map[NamesPhases.Value,ActorSelection]())

    */
    loadBalancerExternalRedis = mapLoadBalancer(NamesPhases.EXTERNAL)

    redis = new RedisClient(ConstantsKeys.REDIS_OUT_IP.toString, ConstantsKeys.REDIS_OUT_PORT.toString.toInt)

  }

  def receive = initState

  def initState: Receive = {
    case UserRequest(xmlString,phaseRequested,email,actor) =>
      logger.info("Received UserRequest Message")
      try{
        if(actor != null) actorCaller = actor
        currentEmail = email
        val xmlFile = getXmlFileFromStringRepresentation(xmlString)
        val requestData = getDataFromXML(xmlFile)
        loadBalancerRef = sender()
        currentPhase = NamesPhases.withName(phaseRequested)
        currentJob = requestData._1
        currentComputationUnit = requestData._2
        currentDataSet = requestData._3
        currentConfigurations = requestData._4
        val key = URIManager.createRedisExternalKey(currentJob,currentDataSet)
        writeOnInternalRedis(key)
        context.become(waitConfirmationState)
        context.setReceiveTimeout(Duration(timeoutJobCFGReceiveState, MILLISECONDS))
      } catch {
        case e: Throwable =>
          error = Some(ErrorStack(e))
          EmailSender(currentEmail,EmailSubjects.ERROR_COMPUTATION.toString,"Hello\nThere were some errors during the computation of your request. We provide you the error report.\n"+error.get)
          resetInternalState()

      }


  }

  def waitConfirmationState: Receive = {

    case KeyResultInserted(uri, tag) =>
      try{
        sendJobCFGMessages(currentJob,currentConfigurations)
        context.setReceiveTimeout(Duration(timeoutJobCFGReceiveState, MILLISECONDS))
      } catch {
        case e: Throwable =>
          error = Some(ErrorStack(e))
          val reportString = Json(Report(None,error)).toString()
          val reportCompressed = DataCompressor.compress(reportString)
          val reportOriginalDim = reportString.getBytes("UTF-8").length
          context.become(waitResultState)
          mapLoadBalancer(NamesPhases.REDIS) ! WriteReport(Job(currentJob),currentComputationUnit,NamesPhases.ENTRYPOINT.toString,currentDataSet,reportCompressed,reportOriginalDim,self)
      }


    case JobCFGReceived() =>
      logger.info("Received JobCFGReceived Message")

      try{
        waitConfirmationJobCFG = waitConfirmationJobCFG - 1
        if(waitConfirmationJobCFG == 0){
          sendRequestComputationMessage()
          context.become(waitResultState)
          context.setReceiveTimeout(Duration(timeoutWaitResultState, MILLISECONDS))

        }
      } catch {
        case e: Throwable =>
          error = Some(ErrorStack(e))
          val reportString = Json(Report(None,error)).toString()
          val reportCompressed = DataCompressor.compress(reportString)
          val reportOriginalDim = reportString.getBytes("UTF-8").length
          context.become(waitResultState)
          mapLoadBalancer(NamesPhases.REDIS) ! WriteReport(Job(currentJob),currentComputationUnit,NamesPhases.ENTRYPOINT.toString,currentDataSet,reportCompressed,reportOriginalDim,self)
      }



    case ReceiveTimeout =>
      logger.info("Received ReceiveTimeout Message")
      //TODO implement
      resetInternalState()

  }

  def waitResultState: Receive = {
    case ResultReady(internalUri) =>
      logger.info("Received ResultReady Message")
      try{
        waitingAnswers -= 1
        currentUri = internalUri
        if(currentPhase != NamesPhases.PHASE1)
          mapLoadBalancer(NamesPhases.REDIS) ! GiveMeDictionary(Job(currentJob),currentComputationUnit,currentDataSet,self)
        else
          mapLoadBalancer(NamesPhases.EXTERNAL) ! CollectAll(currentUri,self,currentPhase.toString)

      }catch {
        case e: Throwable =>
          error = Some(ErrorStack(e))
          val reportString = Json(Report(None,error)).toString()
          val reportCompressed = DataCompressor.compress(reportString)
          val reportOriginalDim = reportString.getBytes("UTF-8").length
          context.become(waitResultState)
          mapLoadBalancer(NamesPhases.REDIS) ! WriteReport(Job(currentJob),currentComputationUnit,NamesPhases.ENTRYPOINT.toString,currentDataSet,reportCompressed,reportOriginalDim,self)
      }


    case TakeDictionary(data,dim) =>
      logger.info("Received TakeDictionary message")
      val dataString = new String(DataCompressor.decompress(data,dim))
      currentDictionary = Json.parse(dataString).as[it.necst.marc.phase1.post.data.ResultDictionary]
      mapLoadBalancer(NamesPhases.EXTERNAL) ! CollectAll(currentUri,self,currentPhase.toString)


    case FinalUriGenerated(uri,result,dim) =>
      logger.info("Received FinalUriGenerated message")
      logger.info(uri.toString)
      if(actorCaller == null) {
        EmailSender(currentEmail, EmailSubjects.RESULT_READY.toString, "Hello\nYou are receiving the URI you can use to retrieve your data about your request.\nPlease insert in Marc website.\nThe uri is " + uri.toString)
      }else{
        actorCaller ! ComputationSuccess(uri.toString)
      }
      resetInternalState()

    case ErrorWorkerMsg(error) =>
      logger.info("Received ErrorWorkerMsg Message")
      waitingAnswers -= 1
      mapLoadBalancer(NamesPhases.EXTERNAL) ! CollectReportError(currentPhase.toString,Job(currentJob),currentDataSet,currentComputationUnit,self)


    case UriReportAfterError(uri) =>
      logger.info("Received UriReportAfterError message")
      if(actorCaller == null) {
        EmailSender(currentEmail, EmailSubjects.ERROR_COMPUTATION.toString, "Hello\nThere were some errors during the computation of your request. We provide you some reports.\nPlease insert in Marc website the next uri " + uri.toString)
      }else{
        actorCaller ! ComputationTerminatedWithErrors(uri.toString)
      }
      resetInternalState()

    case KeyReportInserted(uri) =>
      logger.info("Received KeyReportInserted message")
      if(error.isDefined){
        mapLoadBalancer(NamesPhases.EXTERNAL) ! CollectReportError(currentPhase.toString,Job(currentJob),currentDataSet,currentComputationUnit,self)
      }


    case ReceiveTimeout =>
      logger.info("Received ReceiveTimeout Message")
      //TODO implement
      resetInternalState()

  }

  private def getXmlFileFromStringRepresentation(stringRepresentation: String): Node = {
    ConfigurationParser.validateXML(stringRepresentation)

    val xmlFile = XML.loadString(stringRepresentation)
    Utility.trim(xmlFile)
  }

  private def getDataFromXML(xmlFile: Node):(Job,String,String,Map[NamesPhases.Value,Option[AbstractConfiguration]])= {

    def getJobFromXML: Job ={
      ConfigurationParser.jobFromXML(xmlFile)
    }

    def getComputationUnitFromXML: String = {
      ConfigurationParser.computationUnitFromXML(xmlFile)
    }

    def getConfigurationsFromXML: Map[NamesPhases.Value,Option[AbstractConfiguration]] = {
      ConfigurationParser(xmlFile)
    }

    def getDataSetFromXML: String = {
      ConfigurationParser.dataSetFromXML(xmlFile)
    }

    val job = getJobFromXML
    val computationUnit = getComputationUnitFromXML
    val dataSet = getDataSetFromXML
    val configurations = getConfigurationsFromXML
    (job,computationUnit,dataSet,configurations)
  }

  private def sendJobCFGMessages(job: Job, configurations: Map[NamesPhases.Value,Option[AbstractConfiguration]]): Unit ={

    def sendMessages(toBeProcessed: List[NamesPhases.Value]): Unit = {
      if(toBeProcessed.nonEmpty){
        val configuration = configurations(toBeProcessed.head)
        if(configuration.isDefined){
          waitConfirmationJobCFG = waitConfirmationJobCFG +1
          val configurationString = parseConfiguration(configuration.get)
          val loadBalancer = mapLoadBalancer(toBeProcessed.head)
          val jobString = Job(job)
          val message = SendJobCFG(jobString,configurationString,currentDataSet,self)
          loadBalancer ! message
        }

        sendMessages(toBeProcessed.tail)
      }
    }

    sendMessages(configurationsMap(currentPhase))
  }

  private def sendRequestComputationMessage(): Unit ={
    val jobString = Job(currentJob)

    if(currentPhase != NamesPhases.PHASE1POST){
      val phaseTag = currentPhase match {

        case NamesPhases.PHASE2A =>
          Some(OptionalTag.A.toString)

        case NamesPhases.PHASE2C =>
          Some(OptionalTag.C.toString)
        case _ =>
          None
      }
      waitingAnswers = 1
      mapLoadBalancer(currentPhase) ! SendReqComputation(jobString,currentComputationUnit,phaseTag,self)

    }else{
      waitingAnswers = 4
      mapLoadBalancer(currentPhase) ! SendReqComputation(jobString,currentComputationUnit,Some(OptionalTag.A.toString),self)
      mapLoadBalancer(currentPhase) ! SendReqComputation(jobString,currentComputationUnit,Some(OptionalTag.B.toString),self)
      mapLoadBalancer(currentPhase) ! SendReqComputation(jobString,currentComputationUnit,Some(OptionalTag.C.toString),self)
      mapLoadBalancer(currentPhase) ! SendReqComputation(jobString,currentComputationUnit,Some(OptionalTag.PRE.toString),self)

    }

  }


  private def resetInternalState(): Unit ={
    if(waitingAnswers == 0) {
      loadBalancerRef ! FreeActor(self)
      waitConfirmationJobCFG = 0
      currentJob = null
      currentComputationUnit = null
      //TODO remove this
      //mapLoadBalancer = Map()
      currentDictionary = null
      currentUri = null
      currentPhase = null
      loadBalancerRef = null
      waitingAnswers = 0
      context.become(initState)
    }
  }

  private def parseConfiguration(source: AbstractConfiguration): String = {
    source match {

      case x: it.necst.marc.phase0.data.Configuration =>
        Json(x).toString()

      case x: it.necst.marc.phase1.data.Configuration =>
        Json(x).toString()

      case x: it.necst.marc.phase1.post.data.Configuration =>
        Json(x).toString()

      case x: it.necst.marc.phase2.a.data.Configuration =>
        Json(x).toString()

      case x: it.necst.marc.phase2.b.data.Configuration =>
        Json(x).toString()

      case x: it.necst.marc.phase2.c.data.Configuration =>
        Json(x).toString()

      case x: it.necst.marc.phase3.pre.data.Configuration =>
        Json(x).toString()

      case x: it.necst.marc.phase3.data.Configuration =>
        Json(x).toString()

      case _ =>
        throw UnknownConfigurationException
    }
  }

  private def writeOnInternalRedis(key: String) = {

    val keyDim = key + ":dim"

    if(redis.connect){
      val dataRedis = redis.get(key)
      if(dataRedis.isDefined){
        var data = DataEncoder.decode(redis.get(key).get)
        val dim = redis.get(keyDim).get

        loadBalancerExternalRedis ! WriteDataFromExternalRedis(Job(currentJob),
          NamesPhases.EXTERNAL.toString, currentDataSet, data, dim.toInt, self, None)
        data = null
      }else{
        throw DataNotFoundException("data not found for username = "+currentJob.userName+" job name = "+currentJob.jobName+" dataset = "+currentDataSet+" computation unit = "+currentComputationUnit)
      }
    }else{
      throw ConnectionRedisException("Connection to REDIS external server failed")
    }
    redis.disconnect

  }

}

case class ConnectionRedisException(message: String) extends Throwable
case class DataNotFoundException(message: String) extends Throwable
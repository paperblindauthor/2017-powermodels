package it.necst.marc.entrypoint.computation

import java.io.{ByteArrayInputStream, InputStream}
import javax.xml.XMLConstants
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import org.xml.sax.InputSource
import it.necst.marc.data.support.{ErrorStack, NamesPhases}
import it.necst.marc.data.{Bounds, FeatureMonotony, FeatureType}
import it.necst.marc.infrastructure.support.{Job, URIManager}
import it.necst.marc.phase1.computation.dataManipulation.QuantizationCorrectionImplementation
import it.necst.marc.phase1.data.internal.preprocessing.ReductionRule.Mode
import it.necst.marc.phase1.data._
import it.necst.marc.phase1.post.data.PhaseConfiguration
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase2.a.data.LagsSpec
import it.necst.marc.phase3.pre.data.{BaseConf, SimConfiguration}
import org.xml.sax.ErrorHandler

import scala.xml._

/**
 * Created by Andrea Corna on 21/08/15.
 */
object ConfigurationParser {

  final val JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage"
  final val W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema"
  final val JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource"
  final val MARC_SCHEMA_SOURCE = ConfigurationParser.getClass.getResourceAsStream("/configuration.xsd")

  final val schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new StreamSource(MARC_SCHEMA_SOURCE)).newValidator()

  def validateXML(xml:String) = {
    var input:InputStream = null
    try {
      input = new ByteArrayInputStream(xml.getBytes)
      val dbf = DocumentBuilderFactory.newInstance()
      dbf.setNamespaceAware(true)
      schema.validate(new DOMSource(dbf.newDocumentBuilder().parse(new InputSource(input))))
    } catch {
      case e:SAXException =>
        throw new InvalidConfigurationException(s"Configuration not valid.\n${ErrorStack(e)}")
    } finally {
      if(input!= null) {
        input.close()
      }
    }
  }

  def applyOnFile(xmlFilename: String)    = apply(XML.loadFile(xmlFilename))
  def applyOnString(xmlString: String)    = apply(XML.loadString(xmlString))
  def apply(xml: Node) = {
    val cleanXml = Utility.trim(xml) //Removes all useless blank spaces and line feeds

    //Parsing the configuration XML

    //JOB CONFIGURATION
    val jobN = cleanXml \ "job"
    val jobName                     = (jobN \ "name").text
    val jobOwner                    = (jobN \ "owner").text

    val job = Job(jobName,jobOwner)



    //COMPUTATION UNIT
    val computationUnitN = cleanXml \ "computation-unit"

    val dataset = (cleanXml \ "dataset").text


    val computationUnit  = computationUnitN.text

    //PHASES CONFIGURATION
    val phasesN = cleanXml \ "phases"
    //-PHASE 0
    val phase0N = phasesN \ "phase0"
    //--Timestamp-feature
    val timestampFeatureN = phase0N \ "timestamp-feature"
    val timestampFeature  = (timestampFeatureN \ "@ref").text
    //--Mappings
    val mappingsN         = phase0N \ "mappings"
    //---Features
    val features_mappingN = mappingsN \ "features"
    //----Feature
    val featureNSeq       = features_mappingN \ "feature"
    val featuresData = for {
      featureN <- featureNSeq
      //-----Name
      nameN = featureN \ "name"
      name  = nameN.text
      //-----Col-index
      col_indexN = featureN \ "col-index"
      col_index = col_indexN.text.toInt
      //-----Type
      typeN = featureN \ "type"
      val a = typeN.text
      type_val = try {
                      FeatureType.withName(typeN.text.toUpperCase) }
                  catch { case e:NoSuchElementException => null }
      //-----Monotony
      monotonyN = featureN \ "monotony"
      monotony = monotonyN.text match {
        case x if x.isEmpty =>  if(type_val.equals(FeatureType.CUMULATIVE)) FeatureMonotony.ASCENDANT else FeatureMonotony.NONE
        case x              =>  try FeatureMonotony.withName(monotonyN.text.toUpperCase)
                                catch {case e:NoSuchElementException => null}
      }
      //-----Bounds
      boundsN = featureN \ "bounds"
      //------Lower
      lowerN = boundsN \ "lower"
      lower = lowerN.text match {
        case x if x.isEmpty   => None
        case x                => Some(x.toDouble)
      }
      //------Upper
      upperN = boundsN \ "upper"
      upper = upperN.text match {
        case x if x.isEmpty   => None
        case x                => Some(x.toDouble)
      }
      //-----Reduction
      reductionN = featureN \ "reduction"
      reduction = if (reductionN.isEmpty) null
                  else reductionN.head.child.head match {
                  //------Linear configuration
                  case <linear>{content @ _*}</linear> =>
                      val contentNSeq = NodeSeq.fromSeq(content.filter { x => x.isInstanceOf[Elem] })
                      val values = contentNSeq.map{
                                                              case <splits>{tag_content}</splits> => "splits" -> tag_content.text
                                                              case _ => null
                                                  }.toMap
                      //-------Splits configuration
                      val splits = values("splits").toInt
                      ReductionRule(name, Mode.Linear, Option(splits),None)
                  //------Logarithmic configuration
                  case <logarithmic>{content @ _*}</logarithmic> =>
                      val contentNSeq = NodeSeq.fromSeq(content.filter { x => x.isInstanceOf[Elem] })
                      val values = contentNSeq.map{
                                                              case <splits>{tag_content}</splits> => "splits" -> tag_content.text
                                                              case _ => null
                                                  }.toMap
                      //-------Splits configuration
                      val splits = values("splits").toInt
                      ReductionRule(name, Mode.Log, Option(splits),None)
                  //------Fixed configuration
                  case <fixed>{content @ _*}</fixed> =>
                      val contentNSeq = NodeSeq.fromSeq(content.filter { x => x.isInstanceOf[Elem] })
                      val border = contentNSeq.map { x => x.text.toDouble }.sortWith(_<_).toList
                      ReductionRule(name, Mode.Custom,None, Option(border))
      }
    } yield (it.necst.marc.data.Feature ( name,
                                          col_index,
                                          name==timestampFeature,
                                          type_val,
                                          monotony,
                                          (lower,upper)
                                        ),reduction)

    val features = featuresData.map{ x => x._1}.toList

    val listReductionRules = featuresData.filter(_._2 != null).map{x => x._2}.toList

    val reductionRules = if(listReductionRules.isEmpty) None else Option(listReductionRules)

    //FIXME LEGACY CODE - dataURI is no more necessary, now there is the dataset concept
    //TODO organized insert data about datauri
    val dataURIBase = "marc://redis_external.marc.docker:6379"

    val dataURI = URIManager.createRedisExternalURI(dataURIBase,job,dataset)

    val phase0Config = Option(it.necst.marc.phase0.data.Configuration(dataURI.toString))

    //-PHASE 1
    val phase1N = phasesN \ "phase1"
    //--Features
    val featuresN = phase1N \ "features"
    //---Selections
    val selectionN = featuresN \ "selection"
    val all_as_isN = featuresN \ "all-as-is"
    val exclusionsList = if (all_as_isN.nonEmpty) Nil
    else {
      //---Exclusions
      val excludeNSeq = selectionN \ "exclude"
      val targetsNSeq = excludeNSeq \\ "@ref"
      targetsNSeq.map(_.text).toList
    }
    val exclusions = if(exclusionsList.isEmpty) None else Option(exclusionsList)

    val listFusionsRules = if (all_as_isN.nonEmpty) Nil else{
      val fuseNSeq = selectionN \ "fuse"
      (for {
        fuseN <- fuseNSeq
        //----Feature configuration
        featureN = fuseN \ "feature"
        //-----Name
        nameN = featureN \ "name"
        name  = nameN.text
        //-----Col-index
        col_indexN = featureN \ "col-index"
        col_index = col_indexN.text.toInt
        //-----Type
        typeN = featureN \ "type"
        type_val = try {
          FeatureType.withName(typeN.text.toUpperCase) }
        catch { case e:NoSuchElementException => null }
        //-----Monotony
        monotonyN = featureN \ "monotony"
        monotony = monotonyN.text match {
          case x if x.isEmpty =>  if(type_val.equals(FeatureType.CUMULATIVE)) FeatureMonotony.ASCENDANT else FeatureMonotony.NONE
          case x              =>  try FeatureMonotony.withName(monotonyN.text.toUpperCase)
          catch {case e:NoSuchElementException => null}
        }
        //-----Bounds
        boundsN = featureN \ "bounds"
        //------Lower
        lowerN = boundsN \ "lower"
        lower = lowerN.text match {
          case x if x.isEmpty   => None
          case x                => Some(x.toDouble)
        }
        //------Upper
        upperN = boundsN \ "upper"
        upper = upperN.text match {
          case x if x.isEmpty   => None
          case x                => Some(x.toDouble)
        }
        feature = it.necst.marc.data.Feature(
          name,
          col_index,
          false,
          type_val,
          monotony,
          (lower, upper))
        operationN = fuseN \ "operation"
        generator = operationN.text
      } yield FusionRule(feature,generator)).toList
    }

    val fusionsRules = if(listFusionsRules.isEmpty) None else Option(listFusionsRules)

    //--Coherence configuration

    val coherenceN = phase1N \ "coherence"
    val ruleNSeq = coherenceN \ "rule"
    val listCoherenceRules = (for {
      ruleN <- ruleNSeq
      val condition = (ruleN \ "condition").text

      val correctionsN = ruleN \ "corrections"
      //-----Actuation configuration
      val actuationNSeq = correctionsN \ "actuation"
      val actuationString = actuationNSeq.map(_.text).reduce{
        (cumulated, s) =>
          cumulated +
            (if (!cumulated.endsWith(";")) "" else ";") +
            s }
      val actuationStringTerminated = actuationString + ""
    } yield CoherenceRule(condition,actuationStringTerminated)).toList

    val coherenceRules = if(listCoherenceRules.isEmpty) None else Option(listCoherenceRules)
    //--Manipulation configuration [POSTPONED]
    val manipulationN = phase1N \ "manipulation"

    val phase2aN = phasesN \ "phase2a"

    val timestamp_splitting_diffN = phase2aN \ "timestamp-splitting-diff"
    val timestamp_splitting_diff = timestamp_splitting_diffN.text.toDouble

    val configurationN = phase2aN \ "configuration"

    //---Output-feature configuration
    val outputFeatureN = configurationN \ "output-feature" \\ "@ref"
    val outputFeatureName = outputFeatureN.text

    val targetFeatureNSeq = configurationN \ "target-feature"
    val targetFeaturesPhase2a = (for {
        targetFeatureN <- targetFeatureNSeq
        //---Reference configuration
        val refN = targetFeatureN \ "@ref"
        val ref = refN.text
        //---Monotony configuration
        val monotonyN = targetFeatureN \ "@config-by-monotony"
        val monotony = monotonyN.text match {
          case x if x.isEmpty => None
          case x => Option(x.toBoolean)
        }
      } yield it.necst.marc.phase1.post.data.ConfigurationFeature(ref,monotony)).toList

    val lags = phase2aN \ "lags"
    val arLag = lags \ "ar"
    val arLagValue = if (arLag.isEmpty) 1 else arLag.text.toInt
    val exLag = lags \ "ex"
    val exLagValue = if (exLag.isEmpty) 0 else exLag.text.toInt
    val chunkSizeN = phase2aN \ "noise-level"
    val chunkSize = if(chunkSizeN.nonEmpty) chunkSizeN.text.toInt else throw new InvalidConfigurationException("Noise-level is required")
    val phase2aConfig = Option(it.necst.marc.phase2.a.data.Configuration(outputFeatureName,LagsSpec(arLagValue,exLagValue),timestamp_splitting_diff,chunkSize))


    val phase2bN = phasesN \ "phase2b"
    //Check if phase2b in configuration is empty
    val targetFeaturesPhase2b = if(phase2bN.head.child.length == 0){
      Nil
    }else {
      val configurationN = phase2bN \ "configuration"

      val targetFeatureNSeq = configurationN \ "target-feature"
      (for {
        targetFeatureN <- targetFeatureNSeq
        //---Reference configuration
        val refN = targetFeatureN \ "@ref"
        val ref = refN.text
        //---Monotony configuration
        val monotonyN = targetFeatureN \ "@config-by-monotony"
        val monotony = monotonyN.text match {
          case x if x.isEmpty => None
          case x => Option(x.toBoolean)
        }
      } yield it.necst.marc.phase1.post.data.ConfigurationFeature(ref, monotony)).toList

    }

      val phase2bConfig = if(phase2bN.head.child.length == 0){
      None
    }else{
      val periodicity = (phase2bN \ "periodicity").text.toDouble
      val minimumBand = (phase2bN \ "minimum-band").text.toDouble
      val aggregationThreshold = (phase2bN \ "aggregation-threshold").text.toDouble
      Option(it.necst.marc.phase2.b.data.Configuration(periodicity,minimumBand,aggregationThreshold))
    }

    val phase2cN = phasesN \ "phase2c"


    val targetFeaturesPhase2c = if(phase2cN.head.child.length == 0){
      Nil
    }else{
      val configurationN = phase2cN \ "configuration"

      val targetFeatureNSeq = configurationN \ "target-feature"
      (for {
        targetFeatureN <- targetFeatureNSeq
        //---Reference configuration
        val refN = targetFeatureN \ "@ref"
        val ref = refN.text
        //---Monotony configuration
        val monotonyN = targetFeatureN \ "@config-by-monotony"
        val monotony = monotonyN.text match {
          case x if x.isEmpty => None
          case x => Option(x.toBoolean)
        }
      } yield it.necst.marc.phase1.post.data.ConfigurationFeature(ref,monotony)).toList

    }

    //--Manipulation configuration [Continues]

    val listManipulations = (for (operationN <- manipulationN.head.child if operationN.isInstanceOf[Elem])
      yield operationN match {
        //---Standardization configuration
        case <standardization>{content @ _*}</standardization> =>
          val contentNSeq = NodeSeq.fromSeq(content.filter { x => x.isInstanceOf[Elem] })
          val target_featureNSeq = contentNSeq \\ "@ref"
          val nameOperation = operationN.head.label
          //val nameParameter = contentNSeq.head.label
          val nameParameter = "features"
          val targetFeaturesList = target_featureNSeq.map{x =>  x.text}.toList
          val targetFeaturesParameter:String = generateStringManipulationParameters(targetFeaturesList)
          val parameter = ManipulationParam(nameParameter,targetFeaturesParameter)
          Some(ManipulationOperation(nameOperation,Option(parameter :: Nil)))
        //---Quantization Correction configuration
        case <quantization-correction>{content @ _*}</quantization-correction> =>
          val cumulativeFeatures = (for (f <- features) yield {
            f.specs.feature_type match {
              case FeatureType.CUMULATIVE =>
                Some(f.name)
              case _ =>
                None
            }
          }).filter(_.isDefined).map(_.get).toSet

          val configurationFeatures = targetFeaturesPhase2a.map(_.name).toSet ++
                                      targetFeaturesPhase2b.map(_.name).toSet ++
                                      targetFeaturesPhase2c.map(_.name).toSet

          val excludedFeatures = (cumulativeFeatures ++ configurationFeatures).mkString(",")

          Some(ManipulationOperation("quantizationcorrection", Option(ManipulationParam(QuantizationCorrectionImplementation.EXCLUDED, excludedFeatures) :: ManipulationParam(QuantizationCorrectionImplementation.OUTPUT_FEATURE, outputFeatureName) :: ManipulationParam(QuantizationCorrectionImplementation.TIME_SPLITTING_DIFF, timestamp_splitting_diff.toString) :: Nil)))
        //---Other operations configuration
        case _ => None
      }).toList.filter(_.isDefined).map(_.get)

    val manipulationRules = if(listManipulations.isEmpty) None else Option(listManipulations)

    val phase1Config = Option(it.necst.marc.phase1.data.Configuration(features.toList,timestampFeature,reductionRules,coherenceRules,
      manipulationRules, fusionsRules, exclusions))


    val phase3N = phasesN \ "phase3"

    val tick = (phase3N \ "clock").text.toDouble

    val phase2cConfig = if(phase2cN.head.child.length == 0){
      Option(it.necst.marc.phase2.c.data.Configuration(outputFeatureName,tick)) //TODO INCORRECT ONLY FOR TEST
    }else{
      Option(it.necst.marc.phase2.c.data.Configuration(outputFeatureName,tick))
    }

    val phasesConfigs = PhaseConfiguration(Phase2Subphase.A,targetFeaturesPhase2a) ::
                        PhaseConfiguration(Phase2Subphase.B,targetFeaturesPhase2b) ::
                        PhaseConfiguration(Phase2Subphase.C,targetFeaturesPhase2c) :: Nil
    val phase1PostConfig = Option(it.necst.marc.phase1.post.data.Configuration(phasesConfigs, outputFeatureName))

    val phase3PreConfig = if(phase3N.head.child.length == 0){
      None
    }else{
      //--Clock configuration
      val tick = (phase3N \ "clock").text.toDouble
      //--MaxSimulation configuration
      val simulationLimit = (phase3N \ "max-ticks").text.toInt
      //--Charge/Discharge configuration
      val chargeN = phase3N \ "charge"
      val dischargeN = phase3N \ "discharge"

      val boundOutputFeature = features.filter(_.name.equals(outputFeatureName)).head.specs.bounds
      val chargeConf = if(chargeN.isEmpty) None else Some(parseSimulationConfiguration(chargeN.head,boundOutputFeature))
      val dischargeConf = if(dischargeN.isEmpty) None else Some(parseSimulationConfiguration(dischargeN.head,boundOutputFeature))
      Option(it.necst.marc.phase3.pre.data.Configuration(outputFeatureName,SimConfiguration(chargeConf,dischargeConf),tick,simulationLimit))
    }

    val phase3Config = if(phase3N.head.child.length == 0){
      None
    }else {
      val levelSize = (phase3N \ "level_size").text.toDouble
      val simulationLimit = (phase3N \ "simulation_limit").text.toInt
      Option(it.necst.marc.phase3.data.Configuration(levelSize,simulationLimit))
    }


    Map(NamesPhases.PHASE0 -> phase0Config,NamesPhases.PHASE1 -> phase1Config,NamesPhases.PHASE1POST -> phase1PostConfig,
      NamesPhases.PHASE2A -> phase2aConfig,NamesPhases.PHASE2B -> phase2bConfig, NamesPhases.PHASE2C -> phase2cConfig,
      NamesPhases.PHASE3PRE -> phase3PreConfig, NamesPhases.PHASE3 -> phase3Config)
  }


  def jobFromXML(filename: String): Job = {

    val xml = loadXmlFromFileName(filename)
    val jobN = xml \ "job"
    val jobName                     = (jobN \ "name").text
    val jobOwner                    = (jobN \ "owner").text
    Job(jobName,jobOwner)
  }

  def jobFromXML(xmlFile: Node): Job = {
    val xml = Utility.trim(xmlFile) //Removes all useless blank spaces and line feeds
    val jobN = xml \ "job"
    val jobName                     = (jobN \ "name").text
    val jobOwner                    = (jobN \ "owner").text
    Job(jobName,jobOwner)
  }

  def computationUnitFromXML(xmlFile: Node): String = {
    val xml = Utility.trim(xmlFile) //Removes all useless blank spaces and line feeds
    val computationUnit = (xml \ "computation-unit").text
    computationUnit
  }

  def dataSetFromXML(xmlFile: Node): String = {
    val xml = Utility.trim(xmlFile) //Removes all useless blank spaces and line feeds
    val dataSet = (xml \ "dataset").text
    dataSet
  }

  def computationUnitFromXML(filename: String): String = {
    val xml = loadXmlFromFileName(filename)
    val computationUnit = (xml \ "computation-unit").text
    computationUnit
  }
  
  private def loadXmlFromFileName(filename: String):Node = {
    val originalXML = XML.loadFile(filename)
    Utility.trim(originalXML) //Removes all useless blank spaces and line feeds
  }

  private def loadXmlFromString(content: String):Node = {
    val originalXML = XML.loadString(content)
    Utility.trim(originalXML) //Removes all useless blank spaces and line feeds
  }





  def parseSimulationConfiguration(topNode:Node, bounds:Bounds): BaseConf ={
    //--Start-from configuration
    val startFromN = topNode \ "start-from"
    //---Initial-condition configuration
    val initialPointsY = (for (initial_conditionN <- startFromN.head.child if initial_conditionN.isInstanceOf[Elem]) yield initial_conditionN.text.toDouble).toList

    //--Stop-at configuration
    val stopAtN = topNode \ "stop-at"
    val stopAtContentN = stopAtN.head.child.head
    val stopAt = stopAtContentN match {
      case <boundary>{content}</boundary> => content.text match {
        case "lower" => bounds.lower.get
        case "upper" => bounds.upper.get
      }
      case <threshold>{content}</threshold> => content.text.toDouble
    }

    return BaseConf(initialPointsY, stopAt)
  }

  private def generateStringManipulationParameters(names: List[String]):String = {
    def generateRecursive(toBeProcessed: List[String], finalString: String): String = {
      if(toBeProcessed.isEmpty) finalString
      else{
        val newFinalString = if(!finalString.isEmpty){
          finalString + "," + toBeProcessed.head
        }else{
          toBeProcessed.head
        }
        generateRecursive(toBeProcessed.tail,newFinalString)
      }
    }
    generateRecursive(names,"")
  }
}

class InvalidConfigurationException(message:String) extends Exception(message)
class CoherenceConditionParseException(message:String) extends Exception(message)

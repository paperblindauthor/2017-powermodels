package it.necst.marc.asplos.helpers

import it.necst.marc.data.Sample

/**
  * Created by andrea on 09/03/16.
  */
sealed trait Message

case class DownloadFiles(uri: String,collection: String) extends Message
case class StartCollector() extends Message
case class StartScan(data: List[String]) extends Message
case class DataCreated(data: List[Sample]) extends Message

package it.necst.marc.asplos.helpers

/**
  * Created by andreacorna on 10/08/16.
  */
case class PowerInfo(wattsupEnergy: WattsupEnergy,
                     wattsupPower: WattsupPower,
                     raplSocket1Energy: RaplSocket1Energy,
                     raplSocket1Power: RaplSocket1Power,
                     raplSocket2Energy: RaplSocket2Energy,
                     raplSocket2Power: RaplSocket2Power)

case class WattsupEnergy(data: List[Double])

case class WattsupPower(data: List[Double])

case class RaplSocket1Energy(data: List[Double])
case class RaplSocket1Power(data: List[Double])

case class RaplSocket2Energy(data: List[Double])
case class RaplSocket2Power(data: List[Double])
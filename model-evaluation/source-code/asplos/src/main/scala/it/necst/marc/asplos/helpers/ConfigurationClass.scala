package it.necst.marc.asplos.helpers

import scala.beans.BeanProperty

/**
  * Created by andreacorna on 07/08/16.
  */
class ConfigurationClass {


  @BeanProperty var phase2aFile: String = null
  @BeanProperty var phase1postFile: String = null
  @BeanProperty var phase1: String = null
  @BeanProperty var dictionary: String = null
  @BeanProperty var outputFeatureTrain: String = null
  @BeanProperty var outputFeatureTest: String = null
  @BeanProperty var timeSplittingDiff: Int = 0
  @BeanProperty var chunkSize: Int = 0
  @BeanProperty var configurationFeatureName: String = null

  @BeanProperty var testFoldersPath: String = null
  @BeanProperty var trainingFoldersPath: String = null
  @BeanProperty var outputFolderPath: String = null
}

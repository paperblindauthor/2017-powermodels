package it.necst.marc.asplos

import java.io.{File, PrintWriter}

import it.necst.marc.asplos.helpers._
import it.necst.marc.data.{Binding, FeatureMonotony, Value}
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.infrastructure.support.EmailSender
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification._
import it.necst.marc.phase1.post.data.{ModelConfiguration, ResultDictionary}
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase2.a.data.internal.Configuration
import it.necst.marc.phase2.a.data.{Alpha, LagsSpec}
import org.apache.commons.io.FileUtils
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import rapture.json.Json
import rapture.json.jsonBackends.jawn._

import scalaSci.RichDouble2DArray

/**
  * Created by andrea on 18/03/16.
  */
object ModelComparison {

  val logger = MarcLogger("ModelComparison")
  var config: ConfigurationClass = _


  def main(args: Array[String]): Unit = {
    logger.info("Comparison computations launched")
    val configPathFile = args(0)
    val text = scala.io.Source.fromFile(configPathFile).mkString
    val yaml = new Yaml(new Constructor(classOf[ConfigurationClass]))
    config = yaml.load(text).asInstanceOf[ConfigurationClass]

    try{

      val testSourcesFolder = new File(config.testFoldersPath)
      val trainingSourcesFolder = new File(config.trainingFoldersPath)

      if(testSourcesFolder.exists() && trainingSourcesFolder.exists()){
        val finalReport = iterateOverTests(testSourcesFolder,trainingSourcesFolder)


        val filse = new File(config.outputFolderPath+"/report.txt")
        filse.createNewFile()
        val writer = new PrintWriter(filse)
        writer.write(Json(finalReport).toString())
        writer.close()

        EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPARISON","Completed")

        System.exit(0)
      } else {
        throw SourceDirectoryNotFoundException(testSourcesFolder.getAbsolutePath)
      }

    } catch {
      case e: Throwable =>
        EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPARISON",ErrorStack(e))
        System.exit(-1)
    }


  }

  private def iterateOverTests(testsSourceFolder: File, trainingSourceFolder: File): FinalReport = {

    val testsFolders = testsSourceFolder.listFiles().filter(_.isDirectory)

    val trainingModels = loadTrainingModels(trainingSourceFolder.getAbsolutePath)

    val classes = (for(training <- trainingModels) yield {
      (for(entry <- training._2._1.byPhase.find(_.phase == Phase2Subphase.A).get.entries) yield {
          val currentClasses = for(binding <- entry.bindings.filterNot(_.name == config.outputFeatureTrain)) yield binding.name.split("_").last.toDouble
          currentClasses
        }).flatten
    }).flatten.toSet.toSeq.sorted

    val trainingModelsExpanded = for(training <- trainingModels) yield {

      training._1 -> (expandDictionaryTest(training._2._1,(classes.head.toInt,classes.last.toInt),config.configurationFeatureName),training._2._2)
    }

    val reports = (for(testFolder <- testsFolders) yield {
      iterateOverTrainings(trainingModelsExpanded,testFolder.getAbsolutePath)
    }).flatten.toList

    FinalReport(reports)

  }


  private def iterateOverTrainings(trainingModels: Map[String,(DictionaryExpanded,it.necst.marc.phase2.a.data.Result)],
                                   testFolder:String): List[TestReport] = {

    (for(training <- trainingModels) yield {
      val testInfo = TestInfo(training._1.split("_").head,testFolder,defineHTEnabled(training._1))
      performComparison(training._2._1,training._2._2,testFolder,testInfo)
    }).toList

  }



  private def performComparison(trainingDictionary: DictionaryExpanded,
                                trainingData: it.necst.marc.phase2.a.data.Result,
                                testSourceFolder: String,
                                testInfo: TestInfo): TestReport = {

    val phase1postResultTestSet = loadResultsPhase1Post(testSourceFolder)

    if(phase1postResultTestSet.isDefined){

      val dictionaryTest = loadDictionary(testSourceFolder)

      if(dictionaryTest.isDefined){
        val classes = (for(entry <- trainingDictionary.byPhase.find(_.phase == Phase2Subphase.A).get.entries) yield {
          val currentClasses = for(binding <- entry.bindings.filterNot(_.name == config.outputFeatureTrain)) yield binding.name.split("_").last.toDouble
          currentClasses
        }).flatten.sorted.toSet


        val dictionaryExpanded = expandDictionaryTest(dictionaryTest.get,(classes.head.toInt,classes.last.toInt),config.configurationFeatureName)

        val phase2aTestConfig = createConfigPhase2A(phase1postResultTestSet.get)

        val list = for(configuration <- phase1postResultTestSet.get.batches) yield {
          val batches = splitBatch(configuration.dataForProcessing,phase2aTestConfig.timeFeatureIndex,phase2aTestConfig.timeSplittingDiff)
          val alphas = getAlphas(configuration,trainingData,dictionaryExpanded,trainingDictionary)
          val extendedCode = dictionaryExpanded.byPhase.find(_.phase == Phase2Subphase.A).get.entries.find(_.code == configuration.codes.find(_.subphase == Phase2Subphase.A).get.code).get.codeExpanded
          if(alphas.isDefined){
            val msesInternal:List[(Double,Int)] = for(ba <- batches) yield {
              val results = parallelSquareError(ba.getv(),alphas.get,phase2aTestConfig)
              val newDataSet = results._2
              writeNewDataset(newDataSet)
              results._1
            }
            computeMSE(msesInternal) match {
              case Some(mse) =>
                ConfigurationMSE(extendedCode, Option(mse), configuration.dataForProcessing.numRows())
              case None =>
                ConfigurationMSE(extendedCode, None, configuration.dataForProcessing.numRows())
            }
          }
          else{
            ConfigurationMSE(configuration.codes.find(_.subphase == Phase2Subphase.A).get.code, None, configuration.dataForProcessing.numRows())
          }

        }

        val mses:(List[ConfigurationMSE],List[ConfigurationMSE]) = list.partition(_.mse.isDefined)
        val found = mses._1.map(_.count).sum
        val missed = mses._2.map(_.count).sum
        val generalMSE:Option[Double] = if(found <=0){
          None
        } else {
          computeMSE(mses._1.map{
            elem =>
              (elem.mse.get, elem.count)
          })}

        TestReport(testInfo,ModelMSE(generalMSE, found+missed),list,found.toDouble/(found+missed))
      }else{
        TestReport(testInfo,ModelMSE(None, 0), Nil, 0)

      }



    }else{

      TestReport(testInfo,ModelMSE(None, 0), Nil, 0)

    }


  }


  /*
  * FILE LOADERS
  * */

  private def loadResultsPhase1Post(sourceFolder: String): Option[it.necst.marc.phase1.post.data.Result] = {
    val resultPhase1PostTrainingFilePath = sourceFolder + "/" + config.phase1postFile

    val file = new File(resultPhase1PostTrainingFilePath)
    if(file.exists()){
      Some(Json.parse(FileUtils.readFileToString(file)).as[it.necst.marc.phase1.post.data.Result])
    }else None

  }

  private def loadResultsPhase2A(sourceFolder: String): it.necst.marc.phase2.a.data.Result = {
    val resultPhase1PostTrainingFilePath = sourceFolder + "/" + config.phase2aFile
    Json.parse(FileUtils.readFileToString(new File(resultPhase1PostTrainingFilePath))).as[it.necst.marc.phase2.a.data.Result]

  }


  private def loadDictionary(sourceFolder: String): Option[it.necst.marc.phase1.post.data.ResultDictionary] = {

    val dictionaryFilePath = sourceFolder + "/" + config.dictionary
    if(new File(dictionaryFilePath).exists()) Some(Json.parse(FileUtils.readFileToString(new File(dictionaryFilePath))).as[it.necst.marc.phase1.post.data.ResultDictionary])
    else None
  }


  private def createConfigPhase2A(phase1PostResult: it.necst.marc.phase1.post.data.Result): Configuration = {
    val features = phase1PostResult.features
    new Configuration(features,config.outputFeatureTest,config.timeSplittingDiff,LagsSpec(1,0),config.chunkSize)

  }



  /*
  *
  * HELPERS
  *
  * */

  private def writeNewDataset(dataSet: Array[Array[Double]]): Unit = {
    //TODO implement
  }
  
  private def getAlphas(configuration: ModelConfiguration,
                        phase2aResultTraining: it.necst.marc.phase2.a.data.Result,
                        dictionaryTest: DictionaryExpanded,
                        dictionaryTraining:DictionaryExpanded): Option[List[Alpha]] = {

    val bindings = dictionaryTest.byPhase.find(_.phase == Phase2Subphase.A).get.entries.find(_.code == configuration.codes.find(_.subphase == Phase2Subphase.A).get.code).get.bindingsExpanded.filterNot(_.name == config.outputFeatureTest)

    val entry = dictionaryTraining.byPhase.find(_.phase == Phase2Subphase.A).get.entries.find{x => Set(x.bindingsExpanded.filterNot(_.name == config.outputFeatureTrain):_*) == Set(bindings:_*)}
    if(entry.isDefined){
      val model = phase2aResultTraining.models.find(_.code == entry.get.code)
      if(model.isDefined){
        Some(model.get.alphas)
      }else None
    }else{
      None
    }
  }

  private def splitBatch(data: RichDouble2DArray,timeFeatureIndex:Int,splitDelta: Double): List[RichDouble2DArray] = {

    def recursiveSplit(currentIndex: Int,currentBatch: List[Array[Double]],batches: List[RichDouble2DArray]): List[RichDouble2DArray] = {
      if(currentIndex >= data.numRows())
        if(currentBatch.nonEmpty){
          new RichDouble2DArray(currentBatch.reverse.toArray) :: batches
        }else{
          batches
        }
      else{
        val currentRow = data.getRow(currentIndex)

        val nextIteration = if(currentBatch.nonEmpty){
          val previousTime = currentBatch.last(timeFeatureIndex)
          if(math.abs(previousTime - currentRow(timeFeatureIndex)) > splitDelta){
            (List(currentRow),new RichDouble2DArray(currentBatch.reverse.toArray) :: batches)
          }else{
            (currentRow::currentBatch,batches)
          }
        }else{
          (List(currentRow),batches)
        }
        recursiveSplit(currentIndex+1,nextIteration._1,nextIteration._2)

      }
    }

    recursiveSplit(1,List(data.getRow(0)),Nil)
  }


  private def expandDictionaryTest(dictionary: ResultDictionary,
                                   rangeConfigurationFeature: (Int,Int),
                                   configurationFeatureName: String): DictionaryExpanded = {

    val labels = for(i <- rangeConfigurationFeature._1 to rangeConfigurationFeature._2)
      yield configurationFeatureName + "_" + i.toDouble


    def createBindingsExpanded(toBeProcessed: List[String],
                               previousBinding: List[Binding],
                               newBindings: List[Binding]): List[Binding] = {

      if(toBeProcessed.isEmpty) newBindings

      else{
        val binding = previousBinding.find(_.name == toBeProcessed.head)

        val bindingToAdd = binding.getOrElse(Binding(toBeProcessed.head,Value(Some(0.0),FeatureMonotony.NONE)))

        createBindingsExpanded(toBeProcessed.tail,previousBinding,newBindings :+ bindingToAdd)
      }
    }

    def createCodeExpanded(toBeProcessed: List[Binding], expandedCode: String): Code = {
      if(toBeProcessed.isEmpty) expandedCode.substring(1)

      else{
        createCodeExpanded(toBeProcessed.tail,expandedCode + "|" + toBeProcessed.head.value.exact.get)
      }
    }

    val entries = dictionary.byPhase.filter(_.phase == Phase2Subphase.A).head.entries

    val entriesExpanded = for(entry <- entries) yield {

      val newBindings = createBindingsExpanded(labels.toList,entry.bindings,Nil)

      val newCode = createCodeExpanded(newBindings.sortBy(_.name),"") + "|" +entry.code.split("|").last

      it.necst.marc.asplos.helpers.Entry(entry.code,entry.bindings,newCode,entry.bindings.filterNot(newBindings.toSet).head +: newBindings )

    }


    DictionaryExpanded(List(Dictionary(Phase2Subphase.A,entriesExpanded)))


  }


  private def defineHTEnabled(trainingName: String): Boolean = {
      if (trainingName.split("_").last.equals("1")) true else false
  }

  private def loadTrainingModels(trainingSourceFolder: String): Map[String,(ResultDictionary,it.necst.marc.phase2.a.data.Result)] = {
    val sourceDirectory = new File(trainingSourceFolder)

    if(!sourceDirectory.exists()) throw SourceDirectoryNotFoundException(sourceDirectory.getAbsolutePath)

    val sources = sourceDirectory.listFiles().filter(_.isDirectory)

    (for(sourceFolder <- sources) yield {
      val model = loadResultsPhase2A(sourceFolder.getAbsolutePath)
      val dictionary = loadDictionary(sourceFolder.getAbsolutePath).get
      sourceFolder.toPath.getFileName.toString -> (dictionary,model)
    }).toMap
  }


  private def parallelSquareError(dataset:Array[Array[Double]], alphas:List[Alpha], configuration: Configuration):((Double, Int), Array[Array[Double]]) = {
    val featureMap = configuration.features.map{f => f.name -> f.column_index}.toMap

    val alphasModified = for(alpha <- alphas) yield {
      if(alpha.feature != config.outputFeatureTrain) alpha
      else Alpha(config.outputFeatureTest,alpha.lag,alpha.alpha)
    }
    var newDataset: Array[Array[Double]] = Array()

    val squareErrors = for {
      i <- Math.max(configuration.lags.autoregressive, configuration.lags.exogenous) until dataset.length
    } yield {
      val estimated = (for {
        alpha <- alphasModified
        row = dataset(i - alpha.lag)
        j = featureMap.get(alpha.feature) match {
          case Some(j) => j
          case None => throw new IllegalArgumentException("Feature not found.")
        }
      } yield {
        row(j) * alpha.alpha
      }).sum
      val real = dataset(i)(configuration.outputColumnIndex)
      val newRow: Array[Double] = dataset(i) :+ estimated
      newDataset = newDataset :+ newRow
      Math.pow(real - estimated, 2)
    }

    ((squareErrors.sum, squareErrors.length),newDataset)
  }

  private def computeMSE(parallelResults:Seq[(Double, Int)]):Option[Double] = {
    val total =  (for(r <- parallelResults) yield r._2).sum
    if(total == 0){
      None
    }else{
      Some((for(r <- parallelResults) yield r._1).sum / total)
    }
  }



}

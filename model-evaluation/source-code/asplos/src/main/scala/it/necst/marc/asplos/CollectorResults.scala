package it.necst.marc.asplos

import java.io.File
import java.net.URI
import java.nio.file.Path
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern._
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import it.necst.marc.asplos.helpers.{DownloadFiles, SaveResultsActor}
import it.necst.marc.data.support.MarcLogger

import scala.concurrent.Await


/**
  * Created by andreacorna on 04/08/16.
  */
object CollectorResults {

  private val actorSystemTest = ActorSystem("TestActorSystem", ConfigFactory.load("test_configuration_remote").getConfig("test-app.test"))

  private val loadBalancerEntryPoint = actorSystemTest.actorSelection("akka.tcp://LoadBalancerSystem@entrypoint.marc.docker:5562/user/loadBalancerEntryPoint")

  private val logger = MarcLogger(getClass.getName)

  private val baseUri = "marc://redis_external.marc.docker:6379/phase2a/final?"

  def main(args: Array[String]): Unit = {
    logger.info("CollectorResults launched")

    val listCollections: List[String] = (for(line <- scala.io.Source.fromFile("/home/marc_keys.txt").getLines())
      yield line.toString).toList



    for(collection <- listCollections){
      val epuratedCollection = collection.replace("\n","").replace(".csv","")
      println(epuratedCollection)
      val uri = URI.create(baseUri+createKey(epuratedCollection))
      val resultActor = actorSystemTest.actorOf(SaveResultsActor.props("akka.tcp://RedisLoadBalancerExternal@infrastructure.marc.docker:4000/user/loadBalancerRedisExternal"))
      implicit val timeout = Timeout(10, TimeUnit.MINUTES)
      val data = Await.result(resultActor ? DownloadFiles(uri.toString,epuratedCollection),timeout.duration).asInstanceOf[(List[File],Path)]
    }

    System.exit(0)

  }


  private def createKey(collection:String) = "username=marc&job_name=asplos&dataset="+collection+"&computation_unit="+collection


}
package it.necst.marc.asplos.helpers

import java.io.{File, PrintWriter}
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.util.Timeout
import it.necst.marc.data.Sample
import rapture.json.Json
import rapture.json.jsonBackends.jawn._

import scala.concurrent.Await
import scala.io.Source
import akka.pattern._
import it.necst.marc.phase1.data.Result


/**
  * Created by andreacorna on 10/08/16.
  */
object CreatePowerInfo {

  private val wattsupPowerColumn = 9
  private val wattsupEnergyColumn = 10
  private val raplPkgEnergy = 11
  private val timeColumnIndex = 0

  def apply(dataPhase1Socket1: Result,isMultiSocket: Boolean,dataPhase1Socket2: Result): PowerInfo = {

    val timeColIndex = dataPhase1Socket1.features.find(_.is_time.isDefined).get.column_index

    val wattsUpPowerIndex = dataPhase1Socket1.features.find(_.name == "wattsup_power").get.column_index

    val wattsupPower = (for(row <- dataPhase1Socket1.data.sortBy(_(timeColIndex))) yield {
      row(wattsUpPowerIndex)
    }).filterNot(_.isNaN)

    val wattsUpEnergyIndex = dataPhase1Socket1.features.find(_.name == "wattsup_energy").get.column_index

    val wattsupEnergy = (for(row <- dataPhase1Socket1.data.sortBy(_(timeColIndex))) yield {
      row(wattsUpEnergyIndex)
    }).filterNot(_.isNaN)

    val raplSocket1PKGIndex = dataPhase1Socket1.features.find(_.name == "rapl_pkg").get.column_index

    val raplSocket1Energy = (for(row <- dataPhase1Socket1.data.sortBy(_(timeColIndex))) yield {
      row(raplSocket1PKGIndex)
    }).filterNot(_.isNaN)

    val data = dataPhase1Socket1.data.sortBy(_(timeColIndex))
    val raplSocket1Power = (for (index <- 1 until data.length) yield {
      val diffEnergy = data(index)(raplSocket1PKGIndex) - data(index-1)(raplSocket1PKGIndex)
      val diffTime = data(index)(timeColIndex) - data(index-1)(timeColIndex)
      diffEnergy / diffTime
    }).toList.filterNot(_.isNaN)

    val secondSocket = if(isMultiSocket) {
      val raplSocket2PKGIndex = dataPhase1Socket2.features.find(_.name == "rapl_pkg").get.column_index

      val raplSocket2Energy = (for(row <- dataPhase1Socket2.data.sortBy(_(timeColIndex))) yield {
        row(raplSocket2PKGIndex)
      }).filterNot(_.isNaN)

      val data = dataPhase1Socket2.data.sortBy(_(timeColIndex))

      val raplSocket2Power = (for (index <- 1 until data.length) yield {
        val diffEnergy = data(index)(raplSocket2PKGIndex) - data(index-1)(raplSocket2PKGIndex)
        val diffTime = data(index)(timeColIndex) - data(index-1)(timeColIndex)
        diffEnergy / diffTime
      }).toList.filterNot(_.isNaN)
      (raplSocket2Energy,raplSocket2Power)
    }else(Nil,Nil)

    PowerInfo(WattsupEnergy(wattsupEnergy),WattsupPower(wattsupPower),RaplSocket1Energy(raplSocket1Energy),RaplSocket1Power(raplSocket1Power),RaplSocket2Energy(secondSocket._1),RaplSocket2Power(secondSocket._2))

  }

  def main(args: Array[String]): Unit = {


    val currentFolder = args(0)
    val multiSocket = args(1).toInt
    val nameFileSocket1 = "1.csv"
    val nameFileSocket2 = "2.csv"

    val wattsupData = loadData(currentFolder+"/"+nameFileSocket1)
    val wattsupPower = loadWattsupPower(wattsupData)

    val wattsupEnergy = loadWattsupEnergy(wattsupData)

    val socket1Rapl = loadRaplPgkSocket(currentFolder+"/"+nameFileSocket1)

    val powerInfo: PowerInfo = if(multiSocket == 0)
      PowerInfo(WattsupEnergy(wattsupEnergy),WattsupPower(wattsupPower),RaplSocket1Energy(socket1Rapl._1),RaplSocket1Power(socket1Rapl._2),RaplSocket2Energy(Nil),RaplSocket2Power(Nil))
    else{
      val socket2Rapl = loadRaplPgkSocket(currentFolder+"/"+nameFileSocket2)
      PowerInfo(WattsupEnergy(wattsupEnergy),WattsupPower(wattsupPower),RaplSocket1Energy(socket1Rapl._1),RaplSocket1Power(socket1Rapl._2),RaplSocket2Energy(socket2Rapl._1),RaplSocket2Power(socket2Rapl._2))

    }

    val filse = new File(currentFolder+"/power_info.txt")
    filse.createNewFile()
    val writer = new PrintWriter(filse)
    val string = Json(powerInfo).toString()
    writer.write(string)
    writer.close()

  }


  private def loadWattsupPower(data: List[Sample]): List[Double] = {

    (for(row <- data) yield {
      row.sample(wattsupPowerColumn)
    }).filterNot(_.isNaN)
  }

  private def loadWattsupEnergy(data: List[Sample]): List[Double] = {
    (for(row <- data) yield {
      row.sample(wattsupEnergyColumn)
    }).filterNot(_.isNaN)
  }

  private def loadRaplPgkSocket(file: String): (List[Double],List[Double]) = {
    val data = loadData(file)
    val energyColumnt = (for(row <- data) yield {
      row.sample(raplPkgEnergy)
    }).filterNot(_.isNaN)

    val powerColumn = (for (index <- 1 until data.length) yield {
      val diffEnergy = data(index).sample(raplPkgEnergy) - data(index-1).sample(raplPkgEnergy)
      val diffTime = data(index).sample(timeColumnIndex) - data(index-1).sample(timeColumnIndex)
      diffEnergy / diffTime
    }).toList.filterNot(_.isNaN)
    (energyColumnt,powerColumn)
  }

  private def loadData(file: String): List[Sample] = {
    var stringData = Source.fromFile(file).getLines().toList
    if(!stringData.head.split(",").forall{value:String =>
      try {
        value.toDouble
        true
      } catch {
        case e:NumberFormatException => false
      }
    }){stringData = stringData.tail} //Deletes possible header line

    val actor = ActorSystem("RedisCSVWriter").actorOf(CSVCollector.props(stringData,",",0))

    implicit val timeout =  Timeout(60,TimeUnit.MINUTES)
    val data = Await.result(actor ? StartCollector(), timeout.duration).asInstanceOf[List[Sample]]
    data.sortBy(_.sample(timeColumnIndex))
  }

}

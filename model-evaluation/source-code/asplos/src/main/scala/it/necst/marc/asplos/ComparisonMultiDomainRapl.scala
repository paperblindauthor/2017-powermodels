package it.necst.marc.asplos

import java.io.{File, PrintWriter}
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

import akka.actor.ActorSystem
import akka.util.Timeout
import it.necst.marc.asplos.helpers._
import it.necst.marc.data.{Feature, Sample}
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.infrastructure.support.EmailSender
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification._
import it.necst.marc.phase1.post.data.ResultDictionary
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import org.apache.commons.io.FileUtils
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import rapture.json.Json
import rapture.json.jsonBackends.jawn._
import akka.pattern._

import scala.concurrent.Await
import scala.io.Source

/**
  * Created by andreacorna on 11/08/16.
  */
object ComparisonMultiDomainRapl {


  val logger = MarcLogger("ModelComparison")
  var config: ConfigurationClass = _

  private val powerWindow = 60
  private val powerInfoFile = "power_info.txt"
  private var windowCount = 2
  private var startIndex = 1


  def main(args: Array[String]): Unit = {
    logger.info("Comparison computations launched")
    val configPathFile = args(0)
    val fileLoadPower = args(1)
    val text = scala.io.Source.fromFile(configPathFile).mkString
    val yaml = new Yaml(new Constructor(classOf[ConfigurationClass]))
    config = yaml.load(text).asInstanceOf[ConfigurationClass]

    try{

      val testSourcesFolder = new File(args(2))
      val trainingSourcesFolder = new File(config.trainingFoldersPath)


      if(testSourcesFolder.exists() && trainingSourcesFolder.exists()){
        performComparison(testSourcesFolder,trainingSourcesFolder,fileLoadPower)

        //        EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPARISON","Completed")

        System.exit(0)
      } else {
        throw SourceDirectoryNotFoundException(testSourcesFolder.getAbsolutePath)
      }

    } catch {
      case e: Throwable =>
        EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPARISON",ErrorStack(e))
        System.exit(-1)
    }


  }


  private def performComparison(testsSourceFolder: File, trainSourceFolder: File, fileLoadPower: String): Unit = {

    // CARICO MODELLI 2A PER TRAINING
    val trainingModels = loadTrainingModels(trainSourceFolder.getAbsolutePath)

    // CARICO PHASE1 DATA PER TEST
    val testsTraces = loadTestsTraces(testsSourceFolder.getAbsolutePath)

    // COSTRUISCO MAPPA [TRAIN_NAME -> MAP[CLASS -> MAP[FEATURE, ALPHA]]]
    val alphasMap = buildAlphasMap(trainingModels)

    /*
    * SCORRO IL PHASE1 PARTENDO DAL SECONDO
    *
    * ESTRAGGO LA CLASSE E PRENDO PER OGNI OTTENGO ENERGIA E AGGIUNGO L'INFORMAZIONE
    */
    val extendedTraces = scanAndComputeEnergy(testsTraces,alphasMap)
    /**
      *
      * potenza media per ogni trace
      */
    val powerTraces = obtainPower(extendedTraces,powerWindow)

    //CARICO TRACCIA CONTENENTE LA POTENZA COMPLESSIVA REALE

    val powerInfo = CreatePowerInfo(testsTraces(fileLoadPower),false,null)

    val results = evaluate(powerInfo,powerTraces)

    val testName = testsSourceFolder.getAbsolutePath.split("/").last
    val dir = new File(config.outputFolderPath+"/"+testName)
    dir.mkdir()
    val filse = new File(config.outputFolderPath+"/"+testName+"/report.txt")
    filse.createNewFile()
    val writer = new PrintWriter(filse)
    writer.write(Json(results).toString())
    writer.close()
  }


  private def evaluate(powerInfo: PowerInfo, powerTraces: Map[String,List[Double]]): List[MultiDomainResult] = {
    val testDuration = extractMinimumDuration(powerTraces,powerInfo)

    (for(index <- 0 until testDuration) yield {

      val estimatedTot = (for(trace <- powerTraces) yield {
        trace._2(index)
      }).sum

      val map = for(trace <- powerTraces) yield {
        trace._1 -> trace._2(index)
      }
      val realPower = powerInfo.raplSocket1Power.data.slice(20,powerInfo.wattsupPower.data.length-1)(index)
      val names = powerTraces.map(_._1)
      val mapNames = for(name <- names) yield {
        val p = Pattern.compile("-?\\d+");
        val m = p.matcher(name)
        var find = ""
        while (m.find()) {
          find  = m.group()
        }
        find.toInt -> name
      }
      val orderedNames = mapNames.toList.sortBy(_._1).map(_._2)
      val domainsEnabled = if(windowCount <= names.size) {
        val toWrite = orderedNames.slice(0,windowCount)
        windowCount = windowCount + 1
        toWrite
        /*} else if (windowCount == names.size){
          windowCount = windowCount + 1
          names*/
      }
      else{
        val toWrite = orderedNames.slice(startIndex,names.size)
        startIndex = startIndex + 1
        toWrite

      }
      MultiDomainResult(index,realPower,estimatedTot,map,domainsEnabled)
    }).toList
  }




  private def extractMinimumDuration(powerTraces: Map[String,List[Double]], powerInfo: PowerInfo): Int = {

    def scan(toBeProcessed: Map[String,List[Double]], currentMinimum: Int): Int = {
      if(toBeProcessed.isEmpty) currentMinimum

      else
        scan(toBeProcessed.tail,if(currentMinimum > toBeProcessed.head._2.length)toBeProcessed.head._2.length else currentMinimum)
    }

    if(powerTraces.isEmpty || powerInfo.wattsupPower.data.isEmpty) 0
    else Math.min(scan(powerTraces,powerTraces.head._2.length),powerInfo.wattsupPower.data.length)
  }




  private def obtainPower(energyTraces: Map[String,List[Double]], timeInterval: Int): Map[String,List[Double]] = {


    //OLD IMPLEMENTATION
    /*def scanSingleTrace(toBeProcessed: Map[String,List[List[Double]]],
                        result: Map[String,List[List[Double]]]): Map[String,List[List[Double]]] = {
      if(toBeProcessed.isEmpty) result
      else{
        val timeFeatureIndex = traces.get(toBeProcessed.head._1).get.features.find(_.is_time.isDefined).get.column_index
        val dataset = toBeProcessed.head._2.sortBy(_(timeFeatureIndex))
        val extendedDataset = (for(index <- 1 until dataset.length) yield {
          val energyDifference = dataset(index).last - dataset(index-1).last
          val timeDifference = dataset(index)(timeFeatureIndex) - dataset(index-1)(timeFeatureIndex)
          val power = (energyDifference / timeDifference)

          dataset(index) :+ power
        }).toList

        scanSingleTrace(toBeProcessed.tail,result + (toBeProcessed.head._1 -> extendedDataset))
      }
    }*/

    // scanSingleTrace(extendedTraces,Map())

    def recursiveScan(toBeProcessed: Map[String,List[Double]], powerTraces: Map[String,List[Double]]):Map[String,List[Double]] = {
      if(toBeProcessed.isEmpty) powerTraces

      else{
        val last = (toBeProcessed.head._2.length / timeInterval).toInt * timeInterval
        val data = toBeProcessed.head._2.slice(20,last)
        val powers = data.grouped(timeInterval).map{x => (x.last - x.head) / timeInterval}.toList
        val powerTrace: List[Double] = (for(power <- powers) yield {
          for(i <- 0 until timeInterval) yield {
            power
          }
        }).flatten

        recursiveScan(toBeProcessed.tail,powerTraces + (toBeProcessed.head._1 -> powers))
      }
    }

    recursiveScan(energyTraces,Map())

  }



  private def scanAndComputeEnergy(testTraces: Map[String,it.necst.marc.phase1.data.Result],
                                   alphasMap:  Map[String,Map[Double,Map[Feature.ID,Double]]]):  Map[String,List[Double]] = {


    def scanSingleTrace(toBeProcessed: Map[String,it.necst.marc.phase1.data.Result],
                        result: Map[String,List[Double]]): Map[String,List[Double]] = {

      if(toBeProcessed.isEmpty) result

      else {

        val currentMapAlphas = alphasMap.get("train").get
        val info = toBeProcessed.head._2
        val timeColumIndex = info.features.find(_.is_time.isDefined).get.column_index
        val dataset = info.data.sortBy(_(timeColumIndex))

        val newDataset: List[Double] = (for(index <- 1 until dataset.length) yield {
          val row = dataset(index)
          val featuresClasses = info.features.filter(_.name.contains(config.configurationFeatureName+"_")).filterNot(_.name.contains("power_"))

          val currentClass = extractCurrentClass(featuresClasses,row)
          val alphas = currentMapAlphas.get(currentClass)
          if(alphas.isDefined){
            val estimation = computeEstimation(dataset(index-1),row,alphas.get,info)
            estimation
          }else{
            -1.0
          }


        }).toList

        val res = recursiveSmoothGrouped(newDataset,100,400)

        scanSingleTrace(toBeProcessed.tail,result + (toBeProcessed.head._1 -> newDataset))
      }

    }

    scanSingleTrace(testTraces,Map())

  }

  private def recursiveSmooth(trace: List[Double], window: Int, maxWindow: Int): List[Double] = {
    if(window > maxWindow) {
      Nil
    }
    else{
      val smoothed = trace.sliding(window).map{element => element.sum / element.size}

      val isMonotonous = smoothed.sliding(2).forall{
        element =>
          element.last >= element.head
      }

      if (isMonotonous) {
        smoothed.toList
      } else {
        recursiveSmooth(trace, window + 1,maxWindow)
      }
    }
  }

  private def recursiveSmoothGrouped(trace: List[Double], window: Int, maxWindow: Int): List[Double] = {
    if(window > maxWindow) {
      Nil
    }
    else{
      val smoothed = trace.grouped(window).map{element => element.sum / element.size}

      val isMonotonous = smoothed.sliding(2).forall{
        element =>
          element.last >= element.head
      }

      if (isMonotonous) {
        smoothed.toList
      } else {
        recursiveSmoothGrouped(trace, window + 1,maxWindow)
      }
    }
  }

  private def computeEstimation(previousRow: List[Double],
                                row: List[Double],
                                alphas: Map[Feature.ID,Double],
                                phase1Result: it.necst.marc.phase1.data.Result): Double = {
    (for(alpha <- alphas) yield {
      val value = if(alpha._1 == config.outputFeatureTrain)
        previousRow(phase1Result.features.find(_.name == config.outputFeatureTest).get.column_index) * alpha._2
      else row(phase1Result.features.find(_.name == alpha._1).get.column_index) * alpha._2
      value
    }).sum

  }


  private def extractCurrentClass(features: List[Feature],row: List[Double]):Double = {

    def scanFeature(toBeProcessed: List[Feature]): Double = {
      if(row(toBeProcessed.head.column_index) == 1.0) toBeProcessed.head.name.split("_").last.toDouble
      else scanFeature(toBeProcessed.tail)
    }

    scanFeature(features)
  }





  private def buildAlphasMap(models: Map[String,(ResultDictionary,it.necst.marc.phase2.a.data.Result)]): Map[String,Map[Double,Map[Feature.ID,Double]]] = {


    def scanSingleModel(toBeProcessed: Map[String,(ResultDictionary,it.necst.marc.phase2.a.data.Result)],
                        result:  Map[String,Map[Double,Map[Feature.ID,Double]]]):  Map[String,Map[Double,Map[Feature.ID,Double]]] = {
      if (toBeProcessed.isEmpty) result

      else {

        val dictionary = toBeProcessed.head._2._1
        val models = toBeProcessed.head._2._2.models

        val mapEntries = (for (entry <- dictionary.byPhase.find(_.phase == Phase2Subphase.A).get.entries) yield {
          val code = entry.code
          val model = models.find(_.code == code).head

          val currentClass = extractCurrentClassByCode(code)
          val internalMap = (for (alpha <- model.alphas) yield {
            alpha.feature -> alpha.alpha
          }).toMap
          currentClass -> internalMap
        }).toMap

        scanSingleModel(toBeProcessed.tail, result + (toBeProcessed.head._1 -> mapEntries))
      }
    }

    scanSingleModel(models,Map())
  }

  private def loadTestsTraces(testingSourceFolder: String): Map[String,it.necst.marc.phase1.data.Result] = {
    val sourceDirectory = new File(testingSourceFolder)

    if(!sourceDirectory.exists()) throw SourceDirectoryNotFoundException(sourceDirectory.getAbsolutePath)

    val sources = sourceDirectory.listFiles().filter(_.isDirectory)

    (for(sourceFolder <- sources) yield {
      //      val csvData = loadTrace(sourceFolder.getAbsolutePath)
      val trace = loadResultsPhase1(sourceFolder.getAbsolutePath)
      sourceFolder.toPath.getFileName.toString -> trace
    }).toMap
  }

  private def loadTrainingModels(trainingSourceFolder: String): Map[String,(ResultDictionary,it.necst.marc.phase2.a.data.Result)] = {
    val sourceDirectory = new File(trainingSourceFolder)

    if (!sourceDirectory.exists()) throw SourceDirectoryNotFoundException(sourceDirectory.getAbsolutePath)

    val sources = sourceDirectory.listFiles().filter(_.isDirectory)

    (for (sourceFolder <- sources) yield {
      val model = loadResultsPhase2A(sourceFolder.getAbsolutePath)
      val dictionary = loadDictionary(sourceFolder.getAbsolutePath).get
      sourceFolder.toPath.getFileName.toString -> (dictionary, model)
    }).toMap
  }


  private def extractCurrentClassByCode(code: Code):Double = {
    val splits = code.replace("|",",").split(",").toList

    def scan(toBeProcessed: List[String], currentClass: Double,defaultClass: Double): Double = {
      if(toBeProcessed.isEmpty) defaultClass
      else{
        if(toBeProcessed.head.toDouble == 1.0){
          currentClass
        }else{
          scan(toBeProcessed.tail,currentClass+1,defaultClass)
        }
      }
    }

    scan(splits,0.0,-1.0)
  }

  private def loadResultsPhase1(sourceFolder: String): it.necst.marc.phase1.data.Result = {
    val resultPhase1PostTrainingFilePath = sourceFolder + "/" + config.phase1

    val file = new File(resultPhase1PostTrainingFilePath)
    val string = FileUtils.readFileToString(file)
    Json.parse(string).as[it.necst.marc.phase1.data.Result]

  }

  private def loadTrace(sourceFolder: String): List[Sample] = {
    var stringData = Source.fromFile(sourceFolder+"/data.csv").getLines().toList
    if(!stringData.head.split(",").forall{value:String =>
      try {
        value.toDouble
        true
      } catch {
        case e:NumberFormatException => false
      }
    }){stringData = stringData.tail} //Deletes possible header line

    val actor = ActorSystem("RedisCSVWriter").actorOf(CSVCollector.props(stringData,",",0))

    implicit val timeout =  Timeout(60,TimeUnit.MINUTES)
    val data = Await.result(actor ? StartCollector(), timeout.duration).asInstanceOf[List[Sample]]
    data.sortBy(_.sample(0))

  }

  private def loadResultsPhase2A(sourceFolder: String): it.necst.marc.phase2.a.data.Result = {
    val resultPhase1PostTrainingFilePath = sourceFolder + "/" + config.phase2aFile
    Json.parse(FileUtils.readFileToString(new File(resultPhase1PostTrainingFilePath))).as[it.necst.marc.phase2.a.data.Result]

  }

  private def loadPowerInfo(sourceFolder: String): PowerInfo = {
    val powerFile = sourceFolder + "/" + powerInfoFile
    Json.parse(FileUtils.readFileToString(new File(powerFile))).as[PowerInfo]

  }

  private def loadDictionary(sourceFolder: String): Option[it.necst.marc.phase1.post.data.ResultDictionary] = {

    val dictionaryFilePath = sourceFolder + "/" + config.dictionary
    if(new File(dictionaryFilePath).exists()) Some(Json.parse(FileUtils.readFileToString(new File(dictionaryFilePath))).as[it.necst.marc.phase1.post.data.ResultDictionary])
    else None
  }



}

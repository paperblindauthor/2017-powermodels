package it.necst.marc.asplos.helpers

/**
  * Created by andreacorna on 05/08/16.
  */
case class SourceDirectoryNotFoundException(folder: String) extends Throwable

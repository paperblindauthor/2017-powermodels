package it.necst.marc.asplos

import java.io.PrintWriter
import java.nio.file.{Files, Paths}

import akka.actor.{Actor, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import it.necst.marc.data.support.{MarcLogger, NamesPhases}
import it.necst.marc.entrypoint.actors.{ComputationSuccess, ComputationTerminatedWithErrors, UserRequest}
import it.necst.marc.infrastructure.support.EmailSender

import scala.xml._

/**
  * Created by andreacorna on 04/08/16.
  */
object SendComputationActor {


  val actorSystemTest = ActorSystem("ComputationActorSystem", ConfigFactory.load("test_configuration_remote").getConfig("test-app.test"))

  val loadBalancerEntryPoint = actorSystemTest.actorSelection("akka.tcp://LoadBalancerSystem@entrypoint.marc.docker:5562/user/loadBalancerEntryPoint")

  private val logger = MarcLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    logger.info("SendComputationActor launched")

    var listCollectionsAlreadyDone: List[String] = Nil

    if (Files.exists(Paths.get("/home/output/marc_keys_done.txt"))){
      listCollectionsAlreadyDone = (for(line <- scala.io.Source.fromFile("/home/output/marc_keys_done.txt").getLines())
        yield line.toString).toList
    }

    var listCollectionsErrors: List[String] = Nil
    if (Files.exists(Paths.get("/home/output/marc_keys_error.txt"))){

      listCollectionsErrors = (for(line <- scala.io.Source.fromFile("/home/output/marc_keys_error.txt").getLines())
        yield line.toString).toList
    }

    val listCollections: List[String] = (for(line <- scala.io.Source.fromFile("/home/marc_keys.txt").getLines())
      yield line.toString).toList

    val toBeProcessed = listCollections.filterNot(listCollectionsAlreadyDone.toSet).filterNot(listCollectionsErrors.toSet)
    val actor = actorSystemTest.actorOf(Props(new CallerActor(toBeProcessed)))

    actor ! Start()

  }


  case class Start()

  class CallerActor(collections: List[String]) extends Actor  {

    private var collectionsToCompute: List[String] = Nil
    private var currentCollection: String = _

    override def preStart(): Unit ={
      collectionsToCompute = collections
    }


    def receive = {
      case Start() =>
        context.become(collectResults)
        compute(collectionsToCompute.head)
        collectionsToCompute = collectionsToCompute.tail

    }

    def collectResults: Receive = {
      case ComputationSuccess(uri) =>
        new PrintWriter("/home/output/marc_keys_done.txt") { write(currentCollection+"\n"); close }

        EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPUTATION COMPLETED","Complete "+currentCollection+" \nURI: "+uri)
        if(collectionsToCompute.nonEmpty){
          compute(collectionsToCompute.head)
          collectionsToCompute = collectionsToCompute.tail

        }else{
          EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPUTATION COMPLETED","FINISHED")
          System.exit(0)
        }

      case ComputationTerminatedWithErrors(uri) =>
        new PrintWriter("/home/output/marc_keys_error.txt") { write(currentCollection+"\n"); close }
        EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPUTATION ERROR","ERROR "+currentCollection+" \nURI: "+uri)
        if(collectionsToCompute.nonEmpty){
          compute(collectionsToCompute.head)
          collectionsToCompute = collectionsToCompute.tail

        }else{
          EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS COMPUTATION COMPLETED","FINISHED")
          System.exit(0)
        }
    }



    private def compute(collection: String) = {

      val baseXMl = XML.loadFile("/home/asplos.xml")

      val comp = collection.replace("\n","").replace(" ","").replace(".csv","")
      currentCollection = comp
      def updateVersion(node: Node): Node = node match {
        case <configurations>{ ch @ _* }</configurations> => <configurations>{ ch.map(updateVersion)}</configurations> % Attribute(None, "xmlns", Text("http://mpower.necst.it/marc/configuration.xsd"), Null)
        case <dataset>{ contents }</dataset> => <dataset>{ comp }</dataset>
        case <computation-unit>{ contents }</computation-unit> => <computation-unit>{ comp }</computation-unit>
        case other @ _ => other
      }
      val xml = updateVersion(baseXMl)

      loadBalancerEntryPoint ! UserRequest(xml.toString(),NamesPhases.PHASE2A.toString,"andrea.corna.ac.91@gmail.com",self)
    }


  }
}

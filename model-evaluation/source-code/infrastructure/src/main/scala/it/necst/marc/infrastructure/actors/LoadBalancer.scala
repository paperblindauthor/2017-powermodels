package it.necst.marc.infrastructure.actors

import java.util.concurrent.atomic.AtomicLong

import akka.actor.{Actor, ActorRef, Props}
import it.necst.marc.data.support.MarcLogger
import LoadBalancer._
import it.necst.marc.infrastructure.actors.messages._
import it.necst.marc.infrastructure.support.Job

import scala.annotation.tailrec

/**
 * Created by andrea on 29/05/15.
 */
object LoadBalancer {

  def props(workersActor: List[ActorRef]): Props = Props(new LoadBalancer(workersActor))


}


class LoadBalancer(val workersActor: List[ActorRef]) extends Actor{

  private val logger = MarcLogger(getClass.getName)
  private val next = new AtomicLong(0)
  private var mapJobConfiguration: scala.collection.mutable.Map[Job,ActorRef] = _
  private var mapJobComputationRequest: scala.collection.mutable.Map[Job,List[SendReqComputation]] = _
  private var jobActive: List[Job] = Nil

  override def preStart() {
    super.preStart()
    mapJobConfiguration = scala.collection.mutable.Map[Job,ActorRef]()
    mapJobComputationRequest = scala.collection.mutable.Map[Job,List[SendReqComputation]]()
  }

  def receive: Receive = {

    case SendJobCFG(jobString,configuration,dataSet,replyTo) =>
      logger.debug("Received SendJobCFG message")
      val job = Job(jobString)
      val actorRef = if(mapJobConfiguration.contains(job)){
        Option(mapJobConfiguration(job))
      }else{
        roundRobinChooseActor()
      }

      if(actorRef.isDefined){
        mapJobConfiguration = mapJobConfiguration + (job -> actorRef.get)
          actorRef.get ! SendJobCFG(jobString,configuration,dataSet,replyTo)
      }else{
        replyTo ! ErrorMsgNoWorkerFree()
      }

    case SendReqComputation(jobString,computationUnit,phaseTag,replyTo) =>
      logger.debug("Received SendReqComputation message")
      //se non ho ricevuto prima un messaggio di configurazione reestituisco un errore
      val job = Job(jobString)
      if(!mapJobConfiguration.contains(job)){
        replyTo ! ErrorMsgMissingCfg()
      }else {
        val currentList: List[SendReqComputation] = mapJobComputationRequest.getOrElse(job,Nil)
        val newList = currentList :+ SendReqComputation(jobString, computationUnit,phaseTag, replyTo)
        mapJobComputationRequest = mapJobComputationRequest.updated(job, newList)
        if(!jobActive.contains(job)){
          val actor = mapJobConfiguration(job)
          actor ! newList.head
          //update request
          jobActive = jobActive :+ job
          mapJobComputationRequest = mapJobComputationRequest.updated(job, newList.tail)

        }


      }

    case ComputationComplete(jobString,computationUnit) =>
      logger.debug("Received ComputationComplete message")
      val job = Job(jobString)
      //if there are other requests compute them
      if(mapJobComputationRequest.getOrElse(job,Nil).nonEmpty){
        val currentList = mapJobComputationRequest(job)
        val actor = mapJobConfiguration(job)
        actor ! currentList.head
        mapJobComputationRequest = mapJobComputationRequest.updated(job,currentList.tail)
      }
      else jobActive = jobActive.filter(!_.equals(job))

    case DeallocateActorFromConfig(actorToDeallocate) =>
      logger.debug("Received DeallocateActorFromConfig message")
      deallocateActor(actorToDeallocate)

  }



  private def roundRobinChooseActor(): Option[ActorRef] = {
    @tailrec def createListFreeActors(toBeProcessed: List[ActorRef], listFreeActors: List[ActorRef]): List[ActorRef] = {
      if(toBeProcessed.isEmpty) listFreeActors

      else{
        val newListFreeActor = if(checkActorIsUsed(toBeProcessed.head,mapJobConfiguration)){
          listFreeActors
        }else{
          listFreeActors :+ toBeProcessed.head
        }
        createListFreeActors(toBeProcessed.tail,newListFreeActor)
      }
    }

    @tailrec def checkActorIsUsed(actor: ActorRef,map: scala.collection.mutable.Map[Job,ActorRef]): Boolean = {
      if(map.isEmpty) false

      else{
        if(map.head._2.equals(actor)){
          true
        }else{
          checkActorIsUsed(actor,map.tail)
        }
      }
    }

    val freeActors = createListFreeActors(workersActor,List[ActorRef]())
    if (freeActors.isEmpty){
      Option(null)
    }else{
      Option(freeActors((next.getAndIncrement % freeActors.size).asInstanceOf[Int]))

    }

  }

  private def deallocateActor(actor: ActorRef): Job ={
    val job = mapJobConfiguration.filter(_._2.equals(actor)).head._1
    mapJobComputationRequest = mapJobComputationRequest.filter(!_._1.equals(job))
    mapJobConfiguration = mapJobConfiguration.filter(!_._1.equals(job))
    job
  }


}

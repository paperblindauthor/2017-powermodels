package it.necst.marc.data.support

import scala.annotation.tailrec
import scala.collection.AbstractMap
import scala.collection.immutable.MapLike

/**
  * Created by andreadamiani on 07/11/15.
  */
trait Wildcard {
  def equalsWithWildcard(to:Any):Boolean
  final def =*=(to:Any):Boolean = equalsWithWildcard(to)
}

class WildcardMap[K, V](from: Map[K,V]) extends AbstractMap[K,V] with Map[K,V] with MapLike[K, V, WildcardMap[K, V]] with Wildcard{

  private val internal:Map[K,V] = from

  override def empty:WildcardMap[K,V] = WildcardMap()

  override def equalsWithWildcard(to:Any) = {
    to match {
      case to:Map[K,V] => to.forall{case (key,value) => {
        val values = getAll(key)
        values match {
          case None => false
          case Some(values) => values.forall{
            case v:Wildcard => v =*= value
            case v => v == value
          }
        }
      }}
      case _ => false
    }
  }

  override def +[B1 >: V](kv: (K, B1)): Map[K, B1] = internal + kv

  def matchWithWildcard(key: K): Map[K,V] = internal.filterKeys{
    case k:Wildcard => k =*= key
    case k => k == key
  }

  def getAll(key: K): Option[Iterable[V]] = {
    val matches = matchWithWildcard(key)
    if(matches.isEmpty) None
    else Some(matches.values)
  }

  override def get(key: K): Option[V] = {
    val matches = getAll(key)
    getAll(key) match {
      case None     => None
      case Some(x)  =>
        if(x.size != 1) throw new MultipleMatchException
        else Some(x.head)
    }
  }

  def getAllMatchingInternalKeys(key: K): Option[Iterable[K]] = {
    val matches = matchWithWildcard(key)
    if(matches.isEmpty) None
    else Some(matches.keys)
  }

  override def -(key: K): WildcardMap[K, V] = {
    @tailrec def recursiveApply(toBeProcessed:Iterable[K], internal:Map[K,V]):Map[K,V] = {
      if(toBeProcessed.isEmpty) internal
      else recursiveApply(toBeProcessed.tail, internal - toBeProcessed.head)
    }

    getAllMatchingInternalKeys(key) match {
      case None       => this
      case Some(keys) => WildcardMap(recursiveApply(keys, internal))
    }
  }

  override def iterator: Iterator[(K, V)] = internal.iterator

  class MultipleMatchException extends RuntimeException
}

object WildcardMap {
  def apply[K, V]() = new WildcardMap[K,V](Map[K,V]())
  def apply[K, V](from: Map[K,V]) = new WildcardMap[K,V](from)
  def apply[K, V](elems:(K,V)*) = new WildcardMap[K,V](Map[K,V](elems:_*))

  implicit def fromMap[K, V](x:Map[K,V]):WildcardMap[K,V] = new WildcardMap[K,V](x)
}

package it.necst.marc.infrastructure.support

import scala.pickling._
import scala.pickling.json._

/**
 * Created by andrea on 30/05/15.
 */
object Job {

  def apply(jobName: String, userName: String) =
    new Job(jobName,userName)

  def apply(job: Job):String = {
    job.pickle.value
  }

  def apply(jobJson: String):Job = {
    jobJson.unpickle[Job]
  }

}

class Job(val jobName: String, val userName: String){


  def canEqual(other: Any): Boolean = other.isInstanceOf[Job]

  override def equals(other: Any): Boolean = other match {
    case that: Job =>
      (that canEqual this) && this.hashCode == that.hashCode
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(jobName, userName)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

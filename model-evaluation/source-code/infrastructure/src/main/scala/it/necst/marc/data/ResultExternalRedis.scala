package it.necst.marc.data

import scalaSci.RichDouble2DArray

/**
  * Created by andrea on 09/11/15.
  */
case class ResultExternalRedis(data: List[Sample]){

  val dataForProcessing:RichDouble2DArray = new RichDouble2DArray((for (row <- data) yield row.sample.toArray).toArray)

}

case class Sample(sample:List[Double])
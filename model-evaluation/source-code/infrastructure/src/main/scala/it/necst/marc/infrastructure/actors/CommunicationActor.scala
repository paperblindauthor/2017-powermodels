package it.necst.marc.infrastructure.actors

import akka.actor._
import it.necst.marc.data.support.{MarcLogger, NamesPhases}
import it.necst.marc.infrastructure.actors.messages._
import it.necst.marc.infrastructure.support.{DataCompressor, Job, TraitInternalActor}
import com.typesafe.config.{Config}
import scala.collection.JavaConverters._
import scala.concurrent.duration._
import java.net.URI



/**
 * Created by andrea on 29/05/15.
 */
object CommunicationActor {

  def props(remoteAddressMap:Map[NamesPhases.Value,String], timeoutRequestWaitState: Int,
            startTimeoutReadyState: Int, phaseName: String,
            phaseTag: Option[String], workerType: TraitInternalActor, phaseConfiguration: Config):Props =
    Props(new CommunicationActor(remoteAddressMap,timeoutRequestWaitState,
    startTimeoutReadyState,phaseName,phaseTag,workerType,phaseConfiguration))


}

class CommunicationActor(val remoteAddressMap:Map[NamesPhases.Value,String],
                         val timeoutRequestWaitState: Int,
                         val startTimeoutReadyState: Int,
                         val phaseName: String,
                         val phaseTagInternal: Option[String],
                         val workerType: TraitInternalActor,
                         val phaseConfiguration: Config) extends Actor{

  lazy val timeout: FiniteDuration = 20.milliseconds

  private val internalActorSystem = ActorSystem("InternalActorsSystem")

  private val logger = MarcLogger(getClass.getName)

  private var currentJob: Job = _
  private var currentConfiguration: String = _
  private var currentComputationUnit: String = _
  private var currentDataSet: String = _
  private var currentReplyTo: ActorRef = _
  private var phasesToWait: Int = _
  private var currentLoadBalancer: ActorRef = _
  private var numberVisitsReadyState: Int = _
  private var timeoutReadyState: Int = startTimeoutReadyState
  private var mapData: scala.collection.mutable.Map[URI,(Array[Byte],Int)] = scala.collection.mutable.Map[URI,(Array[Byte],Int)]()
  private var redisClientLoadBalancer: ActorSelection = _
  private var mapLoadBalancers: Map[NamesPhases.Value,ActorSelection] = _
  private var beforeLoadBalancer: List[NamesPhases.Value] = Nil
  private var phaseTagRequest: Option[String] = None
  private var waitingMessageMultipleWrites: Int = 0
  private var currentResultURI: URI = _

  override def preStart(): Unit = {
    def createActorRef(toBeProcessed: Map[NamesPhases.Value, String], temporaryMap: Map[NamesPhases.Value, ActorSelection]): Map[NamesPhases.Value, ActorSelection] = {
      if (toBeProcessed.isEmpty) temporaryMap
      else {
        val actorRefLoadBalancer = context.actorSelection(toBeProcessed.head._2)
        val newMap = temporaryMap + (toBeProcessed.head._1 -> actorRefLoadBalancer)
        createActorRef(toBeProcessed.tail, newMap)
      }

    }
    mapLoadBalancers = createActorRef(remoteAddressMap, Map[NamesPhases.Value, ActorSelection]())
    redisClientLoadBalancer = mapLoadBalancers(NamesPhases.REDIS)

    beforeLoadBalancer = for (phase <- phaseConfiguration.getStringList(phaseName + ".previous_phases").asScala.toList) yield {
      NamesPhases.withName(phase)
    }

  }


  def receive: Receive = initState

  def initState:Receive = {
    case SendJobCFG(jobString,configuration,dataSet,replyTo) =>
      logger.debug("Received SendJobCFG message")
      currentJob = Job(jobString)
      currentDataSet = dataSet
      currentConfiguration = configuration
      currentLoadBalancer = sender()
      numberVisitsReadyState = 0
      phasesToWait = 0
      timeoutReadyState = startTimeoutReadyState
      mapData = scala.collection.mutable.Map[URI,(Array[Byte],Int)]()
      replyTo ! JobCFGReceived()
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

  }


  def readyState:Receive = {

    case SendJobCFG(jobString,configuration,dataSet,replyTo) =>
      logger.debug("Received SendJobCFG message")
      currentJob = Job(jobString)
      currentDataSet = dataSet
      currentConfiguration = configuration
      currentLoadBalancer = sender()
      numberVisitsReadyState = 0
      phasesToWait = 0
      timeoutReadyState = startTimeoutReadyState
      mapData = scala.collection.mutable.Map[URI,(Array[Byte],Int)]()
      replyTo ! JobCFGReceived()
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

    case SendReqComputation(jobString,computationUnit,phaseTag,replyTo) =>
      logger.debug("Received SendReqComputation message")
      updateTimeoutReadyState()
      val job = Job(jobString)
      if(job.equals(currentJob)){
        currentComputationUnit = computationUnit
        currentReplyTo = replyTo
        phaseTagRequest = phaseTag
        context.become(preambleState)
        redisClientLoadBalancer ! AreResultAlreadyComputedConfig(jobString,currentComputationUnit,phaseName,currentDataSet,phaseTagRequest,self)

      }else{
        replyTo ! ErrorWorkerMsg("job not found in communication actor")
      }

    case ReceiveTimeout ⇒
      logger.debug("Timeout expired")
      currentLoadBalancer ! DeallocateActorFromConfig(self)
      context.become(initState)


  }

  def preambleState: Receive = {

    case DataFound(uri) =>
      logger.info("Received DataFound message")
      logger.info("Sending ResultReady message")
      val jobString = Job(currentJob).toString
      currentReplyTo ! ResultReady(uri)
      currentLoadBalancer ! ComputationComplete(jobString,currentComputationUnit)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

    case DataNotFound() =>
      logger.info("Received DataNotFound message")
      sendRequestToPreviousPhases(self)
      context.setReceiveTimeout(Duration(timeoutRequestWaitState,MILLISECONDS))
      context.become(waitState)

  }

  def waitState: Receive = {
    case ErrorWorkerMsg(error) =>
      logger.info("Received ErrorWorkerMsg in waitState")
      currentReplyTo ! ErrorWorkerMsg(error)
      val jobString = Job(currentJob)
      currentLoadBalancer ! ComputationComplete(jobString,currentComputationUnit)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

    case ResultReady(resultUri) =>
      logger.info("Received ResultReady in waitState")
      self ! StartFetchData(resultUri)
      /*currentLoadBalancer ! ComputationComplete(Job(currentJob),currentComputationUnit)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)*/


    case ReceiveTimeout ⇒
      logger.info("Received timeout")
      currentLoadBalancer ! DeallocateActorFromConfig(self)
      resetInternalState()
      context.become(initState)
      currentReplyTo ! ErrorWorkerMsg("Received timeout")

    case ErrorMsgNoWorkerFree() =>
      logger.info("Received ErrorMsgNoWorkerFree")
      currentReplyTo ! ErrorWorkerMsg("ErrorMsgNoWorkerFree")
      currentLoadBalancer ! ComputationComplete(Job(currentJob),currentComputationUnit)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)


    case ErrorMsgMissingCfg() =>
      logger.info("Received ErrorMsgMissingCfg")
      currentReplyTo ! ErrorWorkerMsg("ErrorMsgMissingCfg")
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

    case StartFetchData(uri) =>
      logger.info("Start fetching data")
      redisClientLoadBalancer ! ReadFromURI(uri,self)

    case DataReady(uri,dataByte,originaDim) =>
      logger.info("Received DataReady message")
      mapData = mapData + (uri -> (dataByte,originaDim))
      if(mapData.size.equals(phasesToWait)){
        context.become(computationState)
        self ! StartComputation()
      }

    case DataNotPresent(uri) =>
      logger.info("Received DataNotPresent message")
      currentReplyTo ! ErrorWorkerMsg("DataNotPresent")
      currentLoadBalancer ! ComputationComplete(Job(currentJob),currentComputationUnit)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)
  }

  def computationState: Receive = {
    case StartComputation() =>
      logger.info("Start computation")
      val internalActor = internalActorSystem.actorOf(workerType.props(currentConfiguration,self,mapData.toMap,phaseConfiguration))
      internalActor ! StartInternalComputation()


    case InternalComputationCompleted(report,reportDim,result,resultDim)=>
      logger.info("Received ComputationCompleted")
      val jobString = Job(currentJob)
      redisClientLoadBalancer ! WriteReportAndResult(jobString,currentComputationUnit,phaseName,currentDataSet,report,reportDim,result,resultDim,self)
      mapData = scala.collection.mutable.Map[URI,(Array[Byte],Int)]()
      System.gc()

    case InternalComputationCompletedMultipleResults(mapResults,report,reportDim) =>
      logger.info("Received InternalComputationCompletedMultipleResults message")
      val jobString = Job(currentJob)

      waitingMessageMultipleWrites = mapResults.size + 1
      redisClientLoadBalancer ! WriteReport(jobString,currentComputationUnit,phaseName,currentDataSet,report,reportDim,self)
      writeResults(mapResults)
      mapData = scala.collection.mutable.Map[URI,(Array[Byte],Int)]()
      System.gc()



    case ComputationError(report,error) =>
      logger.info("Received ComputationError message")
      val jobString = Job(currentJob)
      val reportCompressed = DataCompressor.compress(report)
      val reportOriginalDim = report.getBytes("UTF-8").length
      redisClientLoadBalancer ! WriteReport(jobString,currentComputationUnit,phaseName,currentDataSet,reportCompressed,reportOriginalDim,self)
      currentReplyTo ! ErrorWorkerMsg(error)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)
      currentLoadBalancer ! ComputationComplete(Job(currentJob),currentComputationUnit)
      mapData.clear()
      System.gc()


    case KeysInserted(uriReport,uriResult) =>
      logger.info("Received KeysInserted message")
      currentReplyTo ! ResultReady(uriResult)
      val jobString = Job(currentJob)
      currentLoadBalancer ! ComputationComplete(jobString,currentComputationUnit)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

    case KeyReportInserted(uriReport) =>
      logger.info("Received KeyReportInserted message")
      waitingMessageMultipleWrites = waitingMessageMultipleWrites - 1

      if(waitingMessageMultipleWrites == 0){
        currentReplyTo ! ResultReady(currentResultURI)
        val jobString = Job(currentJob)
        currentLoadBalancer ! ComputationComplete(jobString,currentComputationUnit)
        context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
        context.become(readyState)
      }

    case KeyResultInserted(uriResult,phaseTag) =>
      logger.info("Received KeyResultInserted message")
      waitingMessageMultipleWrites = waitingMessageMultipleWrites - 1
      if(phaseTag.isDefined && phaseTag.get.equals(phaseTagRequest.get)){
        currentResultURI = uriResult
      }
      if(waitingMessageMultipleWrites == 0){
        currentReplyTo ! ResultReady(currentResultURI)
        val jobString = Job(currentJob)
        currentLoadBalancer ! ComputationComplete(jobString,currentComputationUnit)
        context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
        context.become(readyState)
      }
  }




  private def sendRequestToPreviousPhases(currentActorRef: ActorRef): Unit ={

    def sendRequests(toBeProcessed: List[NamesPhases.Value],requestMessage: SendReqComputation): Unit ={
      if(toBeProcessed.nonEmpty){
        val previousLoadBalancer = toBeProcessed.head
        logger.info("SENDING REQUEST TO "+previousLoadBalancer)
        mapLoadBalancers(previousLoadBalancer) ! requestMessage
        phasesToWait = phasesToWait + 1
        sendRequests(toBeProcessed.tail,requestMessage)

      }
    }
    phasesToWait = 0
    val jobString = Job(currentJob)
    val requestMessage = new SendReqComputation(jobString,currentComputationUnit,phaseTagInternal,currentActorRef)

    sendRequests(beforeLoadBalancer,requestMessage)

  }

  private def resetInternalState(): Unit ={
    currentJob = null
    currentConfiguration = null
    currentComputationUnit = null
    currentReplyTo = null
    phasesToWait = 0
    mapData = scala.collection.mutable.Map[URI,(Array[Byte],Int)]()
    numberVisitsReadyState = 0
    phaseTagRequest = None
    waitingMessageMultipleWrites = 0
    currentDataSet = null

  }

  private def updateTimeoutReadyState(): Unit ={
    numberVisitsReadyState = numberVisitsReadyState + 1
    timeoutReadyState = timeoutReadyState - (timeoutReadyState * numberVisitsReadyState * 0.1).toInt
  }

  private def writeResults(results: Map[String,(Array[Byte],Int)]): Unit ={

    val jobString = Job(currentJob)

    def recursiveSend(toBeProcessed: Map[String,(Array[Byte],Int)]): Unit ={
      if(toBeProcessed.nonEmpty){
        redisClientLoadBalancer ! WriteResult(jobString,currentComputationUnit,phaseName,currentDataSet,toBeProcessed.head._2._1,toBeProcessed.head._2._2,self,Some(toBeProcessed.head._1))
        recursiveSend(toBeProcessed.tail)
      }
    }

    recursiveSend(results)
  }


}

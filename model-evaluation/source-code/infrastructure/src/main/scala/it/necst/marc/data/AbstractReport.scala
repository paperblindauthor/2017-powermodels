package it.necst.marc.data

/**
 * Created by andreadamiani on 04/06/15.
 */
abstract class AbstractReport(content:Option[MathStats], error:Option[String]) {
  require((content.isDefined && error.isEmpty)||(content.isEmpty && error.isDefined), "Reports can either report a Success or a Failure, not both.")
  def succeeded = content.isDefined
  def get = content.getOrElse(throw new NoSuchElementException("The reports is empty due to an error."))
  def getOrElse[B >: MathStats](default: => B): B = content.getOrElse(default)
}

trait MathStats

package it.necst.marc.infrastructure.support

import java.net.URI

import it.necst.marc.data.support.{ExtractableEnumeration, NamesPhases, OptionalTag}


/**
 * Created by andrea on 06/08/15.
 */
object URIManager {

  private final val baseInternalURI = "marc://"
  private final val baseExternalURI = "marc://"

  def createResultInternalPhaseURI(ip: String, port: Int,job:Job,phaseName: String,dataset: String,computationUnit: String,optionalTag: Option[String] = None):URI = {
    URI.create(baseInternalURI+ip+":"+port+"/"+generateKeyRedisResult(job,phaseName,dataset,computationUnit,optionalTag))
  }

  def createResultInternalPhaseURIFromKey(ip: String, port: Int, key: String):URI = {
    URI.create(baseInternalURI+ip+":"+port+"/"+key)
  }

  def createReportInternalPhaseURI(ip: String, port: Int,job:Job,phaseName: String,dataset: String,computationUnit: String):URI = {
    URI.create(baseInternalURI+ip+":"+port+"/"+generateKeyRedisReport(job,phaseName,computationUnit,dataset))
  }

  def createReportInternalPhaseURI(ip: String, port: Int, key: String):URI = {
    URI.create(baseInternalURI+ip+":"+port+"/"+key)
  }

  def createRedisExternalURI(baseURI: String,job: Job,dataset: String): URI = {
    URI.create(baseURI+"/"+createRedisExternalKey(job,dataset))
  }

  def createFinalURI(keyHSet: String,host: String,port: Int): URI = {
    URI.create(baseExternalURI+host+":"+port+"/"+keyHSet)
  }

  def createRedisExternalKey(job: Job,dataset:String): String = {
    NamesPhases.EXTERNAL.toString+"/result"+generateKeyRedisJobAndDataSet(job,dataset)
  }
  private def generateKeyRedisJobAndDataSet(job: Job,dataset:String): String = {
    "?"+URIManagerKeys.USERNAME.toString+"="+job.userName+"&"+URIManagerKeys.JOB_NAME.toString+"="+job.jobName+"&"+URIManagerKeys.DATASET.toString+"="+dataset
  }
  private def generateKeyRedisInvariant(job:Job,computationUnit: String,dataset: String,optionTag: Option[String] = None) = {
    val base = "?"+URIManagerKeys.USERNAME.toString+"="+job.userName+"&"+URIManagerKeys.JOB_NAME.toString+"="+job.jobName+"&"+URIManagerKeys.DATASET.toString+"="+dataset+"&"+URIManagerKeys.COMPUTATIONAL_UNIT.toString+"="+computationUnit
    if(optionTag.isEmpty) base
    else base+"#"+optionTag.get
  }

  def generateHSetKey(uri: URI): String = {
    val parameters = getAllFieldFromURI(uri)
    val job = Job(parameters.get(URIManagerKeys.JOB_NAME).get,parameters.get(URIManagerKeys.USERNAME).get)
    parameters.get(URIManagerKeys.PHASE_NAME).get+"/final"+generateKeyRedisInvariant(job,parameters.get(URIManagerKeys.COMPUTATIONAL_UNIT).get,parameters.get(URIManagerKeys.DATASET).get)
  }

  def generateHSetKey(phase: String,job: Job,computationUnit: String,dataSet: String) = {
    phase+"/final"+generateKeyRedisInvariant(job,computationUnit,dataSet)
  }

  def generateKeyRedisReport(job:Job,phaseName: String,computationUnit: String,dataSet: String):String = {
    phaseName+"/report"+generateKeyRedisInvariant(job,computationUnit,dataSet)
  }

  def generateKeyRedisResult(job:Job,phaseName: String,dataset: String,computationUnit: String,optionTag: Option[String] = None):String = {
    if(phaseName != NamesPhases.PHASE0.toString) phaseName+"/result"+generateKeyRedisInvariant(job,computationUnit,dataset,optionTag)
    else phaseName+"/result?"+URIManagerKeys.USERNAME.toString+"="+job.userName+"&"+URIManagerKeys.JOB_NAME.toString+"="+job.jobName
  }



  def getConnectionInfoFromURI(uri: URI):(String,Int)= {
    val data = uri.getAuthority.split(":")
    (data(0),data(1).toInt)
  }


  def getKeyFromURI(uri: URI):String = {
    if(uri.getFragment != null) uri.getPath.substring(1)+"?"+uri.getQuery+"#"+uri.getFragment
    else uri.getPath.substring(1)+"?"+uri.getQuery
  }

  def getAllFieldFromURI(uri: URI):Map[URIManagerKeys.Value,String] = {
    val data = uri.getAuthority.split(":")
    val host = data(0)
    val port = data(1)
    val requestsData = uri.getPath.split("/").filter(!_.isEmpty)
    val phaseName = requestsData(0)
    val queryDataList = uri.getQuery.split("&").toList
    val optionalTag = if(uri.getFragment != null) uri.getFragment else ""

    def createMapRequest(toBeProcessed: List[String],mapQueryData: Map[String,String]): Map[String,String] = {
      if(toBeProcessed.isEmpty) mapQueryData
      else{
        val newMap = if(!toBeProcessed.head.isEmpty){
          val currentParts = toBeProcessed.head.split("=").toList
          mapQueryData + (currentParts.head -> currentParts(1))
        }else{
          mapQueryData
        }
        createMapRequest(toBeProcessed.tail,newMap)
      }
    }
    val mapQuery = createMapRequest(queryDataList,Map[String,String]())

    Map(URIManagerKeys.HOST -> host,
      URIManagerKeys.PORT -> port,
      URIManagerKeys.PHASE_NAME -> phaseName,
      URIManagerKeys.JOB_NAME ->mapQuery.get(URIManagerKeys.JOB_NAME.toString).get,
      URIManagerKeys.USERNAME->mapQuery.get(URIManagerKeys.USERNAME.toString).get,
      URIManagerKeys.COMPUTATIONAL_UNIT->mapQuery.get(URIManagerKeys.COMPUTATIONAL_UNIT.toString).get,
      URIManagerKeys.DATASET->mapQuery.get(URIManagerKeys.DATASET.toString).get,
      URIManagerKeys.OPTIONAL_TAG->optionalTag)
  }

  def getSizeKey(key: String) = key + ":dim"

  def getPhaseNameFromURI(uri: URI): String = uri.getPath.split("/").filter(_.nonEmpty).head

  def getDictionaryKey(job: String,dataset:String,computationUnit: String): String = {
    NamesPhases.PHASE1POST+"/result"+generateKeyRedisInvariant(Job(job),computationUnit,dataset,Some(OptionalTag.DICT.toString))
  }

  //TODO passare lo URI in modo da rendere generale l'approccio
  def getLocationDictionaryInfo(): (String,Int) = {
    ("redis_internal.marc.docker",6379)
  }
  def createListReportsURI(uri: URI): List[(String,URI)] = {

    val baseURI = baseInternalURI+uri.getAuthority+"/"
    val baseURIQuery = "/report?"+uri.getQuery

    def createList(toBeProcessed: List[NamesPhases.Value],uris: List[(String,URI)]): List[(String,URI)] = {
      if(toBeProcessed.isEmpty) uris
      else{
        createList(toBeProcessed.tail,(toBeProcessed.head.toString+"/report",URI.create(baseURI+toBeProcessed.head.toString+baseURIQuery)) :: uris)
      }
    }
    createList(NamesPhases.values.toList,Nil)

  }

  def createListResultURI(uri: URI): List[(String,URI)] = {

    val baseURI = baseInternalURI+uri.getAuthority+"/"
    val baseURIQuery = "/result?"+uri.getQuery

    def createList(toBeProcessed: List[NamesPhases.Value],uris: List[(String,URI)]): List[(String,URI)] = {
      if(toBeProcessed.isEmpty) uris
      else{
        createList(toBeProcessed.tail,(toBeProcessed.head.toString+"/result",URI.create(baseURI+toBeProcessed.head.toString+baseURIQuery)) :: uris)
      }
    }
    createList(NamesPhases.values.toList,Nil)

  }

  def createListReportsURI(currentJob: String,currentDataSet: String,currentComputationUni: String) = {

    def createList(toBeProcessed: List[NamesPhases.Value],uris: List[(String,URI)]): List[(String,URI)] = {
      if(toBeProcessed.isEmpty) uris
      else{
        createList(toBeProcessed.tail,(toBeProcessed.head.toString+"/report",URI.create("marc://redis_internal.marc.docker:6379/"+toBeProcessed.head.toString+"/report"+generateKeyRedisInvariant(Job(currentJob),currentComputationUni,currentDataSet))) :: uris)
      }
    }
    createList(NamesPhases.values.toList.filter(_ != NamesPhases.PHASE0),Nil)

  }

  def addTag(fields: Map[URIManagerKeys.Value,String],currentTag: String):URI = {
    val job = Job(fields.get(URIManagerKeys.JOB_NAME).get,fields.get(URIManagerKeys.USERNAME).get)
    URI.create(baseInternalURI+fields.get(URIManagerKeys.HOST).get+":"+fields.get(URIManagerKeys.PORT).get+"/"+
      generateKeyRedisResult(job,fields.get(URIManagerKeys.PHASE_NAME).get,fields.get(URIManagerKeys.DATASET).get,fields.get(URIManagerKeys.COMPUTATIONAL_UNIT).get,Some(currentTag)))
  }

  object URIManagerKeys extends ExtractableEnumeration {
    val HOST                = Value("host")
    val PORT                = Value("port")
    val PHASE_NAME          = Value("phase_name")
    val JOB_NAME            = Value("job_name")
    val USERNAME            = Value("username")
    val COMPUTATIONAL_UNIT  = Value("computation_unit")
    val OPTIONAL_TAG        = Value("optional_tag")
    val DATASET             = Value("dataset")

  }

}

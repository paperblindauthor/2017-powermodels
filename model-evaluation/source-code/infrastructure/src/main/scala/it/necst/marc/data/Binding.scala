package it.necst.marc.data

/**
 * Created by andreadamiani on 07/10/15.
 */


case class Binding(name:it.necst.marc.data.Feature.ID, value:Value){
  require(!name.isEmpty, "Feature name must be defined")
}

case class Value(exact:Option[Double], monotony:FeatureMonotony.Value){
  require((exact.isDefined || !monotony.equals(FeatureMonotony.NONE)) && !(exact.isDefined && !monotony.equals(FeatureMonotony.NONE)), "A value must be either exact or monotony-based.")
}



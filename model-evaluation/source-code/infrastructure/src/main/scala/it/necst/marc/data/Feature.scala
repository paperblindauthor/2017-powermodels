package it.necst.marc.data

import it.necst.marc.data.Feature.ID
import it.necst.marc.data.support.ExtractableEnumeration

import scalaSci.RichDouble1DArray

/**
 * Created by andreadamiani on 31/05/15.
 */
case class Feature(name:ID,
                             column_index:Int,
                             is_time:Option[Boolean],
                             specs:FeatureSpec){
  require(name!=null)
  require(is_time!=null && is_time.getOrElse(true),"Error is_time field")
  require(specs!=null)
}

object Feature {
  type ID = String
  def apply(name:ID, column_index:Int, is_time:Boolean, feature_type:FeatureType.Value, monotony:FeatureMonotony.Value, bounds:(Option[Double],Option[Double])):Feature = {
    Feature(name, column_index, if(is_time)Some(true) else None, FeatureSpec(feature_type, None, monotony, Bounds(bounds._1,bounds._2)))
  }
  def apply(name:ID, column_index:Int, is_time:Boolean, feature_type:FeatureType.Value, expanded_from:String, categorical_value:Double, monotony:FeatureMonotony.Value, bounds:(Option[Double],Option[Double])):Feature = {
    Feature(name, column_index, if(is_time)Some(true) else None, FeatureSpec(feature_type, Some(ExpandedFrom(expanded_from, categorical_value)), monotony, Bounds(bounds._1,bounds._2)))
  }

  def getValue(feature:ID, sample:RichDouble1DArray, prev:RichDouble1DArray, features:List[Feature]): Double = {
    features.find(_.name == feature) match {
      case Some(f) =>
        require(f.column_index < sample.length, s"$feature feature is not correctly bounded")
        sample.get(f.column_index)
      case None =>
        //Expanded feature?
        val expansions = features.filter{
          _.specs.expanded_from match {
              case Some(props) =>
                props.original_name == feature
              case None => false
            }
        }
        require(expansions.nonEmpty, s"$feature feature required is not available")
        val active = expansions.find{
          ef =>
            sample(ef.column_index) == (ef.specs.feature_type match {
              case FeatureType.INSTANTANEOUS => 1.0
              case FeatureType.CUMULATIVE => prev(ef.column_index) + 1.0
              case _ =>
                throw new IllegalArgumentException(s"$feature has not been expanded.")
            })
        }
        val activeExtended = if(active.isEmpty){
          expansions.find{
            ef =>
              sample(ef.column_index) == (ef.specs.feature_type match {
                case FeatureType.INSTANTANEOUS => Double.NaN //Excluded case ... looking for a cumulative just initialized
                case FeatureType.CUMULATIVE => 1.0
                case _ =>
                  throw new IllegalArgumentException(s"$feature has not been expanded.")
              })
          }
        } else {
          active
        }
        require(activeExtended.isDefined, s"$feature has been incorrectly expanded.")
        activeExtended.get.specs.expanded_from.get.value
    }
  }
}

case class FeatureSpec(feature_type:FeatureType.Value,
                   expanded_from:Option[ExpandedFrom],
                   monotony:FeatureMonotony.Value,
                   bounds:Bounds){
  require((expanded_from.isDefined &&
    feature_type != FeatureType.CATEGORICAL) || expanded_from.isEmpty)
  require(feature_type!=null)
  require(monotony!=null)
  require(bounds!=null)
}

case class ExpandedFrom(original_name:String, value:Double){
  require(original_name!=null && original_name.nonEmpty)
}

case class Bounds(lower:Option[Double], upper:Option[Double]){
  require(lower!=null)
  require(upper!=null)
  implicit def toTuple(b:Bounds):(Option[Double],Option[Double]) = (b.lower, b.upper)
  override def toString = "("+lower+","+upper+")"
}

object FeatureType extends ExtractableEnumeration {
  val INSTANTANEOUS   = Value("INSTANTANEOUS")
  val CATEGORICAL     = Value("CATEGORICAL")
  val CUMULATIVE      = Value("CUMULATIVE")
}

object FeatureMonotony extends ExtractableEnumeration {
  val ASCENDANT  = Value("ASCENDANT")
  val DESCENDANT = Value("DESCENDANT")
  //TODO remove when rapture option divergent implicit solved
  val NONE = Value("NONE")
}
package it.necst.marc.data


/**
 * Created by andreadamiani on 04/06/15.
 */
case class Performance(stats:Option[PerfStats]){
  def isDefined = stats.isDefined
  def isEmpty = stats.isEmpty
  def get = stats.get
  def getOrElse[B >: PerfStats](default: => B): B = stats.getOrElse(default)
}

case class PerfStats(total_computation_time:Long, partials:Option[PartialStats])
case class PartialStats(list:List[PartialStat])
case class PartialStat(part: String, computation_time:Long)


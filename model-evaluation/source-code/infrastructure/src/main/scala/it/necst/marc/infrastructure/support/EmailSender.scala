package it.necst.marc.infrastructure.support

import java.util.Properties
import javax.activation.{DataHandler, FileDataSource}
import java.io.File


import javax.mail._
import javax.mail.internet.{MimeMultipart, MimeBodyPart, InternetAddress, MimeMessage}

import it.necst.marc.data.support.ExtractableEnumeration
;

/**
  * Created by andrea on 19/01/16.
  */
object EmailSender {


  def apply(receiver: String,subject: String,messageString: String, attachment: Option[String] = None): Unit ={
    val username = USERNAME
    val password = PASSWORD

    val props = new Properties()
    props.put("mail.smtp.host", "smtp.gmail.com")
    props.put("mail.smtp.socketFactory.port", "465")
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory")
    props.put("mail.smtp.auth", "true")
    props.put("mail.smtp.port", "465")


    val session = Session.getInstance(props,
      new Authenticator {
        override def getPasswordAuthentication: PasswordAuthentication = {
          return new PasswordAuthentication(username, password)
        }
      })

    try {

      if(attachment.isEmpty){
        val message = new MimeMessage(session)
        message.setFrom(new InternetAddress(username))
        message.setRecipients(Message.RecipientType.TO,receiver)
        message.setSubject(subject)
        message.setText(messageString)
        Transport.send(message);
      }else{

        val message = new MimeMessage(session)
        message.setFrom(new InternetAddress(username))
        message.setRecipients(Message.RecipientType.TO,receiver)
        message.setSubject(subject)
        val multipart = new MimeMultipart()


        val messageBodyPart = new MimeBodyPart()
        messageBodyPart.setContent(message, "text/html")
        messageBodyPart.setText(messageString)
        multipart.addBodyPart(messageBodyPart)

        val attachmentBodyPart = new MimeBodyPart()
        val source = new FileDataSource(attachment.get)
        attachmentBodyPart.setDataHandler(new DataHandler(source))
        attachmentBodyPart.setFileName(new File(attachment.get).getName())
        multipart.addBodyPart(attachmentBodyPart)

        message.setContent(multipart)
        Transport.send(message)
      }

    } catch {
      case e: Throwable =>
        throw new RuntimeException(e);
    }
  }
  
}

object EmailSubjects extends ExtractableEnumeration {
  val DATA_INSERTED = Value("[MARC] Data Inserted")
  val DATA_NOT_INSERTED = Value("[MARC] Data Load Error")
  val RESULT_READY = Value("[MARC] Results Ready")
  val ERROR_COMPUTATION = Value("[MARC] Computation Error")
  val RESULT_SEND = Value("[MARC] Results files")
}

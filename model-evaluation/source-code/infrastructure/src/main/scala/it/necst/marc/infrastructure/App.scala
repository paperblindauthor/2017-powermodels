package it.necst.marc.infrastructure

import akka.actor.{ActorRef, ActorSystem}
import com.typesafe.config.{ConfigValue, Config, ConfigFactory}
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.infrastructure.actors.{RedisClientActor, RedisLoadBalancerExternal, RedisLoadBalancerInternal}
import scala.collection.JavaConversions._

import scala.annotation.tailrec

/**
 * Created by andrea on 02/06/15.
 */
object App {

  private val logger = MarcLogger(getClass.getName)

  def main(args : Array[String]) {
    logger.info("Set up infrastructure")

    val redisClientConfig = ConfigFactory.load("redis_client")
    val phaseName = redisClientConfig.getString("name")
    val expirationTimeResult = redisClientConfig.getLong(phaseName+".expiration_time_result")
    val expirationTimeReport = redisClientConfig.getLong(phaseName+".expiration_time_report")
    val expirationTimeData = redisClientConfig.getLong(phaseName+".expiration_time_data")
    val expirationTimeFinalResult = redisClientConfig.getInt(phaseName+".expiration_time_final_results")


    val numberActorsInternalLoadBalancer = redisClientConfig.getInt(phaseName+".client_internal_load_balancer")
    val numberActorsExternalLoadBalancer = redisClientConfig.getInt(phaseName+".client_external_load_balancer")

    val redisServersInternal = redisClientConfig.getConfig(phaseName+".servers_addresses_internal")

    val redisServerInternalMap: Map[String,Int] = loadMapRemoteAddresses(redisServersInternal)

    val redisServersExternal = redisClientConfig.getConfig(phaseName+".servers_addresses_external")

    val redisServerExternalMap: Map[String,Int] = loadMapRemoteAddresses(redisServersExternal)

    val loadBalancerInternalSystem = ActorSystem("RedisLoadBalancerInternal",ConfigFactory.load("redisClientLoadBalancerInternal"))
    val poolClientInternalBalancer =
      createPool(loadBalancerInternalSystem,redisServerInternalMap,numberActorsInternalLoadBalancer,expirationTimeData,expirationTimeReport,expirationTimeResult,
        expirationTimeFinalResult)
    val internalLoadBalancer =
      loadBalancerInternalSystem.actorOf(RedisLoadBalancerInternal.props(poolClientInternalBalancer),name="loadBalancerRedisInternal")

    val loadBalancerExternalSystem = ActorSystem("RedisLoadBalancerExternal",ConfigFactory.load("redisClientLoadBalancerExternal"))
    val poolClientExternalBalancer =
      createPool(loadBalancerExternalSystem,redisServerExternalMap,numberActorsExternalLoadBalancer,expirationTimeData,expirationTimeReport,expirationTimeResult,
        expirationTimeFinalResult)
    val externalLoadBalancer =
      loadBalancerExternalSystem.actorOf(RedisLoadBalancerExternal.props(poolClientExternalBalancer),name = "loadBalancerRedisExternal")

    System.gc()

  }

  private def createPool(actorSystem: ActorSystem,redisServerMap: Map[String,Int],
                         numberOfActors: Int,expirationTimeData:Long,
                         expirationTimeReport: Long,expirationTimeResult: Long,
                         expirationTimeFinalResult: Int):List[ActorRef] = {

    @tailrec def createListOfActors(currentNumber: Int, totalActor: Int, actorsList: List[ActorRef]): List[ActorRef] = {
      if(currentNumber == totalActor) actorsList
      else{
        val newActor = actorSystem.actorOf(RedisClientActor.props(redisServerMap,expirationTimeData,expirationTimeReport,expirationTimeResult,expirationTimeFinalResult))
        val newList = actorsList :+ newActor
        createListOfActors(currentNumber+1,totalActor,newList)
      }
    }

    createListOfActors(0,numberOfActors,Nil)

  }

  private def loadMapRemoteAddresses(configuration: Config): Map[String,Int] = {

    @tailrec def scanConfiguration(toBeProcessed: java.util.Set[java.util.Map.Entry[String, ConfigValue]], currentMap: Map[String, Int]): Map[String, Int] = {
      if (toBeProcessed.isEmpty) currentMap

      else {
        val newMap = currentMap + (toBeProcessed.head.getKey.replace("\"","") -> toBeProcessed.head.getValue.render().toInt)
        scanConfiguration(toBeProcessed.tail, newMap)
      }
    }
    scanConfiguration(configuration.entrySet(), Map[String, Int]())
  }
}

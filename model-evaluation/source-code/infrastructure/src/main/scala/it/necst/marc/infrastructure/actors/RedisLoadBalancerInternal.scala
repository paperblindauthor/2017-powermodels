package it.necst.marc.infrastructure.actors

import akka.actor.{Props, ActorRef}
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.infrastructure.actors.messages._

/**
 * Created by andrea on 02/06/15.
 */
object RedisLoadBalancerInternal {

  def props(redisClientActors: List[ActorRef]): Props =
    Props(new RedisLoadBalancerInternal(redisClientActors))

}

class RedisLoadBalancerInternal(redisClientActors: List[ActorRef]) extends RedisLoadBalancer(redisClientActors){

  private val logger = MarcLogger(getClass.getName)

  override def receive: Receive = {
    case WriteReport(job,computationUnit,phaseName,dataSet,reportByte,originalDimension,replyTo) =>
      logger.info("Received WriteReport message")
      val actor = chooseOneActor()
      actor ! WriteReport(job,computationUnit,phaseName,dataSet,reportByte,originalDimension,replyTo)


    case WriteResult(job,computationUnit,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag) =>
      logger.info("Received WriteResult message")
      val actor = chooseOneActor()
      actor ! WriteResult(job,computationUnit,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag)

    case WriteReportAndResult(job,computationUnit,phaseName,dataSet,reportByte,reportOriginalDim,resultByte,resultOriginalDim,replyTo,optionalTag) =>
      logger.info("Received WriteReportAndResult message")
      val actor = chooseOneActor()
      actor ! WriteReportAndResult(job,computationUnit,phaseName,dataSet,reportByte,reportOriginalDim,resultByte,resultOriginalDim,replyTo,optionalTag)

    case ReadFromURI(uri,replyTo) =>
      logger.info("Received ReadFromURI message")
      val actor = chooseOneActor()
      actor ! ReadFromURI(uri,replyTo)

    case AreResultAlreadyComputedURI(uri,replyTo)=>
      logger.info("Received AreDataAlreadyComputed message")
      val actor = chooseOneActor()
      actor ! AreResultAlreadyComputedURI(uri,replyTo)

    case AreResultAlreadyComputedConfig(job,computationUnit,phaseName,dataSet,optionalTag,replyTo) =>
      logger.info("Received AreDataAlreadyComputed message")
      val actor = chooseOneActor()
      actor ! AreResultAlreadyComputedConfig(job,computationUnit,phaseName,dataSet,optionalTag,replyTo)

    case CollectAll(uri,replyTo,namePhaseResult) =>
      logger.info("Received CollectAll message")
      val actor = chooseOneActor()
      actor ! CollectAll(uri,replyTo,namePhaseResult)

    case GiveMeDictionary(job,computationUnit,dataSet,replyTo) =>
      logger.info("Received GiveMeDictionary message")
      val actor = chooseOneActor()
      actor ! GiveMeDictionary(job,computationUnit,dataSet,replyTo)

    case _ =>
      logger.info("Received unknown message")
      sender() ! ErrorOperationUnknown()
  }
}

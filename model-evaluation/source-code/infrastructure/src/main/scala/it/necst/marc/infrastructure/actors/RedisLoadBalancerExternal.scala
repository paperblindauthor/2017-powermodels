package it.necst.marc.infrastructure.actors

import akka.actor.{Props, ActorRef}
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.infrastructure.actors.messages._

/**
 * Created by andrea on 02/06/15.
 */
object RedisLoadBalancerExternal {


  def props(redisClientActors: List[ActorRef]): Props =
    Props(new RedisLoadBalancerExternal(redisClientActors))

}

class RedisLoadBalancerExternal(val redisClientActors: List[ActorRef]) extends RedisLoadBalancer(redisClientActors){

  private val logger = MarcLogger(getClass.getName)

  override def receive:Receive = {

    case ReadFromURI(uri,replyTo) =>
      logger.info("Received ReadFromURI message")
      val actor = chooseOneActor()
      actor ! ReadFromURI(uri,replyTo)

    case WriteResult(job,computationUnit,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag) =>
      logger.info("Received WriteResult message")
      val actor = chooseOneActor()
      actor ! WriteResult(job,computationUnit,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag)

    case CollectAll(uri,replyTo,namePhaseResult) =>
      logger.info("Received CollectAll message")
      val actor = chooseOneActor()
      actor ! CollectAll(uri,replyTo,namePhaseResult)

    case WriteDataFromExternalRedis(job,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag) =>
      logger.info("Received WriteResult message")
      val actor = chooseOneActor()
      actor ! WriteDataFromExternalRedis(job,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag)

    case CollectReportError(currentPhase,currentJob,currentDataSet,currentComputationUnit,replyTo) =>
      logger.info("Received CollectReportError message")
      val actor = chooseOneActor()
      actor ! CollectReportError(currentPhase,currentJob,currentDataSet,currentComputationUnit,replyTo)

    case GiveMeAllResults(uri,replyTo) =>
      logger.info("Received GiveMeAllResults message")
      val actor = chooseOneActor()
      actor ! GiveMeAllResults(uri,replyTo)


    case _ =>
      logger.info("Received unknown message")
      sender() ! ErrorOperationUnknown()
  }
}

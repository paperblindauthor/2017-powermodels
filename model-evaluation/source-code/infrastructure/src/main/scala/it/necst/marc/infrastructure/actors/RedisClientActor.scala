package it.necst.marc.infrastructure.actors

import akka.actor.{Actor, Props}
import com.redis.RedisClient
import it.necst.marc.data.support.{MarcLogger, OptionalTag, NamesPhases}
import it.necst.marc.infrastructure.support._
import URIManager.URIManagerKeys
import it.necst.marc.infrastructure.actors.messages._
import java.net.URI



/**
 * Created by andrea on 31/05/15.
 */
object RedisClientActor {

  def props(redisServers: Map[String,Int],expirationTimeData: Long,
            expirationTimeReport:Long,expirationTimeResult:Long,
            expirationTimeFinalResult: Int): Props =
    Props(new RedisClientActor(redisServers,expirationTimeData,expirationTimeReport,expirationTimeResult,expirationTimeFinalResult))


}

class RedisClientActor(val redisServers: Map[String,Int],val expirationTimeData:Long,
                       val expirationTimeReport:Long,val expirationTimeResult:Long,
                       val expirationTimeFinalResult: Int) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  def receive: Receive = {

    case WriteReport(jobString,computationUnit,phaseName,dataSet,reportByte,originalDimension,replyTo) =>
      logger.info("Received WriteReport message")
      val job = Job(jobString)
      val report = DataEncoder.encode(reportByte)
      val uri = writeReport(job,computationUnit,phaseName,report,dataSet,originalDimension)
      replyTo ! KeyReportInserted(uri)

    case WriteResult(jobString,computationUnit,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag) =>
      logger.info("Received WriteResult message")
      val job = Job(jobString)
      val result = DataEncoder.encode(resultByte)
      val uri = writeResult(job,computationUnit,phaseName,result,dataSet,originalDimension,optionalTag)
      replyTo ! KeyResultInserted(uri,optionalTag)

    case WriteReportAndResult(jobString,computationUnit,phaseName,dataSet,reportByte,reportOriginalDim,resultByte,resultOriginalDim,replyTo,optionalTag) =>
      logger.info("Received WriteReportAndResult message")
      val job = Job(jobString)
      val report = DataEncoder.encode(reportByte)
      val result = DataEncoder.encode(resultByte)
      val uriReport = writeReport(job,computationUnit,phaseName,report,dataSet,reportOriginalDim)
      val uriResult = writeResult(job,computationUnit,phaseName,result,dataSet,resultOriginalDim,optionalTag)
      replyTo ! KeysInserted(uriReport,uriResult)

    case ReadFromURI(uri,replyTo)=>
      logger.info("Received ReadFromURI message")
      val data = readFromServer(uri)
      if(data._1.isDefined){
        val compressedData = DataEncoder.decode(data._1.get)
        val originalDim = data._2.get
        replyTo ! DataReady(uri,compressedData,originalDim)
      }else{
        replyTo ! DataNotPresent(uri)
      }


    case AreResultAlreadyComputedURI(uri,replyTo) =>
      logger.info("Received AreDataAlreadyComputed message")
      val resultIsAlreadyComputed = checkResultInRedis(uri)
      if(resultIsAlreadyComputed){
        replyTo ! DataFound(uri)
      }else{
        replyTo ! DataNotFound()
      }

    case AreResultAlreadyComputedConfig(jobString,computationUnit,phaseName,dataSet,phaseTag,replyTo) =>
      logger.info("Received AreDataAlreadyComputed message")
      val job = Job(jobString)
      val resultIsAlreadyComputed = checkResultInAllRedisServer(job,computationUnit,phaseName,dataSet,phaseTag)
      if(resultIsAlreadyComputed._1){
        val ip = resultIsAlreadyComputed._2.get
        val port = redisServers.get(ip).get
        val uri = URIManager.createResultInternalPhaseURI(ip,port,job,phaseName,dataSet,computationUnit,phaseTag)
        replyTo ! DataFound(uri)
      }else{
        replyTo ! DataNotFound()
      }

    case CollectAll(uri,replyTo,phaseResult) =>
      logger.info("Received CollectAll message")
      val results = collectAndCreateResults(uri,phaseResult)
      replyTo ! FinalUriGenerated(results._1,results._2,results._3)

    case GiveMeDictionary(job,computationUnit,dataSet,replyTo) =>
      logger.info("Received GiveMeDictionary message")
      val results = getDictionary(job,dataSet,computationUnit)
      if(results._1.isDefined){
        replyTo ! TakeDictionary(DataEncoder.decode(results._1.get),results._2.get)
      }else{
        replyTo ! DictionaryNotFound()
      }

    case WriteDataFromExternalRedis(jobString,phaseName,dataSet,resultByte,originalDimension,replyTo,optionalTag) =>
      logger.info("Received WriteDataFromExternalRedis message")
      val job = Job(jobString)
      val result = DataEncoder.encode(resultByte)
      val uri = writeExternalData(job,phaseName,result,dataSet,originalDimension,optionalTag)
      replyTo ! KeyResultInserted(uri,optionalTag)

    case CollectReportError(currentPhase,currentJob,currentDataSet,currentComputationUnit,replyTo) =>
      logger.info("Received CollectReportError message")
      val uri = collectAllReports(currentPhase,currentJob,currentDataSet,currentComputationUnit)
      replyTo ! UriReportAfterError(uri.toString)

    case GiveMeAllResults(uri,replyTo) =>
      logger.info("Received GiveMeAllResults message")
      replyTo ! TakeAllResults(createList(URI.create(uri)))

  }

  private def writeExternalData(job:Job,
                                phaseName: String,
                                result:String,
                                dataSet: String,
                                originalDim: Int,
                                optionalTag: Option[String]):URI = {

    val keyData = URIManager.createRedisExternalKey(job,dataSet)
    val keySize = keyData+":dim"
    val connectionData = chooseConnection()
    val connection = new RedisClient(connectionData._1,connectionData._2)
    connection.connect
    connection.setex(keyData,expirationTimeResult,result)
    connection.setex(keySize,expirationTimeResult,originalDim)
    connection.disconnect
    URI.create("")
  }

  private def writeReport(job:Job,
                          computationUnit: String,
                          phaseName: String,
                          report:String,
                          dataSet: String,
                          originalDim: Int):URI = {
    val keyData = URIManager.generateKeyRedisReport(job,phaseName,computationUnit,dataSet)
    val keySize = URIManager.getSizeKey(keyData)
    val connectionData = chooseConnection()
    val connection = new RedisClient(connectionData._1,connectionData._2)
    connection.connect
    connection.setex(keyData,expirationTimeReport,report)
    connection.setex(keySize,expirationTimeReport,originalDim)
    connection.disconnect
    URIManager.createReportInternalPhaseURI(connectionData._1,connectionData._2,keyData)
  }

  private def writeResult(job:Job,
                          computationUnit: String,
                          phaseName: String,
                          result:String,
                          dataSet: String,
                          originalDim: Int,
                          optionalTag: Option[String]):URI = {
    val keyData = URIManager.generateKeyRedisResult(job,phaseName,dataSet,computationUnit,optionalTag)
    val keySize = URIManager.getSizeKey(keyData)
    val connectionData = chooseConnection()
    val connection = new RedisClient(connectionData._1,connectionData._2)
    connection.connect
    connection.setex(keyData,expirationTimeResult,result)
    connection.setex(keySize,expirationTimeResult,originalDim)
    connection.disconnect
    URIManager.createResultInternalPhaseURIFromKey(connectionData._1,connectionData._2,keyData)

  }

  private def readFromServer(uri: URI):(Option[String],Option[Int]) = {
    val dataConnection = URIManager.getConnectionInfoFromURI(uri)
    val keyData = URIManager.getKeyFromURI(uri)
    val keySize = URIManager.getSizeKey(keyData)
    val connection = new RedisClient(dataConnection._1,dataConnection._2)
    connection.connect
    val data = connection.get(keyData)
    val dim = connection.get(keySize)
    val dimInt = if(dim.isDefined){
      Some(dim.get.toInt)
    }else{
      None
    }
    connection.disconnect
    (data,dimInt)
  }


  private def chooseConnection():(String,Int) = {
    (redisServers.head._1,redisServers.head._2)
  }


  private def checkResultInRedis(uri: URI):Boolean = {
    val uriInfo = URIManager.getConnectionInfoFromURI(uri)
    val key = URIManager.getKeyFromURI(uri)
    val connection = new RedisClient(uriInfo._1,uriInfo._2)
    connection.connect
    val result = connection.exists(key)
    if(result) {
      connection.expire(key,expirationTimeResult.toInt)
      connection.expire(URIManager.getSizeKey(key),expirationTimeResult.toInt)
    }
    connection.disconnect
    result
  }

  private def checkResultInAllRedisServer(job: Job, computationUnit: String, phaseName: String,dataSet: String,optionalTag: Option[String]): (Boolean,Option[String]) = {

    def checkOnEveryRedisServer(toBeProcessed: Map[String,Int], key: String):(Boolean,Option[String]) = {
      if(toBeProcessed.isEmpty) (false,None)
      else{
        val ip = toBeProcessed.head._1
        val port = toBeProcessed.head._2
        val connectionToServer = new RedisClient(ip,port)
        connectionToServer.connect
        if(connectionToServer.exists(key)){
          connectionToServer.expire(key,expirationTimeResult.toInt)
          connectionToServer.disconnect
          (true,Some(ip))
        }else{
          connectionToServer.disconnect
          checkOnEveryRedisServer(toBeProcessed.tail,key)
        }
      }
    }

    val key = URIManager.generateKeyRedisResult(job,phaseName,dataSet,computationUnit,optionalTag)
    checkOnEveryRedisServer(redisServers,key)
  }



  private def collectAndCreateResults(uri: URI,phaseResult:String):(URI,Option[Array[Byte]],Option[Int])= {

    val keyHSet = URIManager.generateHSetKey(uri)
    val reportsURIs = URIManager.createListReportsURI(uri)
    val fields = URIManager.getAllFieldFromURI(uri)
    val connectionData = chooseConnection()
    val connection = new RedisClient(connectionData._1,connectionData._2)
    connection.connect
    val jobString = Job(Job(fields.get(URIManagerKeys.JOB_NAME).get.toString,fields.get(URIManagerKeys.USERNAME).get.toString))
    val dictionaryData = getDictionary(jobString,fields.get(URIManagerKeys.DATASET).get,fields.get(URIManagerKeys.COMPUTATIONAL_UNIT).get)

    if(dictionaryData._1.isDefined){
      connection.hset(keyHSet,"dictionary",dictionaryData._1.get)
      connection.hset(keyHSet,"dictionary:dim",dictionaryData._2.get)
    }

    def insertReports(toBeProcessed: List[(String,URI)]): Unit = {
      if(toBeProcessed.nonEmpty){
        val data = readFromServer(toBeProcessed.head._2)
        if(data._1.isDefined){

          connection.hset(keyHSet,toBeProcessed.head._1,data._1.get)
          connection.hset(keyHSet,toBeProcessed.head._1+":dim",data._2.get)

        }
        insertReports(toBeProcessed.tail)
      }
    }

    def insertResult():(Option[Array[Byte]],Option[Int]) = {
      if(!phaseResult.equals(NamesPhases.PHASE1POST.toString)){
        val data = readFromServer(uri)
        if(data._1.isDefined){
          connection.hset(keyHSet,phaseResult+"/result",data._1.get)
          connection.hset(keyHSet,phaseResult+"/result:dim",data._2.get)

          (Some(DataEncoder.decode(data._1.get)),Some(data._2.get))
        }else (None,None)
      }else{

        def insert(toBeProcessed: List[OptionalTag.Value]):Unit = {
          if(toBeProcessed.nonEmpty){
              val currentUri = URIManager.addTag(fields,toBeProcessed.head.toString)
              val data = readFromServer(currentUri)
              if(data._1.isDefined) {
                connection.hset(keyHSet, phaseResult + "/result#" + toBeProcessed.head.toString, data._1.get)
                connection.hset(keyHSet, phaseResult + "/result#" + toBeProcessed.head.toString + ":dim", data._2.get)
              }
              insert(toBeProcessed.tail)
          }
        }
        insert(OptionalTag.values.toList)
        (None,None)
      }

    }

    insertReports(reportsURIs)
    val results = insertResult()

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    //TODO remove after debug
    val resultURI = URIManager.createListResultURI(uri)
    insertReports(resultURI)
    val fiedsFiltered = fields.filter(_._1 != URIManagerKeys.PHASE_NAME) + (URIManagerKeys.PHASE_NAME -> NamesPhases.PHASE1POST.toString)
    def insert(toBeProcessed: List[OptionalTag.Value]):Unit = {
      if(toBeProcessed.nonEmpty){
        val currentUri = URIManager.addTag(fiedsFiltered,toBeProcessed.head.toString)
        val data = readFromServer(currentUri)
        if(data._1.isDefined) {
          connection.hset(keyHSet, NamesPhases.PHASE1POST.toString + "/result#" + toBeProcessed.head.toString, data._1.get)
          connection.hset(keyHSet, NamesPhases.PHASE1POST.toString + "/result#" + toBeProcessed.head.toString + ":dim", data._2.get)
        }
        insert(toBeProcessed.tail)
      }
    }
    insert(OptionalTag.values.toList)
  //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    connection.expire(keyHSet,expirationTimeFinalResult)
    connection.disconnect

    (URIManager.createFinalURI(keyHSet,connectionData._1,connectionData._2),results._1,results._2)
  }

  private def getDictionary(job: String, dataset: String,computationUnit: String):(Option[String],Option[Int]) = {

    val dataConnection = URIManager.getLocationDictionaryInfo()
    val keyData = URIManager.getDictionaryKey(job,dataset,computationUnit)
    val keySize = URIManager.getSizeKey(keyData)
    val connection = new RedisClient(dataConnection._1,dataConnection._2)
    connection.connect
    val data = connection.get(keyData)
    val dim = connection.get(keySize)
    val dimInt = if(dim.isDefined){
      Some(dim.get.toInt)
    }else{
      None
    }
    connection.disconnect
    (data,dimInt)
  }

  private def collectAllReports(currentPhase: String,currentJob: String,currentDataSet: String,currentComputationUnit: String):URI = {
    val keyHSet = URIManager.generateHSetKey(currentPhase,Job(currentJob),currentComputationUnit,currentDataSet)
    val reportsURIs = URIManager.createListReportsURI(currentJob,currentDataSet,currentComputationUnit)
    val connectionData = chooseConnection()
    val connection = new RedisClient(connectionData._1,connectionData._2)
    connection.connect

    def insertReports(toBeProcessed: List[(String,URI)]): Unit = {
      if(toBeProcessed.nonEmpty){
        val data = readFromServer(toBeProcessed.head._2)
        if(data._1.isDefined){

          connection.hset(keyHSet,toBeProcessed.head._1,data._1.get)
          connection.hset(keyHSet,toBeProcessed.head._1+":dim",data._2.get)

        }
        insertReports(toBeProcessed.tail)
      }
    }
    insertReports(reportsURIs)
    URIManager.createFinalURI(keyHSet,connectionData._1,connectionData._2)
  }

  private def createList(uri: URI): List[(String,(Array[Byte],Int))] = {

    def createList(toBeProcessed: Map[String,String],dimMap: Map[String,String],results: List[(String,(Array[Byte],Int))]): List[(String,(Array[Byte],Int))] = {
      if(toBeProcessed.isEmpty) results
      else{
        val dimSearch = dimMap.find(_._1.contains(toBeProcessed.head._1))
        if(dimSearch.nonEmpty){
          val dim = dimSearch.head._2.toInt
          createList(toBeProcessed.tail,dimMap,(toBeProcessed.head._1,(DataEncoder.decode(toBeProcessed.head._2),dim))::results)
        } else {
          results
        }
      }
    }
    val dataConnection = URIManager.getConnectionInfoFromURI(uri)
    val keyData = URIManager.getKeyFromURI(uri)

    val connection = new RedisClient(dataConnection._1,dataConnection._2)
    connection.connect
    val mapResult = connection.hgetall(keyData)
    if(mapResult.isDefined){
      val dimMap = mapResult.get.filter(_._1.contains(":dim"))
      createList(mapResult.get -- dimMap.keySet,dimMap,Nil)
    }else Nil
  }


}

package it.necst

import scalaSci._

/**
 * Created by Andrea on 11/08/2015.
 */
package object marc {
    implicit def toRichDouble1DArray(from:Array[Double]):RichDouble1DArray = new RichDouble1DArray(from)
    implicit def toRichDouble1DArray(from:Vec):RichDouble1DArray = toRichDouble1DArray(toStandardDouble1DArray(from))
    implicit def toRichDouble2DArray(from:Array[Array[Double]]):RichDouble2DArray = new RichDouble2DArray(from)
    implicit def toRichDouble2DArray(from:Mat):RichDouble2DArray = toRichDouble2DArray(toStandardDouble2DArray(from))
    implicit def toRichDouble2DArray(from:Matrix):RichDouble2DArray = toRichDouble2DArray(toStandardDouble2DArray(from))
    implicit def toStandardDouble1DArray(from:RichDouble1DArray):Array[Double] = from.getv().array
    implicit def toStandardDouble1DArray(from:Vec):Array[Double] = from.getv()
    implicit def toStandardDouble2DArray(from:RichDouble2DArray):Array[Array[Double]] = from.toDoubleArray()
    implicit def toStandardDouble2DArray(from:Mat):Array[Array[Double]] = from.toDoubleArray()
    implicit def toStandardDouble2DArray(from:Matrix):Array[Array[Double]] = from.toDoubleArray()
}

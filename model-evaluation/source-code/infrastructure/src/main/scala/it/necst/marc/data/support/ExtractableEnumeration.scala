package it.necst.marc.data.support

import java.util.NoSuchElementException

import rapture.data.{Serializer, Extractor}

/**
 * Created by andreadamiani on 04/06/15.
 */
class ExtractableEnumeration extends Enumeration {
  implicit def enumerationExtractor[Data](implicit ext: Extractor[String, Data]): Extractor[Value,Data] = ext.map(unapply(_).get)
  implicit def enumerationSerializer[Data](implicit ser: Serializer[String, Data]): Serializer[Value,Data] = ser.contramap(_.toString)


  def unapply(value:String):Option[Value] =
    try
      Some(ExtractableEnumeration.this.withName(value))
    catch{
      case e:NoSuchElementException => None
    }
}

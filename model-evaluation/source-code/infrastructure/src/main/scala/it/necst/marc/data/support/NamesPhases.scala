package it.necst.marc.data.support

/**
  * Created by andrea on 08/11/15.
  */
object NamesPhases extends ExtractableEnumeration{

  val PHASE0        = Value("phase0")
  val PHASE1        = Value("phase1")
  val PHASE1POST    = Value("phase1post")
  val PHASE2A       = Value("phase2a")
  val PHASE2B       = Value("phase2b")
  val PHASE2C       = Value("phase2c")
  val PHASE3PRE     = Value("phase3pre")
  val PHASE3        = Value("phase3")
  val EXTERNAL      = Value("external")
  val REDIS         = Value("redis")
  val ENTRYPOINT    = Value("entry-point")

}
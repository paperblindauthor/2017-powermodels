package it.necst.marc.phase3.data

import it.necst.marc.data.Binding
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code

/**
 * Created by andreadamiani on 17/06/15.
 */
case class Result(simulations:List[Simulation])

case class Simulation(initialCode:Code, ttls:List[TTL])

case class TTL(level:Double, charge:Option[Double], discharge:Option[Double]){
  require(charge.isDefined || discharge.isDefined)
}

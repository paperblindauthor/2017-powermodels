package it.necst.marc.phase3.pre.data.internal

import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.data.support.{WildcardMap, Wildcard}
import it.necst.marc.data.support.WildcardMap._

/**
 * Created by andreadamiani on 02/11/15.
 */
class SimulationCodes(val codes:WildcardMap[Phase2Subphase.Value, SimulationCode]) extends Wildcard{
  require(Phase2Subphase.values.forall(codes.contains), "A CODE FOR EACH PHASE MUST BE DEFINED (EITHER FIXED OR *)")
  override def equalsWithWildcard(to:Any) = to match {
    case to:SimulationCodes => this.codes =*= to.codes
    case _ => false
  }

  override def toString = s"SimulationCodes($codes)"

  def canEqual(other: Any): Boolean = other.isInstanceOf[SimulationCodes]

  override def equals(other: Any): Boolean = other match {
    case that: SimulationCodes =>
      (that canEqual this) &&
        codes == that.codes
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(codes)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
object SimulationCodes{
  def apply(codes:Map[Phase2Subphase.Value, SimulationCode]):SimulationCodes = new SimulationCodes(codes)
  def apply(codes:(Phase2Subphase.Value, SimulationCode)*):SimulationCodes = apply(codes.toMap)
}

abstract sealed class SimulationCode extends Wildcard
object *** extends SimulationCode{
  override def equalsWithWildcard(to:Any) = to match {
    case x:SimulationCode => true
    case _ => false
  }
  override def toString = "***"
}
class Code(val code:it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code) extends SimulationCode{
  def equalsWithWildcard(to:Any) = to match {
    case x:Code => x.code == this.code
    case *** => true
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(code)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
  override def toString = s"Code($code)"

  def canEqual(other: Any): Boolean = other.isInstanceOf[Code]

  override def equals(other: Any): Boolean = other match {
    case that: Code =>
      (that canEqual this) &&
        code == that.code
    case _ => false
  }
}
object Code {
  def apply(code:it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code) = new Code(code)
  implicit def directApply(code:it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code):Code = apply(code)
}
package it.necst.marc.phase3.pre.data.support

import it.necst.marc.data.support.ExtractableEnumeration

/**
 * Created by andreadamiani on 04/11/15.
 */
object SimulationDirection extends ExtractableEnumeration {
  val CHARGE = Value("CHARGE")
  val DISCHARGE = Value("DISCHARGE")
}

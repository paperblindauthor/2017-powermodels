package it.necst.marc.phase3.pre.data.internal

import it.necst.marc.phase1.post.data.ConfigurationCode
import it.necst.marc.phase1.post.data.support.Phase2Subphase

/**
 * Created by andreadamiani on 02/11/15.
 */
class MergeConfiguration(mainCodes: List[ConfigurationCode], subCodes: List[ConfigurationCode]) {
  require(subCodes.nonEmpty, "EVERY SIMULATION MUST HAVE AT LEAST AN IDENTIFYING SUBCODE")
  val get = {
    val main = for(main<-mainCodes) yield main.subphase -> Code(main.code)
    val sub = for(sub<-Phase2Subphase.values.toList.filterNot(mainCodes.map(_.subphase) contains _)) yield sub -> ***
    SimulationCodes(main ::: sub:_*)
  }

  override def toString = s"MergeConfiguration($get)"
}

package it.necst.marc.phase3.pre.actors

import akka.actor.{Props, ActorRef}
import it.necst.marc.phase3.pre.data.Configuration
import it.necst.marc.phase3.pre.data.internal.SimulationCodes
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andrea on 31/10/15.
 */

object ActorCCharge{

  def props(simulationCodes: SimulationCodes,
            loadBalancerActorsB: ActorRef,
            configuration: Configuration): Props = Props(new ActorCCharge(simulationCodes, loadBalancerActorsB,configuration))

}

class ActorCCharge(simulationCodes: SimulationCodes,
                   loadBalancerActorsB: ActorRef,
                   configuration: Configuration) extends CSimulator(simulationCodes, loadBalancerActorsB,SimulationDirection.CHARGE,configuration){}

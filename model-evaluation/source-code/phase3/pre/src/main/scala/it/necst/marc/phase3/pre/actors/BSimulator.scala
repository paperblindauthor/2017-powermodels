package it.necst.marc.phase3.pre.actors

import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import akka.actor.{Actor, ActorRef}
import it.necst.marc.phase3.pre.computation.Simulation
import it.necst.marc.phase3.pre.data.internal.{Code, SimulationCodes}
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andrea on 31/10/15.
 */

object BSimulator{

}

abstract class BSimulator(simulationCodes: SimulationCodes,
                          collectorBSimulation: ActorRef,
                          dischargeSimulation: SimulationDirection.Value) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  protected val direction:String = "UNINIT"
  private val name = simulationCodes + " " + direction

  private var models: List[(Array[Long], Double)] = Nil
  require(simulationCodes.codes(Phase2Subphase.B).isInstanceOf[Code], "MALFORMED B SIMULATOR")
  private val codeB = simulationCodes.codes(Phase2Subphase.B).asInstanceOf[Code].code

  def receive: Receive = {
    case AddWeightedModelAC(data, frequency) =>
      logger.info(s"ACTOR B SIULATOR ($name)\nReceived AddWeightedModelAC message")
      models = if(data.isDefined) (data.get, frequency) :: models else models

    case StartSimulation()=>
      logger.info(s"ACTOR B SIMULATOR ($name)\nReceived Simulate message")
      try{
        val result = if (models.nonEmpty) Some(Simulation.groupSimulations(models)) else None
        logger.info(s"ACTOR B SIMULATOR ($name)\nMODEL AGGREGATION COMPUTED")
        collectorBSimulation ! CollectResults(dischargeSimulation, result, codeB)
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }

  }

}

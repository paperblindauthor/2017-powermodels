package it.necst.marc.phase3.pre.actors

import akka.actor.{ActorSystem, Actor, Props, ActorRef}
import it.necst.marc.data.support.{MarcLogger, ErrorStack, WildcardMap}
import it.necst.marc.phase1.post.data.ResultFrequencies
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase3.pre.computation.Simulation
import it.necst.marc.phase3.pre.data.internal.{Code, SimulationCodes}
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andrea on 31/10/15.
 */
object LoadBalancerActorsB{

  def props(waitingMessages: Int,
            collector: ActorRef,
            codes: List[SimulationCodes],
            simulationDirection: SimulationDirection.Value,
            sourceFrequencies: ResultFrequencies): Props = Props(new LoadBalancerActorsB(waitingMessages,collector,codes,simulationDirection, sourceFrequencies))

}

class LoadBalancerActorsB(waitingMessages: Int,
                          collector: ActorRef,
                          codes: List[SimulationCodes],
                          simulationDirection: SimulationDirection.Value,
                          sourceFrequencies: ResultFrequencies) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private val system = ActorSystem("LoadBalancerActorsB")

  private var waitingActors: Int = 0
  private var mapActors: WildcardMap[SimulationCodes,ActorRef] = WildcardMap()

  override def preStart() = {
    waitingActors = waitingMessages

    mapActors = createMappingCodesActors
  }

  def receive: Receive = {

    case AddModelAC(data, code) =>
      logger.info("Received AddModelAC message")
      try{
        waitingActors = waitingActors - 1
        logger.info(s"LOAD BALANCER B - STILL WAITING FOR ${waitingActors} ACTOR C TO COMPLETE")

        val myCode = code.codes(Phase2Subphase.C).asInstanceOf[Code].code
        val codesAndFreqs = Simulation.computeMergeSites(Phase2Subphase.C, myCode, sourceFrequencies)
        sendAddWeightedModelAC(data, codesAndFreqs)

        if(waitingActors == 0){
          sendRequestsSimulations()
        }
      } catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }


    case Deallocate() =>
      for(actor <- mapActors)
        system.stop(actor._2)
      context.stop(self)

  }

  private def createMappingCodesActors(): Map[SimulationCodes,ActorRef] = {

    def createChargeActors(toBeProcessed: List[SimulationCodes],map: Map[SimulationCodes,ActorRef], index: Int): Map[SimulationCodes,ActorRef] = {
      if(toBeProcessed.isEmpty) map
      else{
        val actorRef = system.actorOf(ActorBCharge.props(toBeProcessed.head, collector),"actorBCharge"+index)
        createChargeActors(toBeProcessed.tail,map + (toBeProcessed.head -> actorRef),index+1)
      }
    }

    def createDischargeActors(toBeProcessed: List[SimulationCodes],map: Map[SimulationCodes,ActorRef], index: Int): Map[SimulationCodes,ActorRef] = {
      if(toBeProcessed.isEmpty) map
      else{
        val actorRef = system.actorOf(ActorBDischarge.props(toBeProcessed.head, collector),"actorBCharge"+index)
        createDischargeActors(toBeProcessed.tail,map + (toBeProcessed.head -> actorRef),index+1)
      }
    }

    if(simulationDirection.equals(SimulationDirection.CHARGE)) createChargeActors(codes,Map(),0) else createDischargeActors(codes,Map(),0)
  }

  private def sendAddWeightedModelAC(data:Option[Array[Long]], code: List[(SimulationCodes, Double)]) = {
    val destinationsAndFreq = code.map(x => (mapActors(x._1), x._2))

    def send(toBeProcessed: List[(ActorRef, Double)]): Unit = {
      if(toBeProcessed.nonEmpty){
        toBeProcessed.head._1 ! AddWeightedModelAC(data, toBeProcessed.head._2)
        send(toBeProcessed.tail)
      }
    }
    send(destinationsAndFreq)

  }

  private def sendRequestsSimulations() = {

    def send(toBeProcessed: List[ActorRef]): Unit = {
      if(toBeProcessed.nonEmpty){
        toBeProcessed.head ! StartSimulation()
        send(toBeProcessed.tail)
      }
    }
    val actors = (for(item <- mapActors) yield item._2).toList
    send(actors)
  }

}

package it.necst.marc.phase3.pre.actors

import akka.actor.{Props, ActorRef}
import it.necst.marc.phase3.pre.data.internal.SimulationCodes
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andrea on 31/10/15.
 */

object ActorBCharge{

  def props(simulationCodes: SimulationCodes, collectorBSimulations: ActorRef): Props = Props(new ActorBCharge(simulationCodes, collectorBSimulations))
}

class ActorBCharge(simulationCodes: SimulationCodes, collectorBSimulations: ActorRef) extends BSimulator(simulationCodes, collectorBSimulations, SimulationDirection.CHARGE){
  override protected val direction:String = "CHARGE"
}

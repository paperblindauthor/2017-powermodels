package it.necst.marc.phase3.pre.data.internal

import it.necst.marc.data.support.ExtractableEnumeration

/**
  * Created by andrea on 03/01/16.
  */

object DataManagementStats extends ExtractableEnumeration{

  val DATA_DECOMPRESSION           = Value("DataDecompression")
  val DATA_COMPRESSION             = Value("DataCompression")

}
package it.necst.marc.phase3.pre.computation

import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.{ConfigurationCode, ResultFrequencies}
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase2.a.data.{Alpha, Model}
import it.necst.marc.phase2.c.data.Value
import it.necst.marc.phase3.pre.data.Configuration
import it.necst.marc.phase3.pre.data.internal.{***, SimulationCodes, MergeConfiguration}
import it.necst.marc.phase3.pre.data.support.SimulationDirection

import scala.annotation.tailrec

/**
 * Created by andreadamiani on 27/10/15.
 */
object Simulation {
  def computeMergeConfigurations(frequencies: ResultFrequencies) = {
    (for {
      phase <- Phase2Subphase.values.toList
      frequenciesOfPhaseOpt = frequencies.byPhase.find(_.phase == phase)
      void = require(frequenciesOfPhaseOpt.isDefined, "MALFORMED FREQUENCY RESULT.")
      frequenciesOfPhase = frequenciesOfPhaseOpt.get.frequencies
      mainPhases = Phase2Subphase.groupingRules(phase)
      subPhases = Phase2Subphase.values.filterNot(mainPhases contains _)
      frequency <- frequenciesOfPhase
    } yield {
      phase -> new MergeConfiguration(frequency.configuration.filter(mainPhases contains _.subphase), frequency.configuration.filter(subPhases contains _.subphase))
    }) groupBy {_._1} map {case (k,v) => k -> v.map(_._2).toSet}
  }

  def computeMergeSites(sourcePhase:Phase2Subphase.Value, sourceCode:Code, sourceFrequencies:ResultFrequencies) = {
    val freqOfInterestOpt = sourceFrequencies.byPhase.find(_.phase == sourcePhase)
    require(freqOfInterestOpt.isDefined, "MALFORMED FREQUENCY RESULT")
    val freqOfInterest = freqOfInterestOpt.get.frequencies
    val mergeResultEntries = freqOfInterest.filter(_.configuration contains ConfigurationCode(sourcePhase, sourceCode))
    val mergeEntries = for (entry <- mergeResultEntries)
      yield entry.configuration.map(x => x.subphase -> it.necst.marc.phase3.pre.data.internal.Code(x.code)) -> entry.frequency
    mergeEntries.map{x =>
      val codeList = (for(phase <- Phase2Subphase.values if !(x._1.map(_._1) contains phase)) yield phase -> ***).toList ::: x._1
      SimulationCodes(codeList:_*) -> x._2}
  }

  def computeMeanAlphas(models:List[(Model, Double)]) = {
    val intermediateMap = (for{
      model <- models
      alpha <- model._1.alphas
      relativeFrequency = model._2
    } yield (alpha.feature, alpha.lag) -> alpha.alpha*relativeFrequency).groupBy(_._1).map(e => e._1 -> e._2.map(_._2))
    val cumulativeVals = intermediateMap.map{case (k,v) => k -> v.sum}
    (for (mean <- cumulativeVals) yield Alpha(mean._1._1, mean._1._2, mean._2)).toList
  }

  def simulateModel(direction:SimulationDirection.Value, alphas:List[Alpha], environment:List[Value], configuration: Configuration) = {
    val levels = configuration.levels(direction)

    val exogenPart = (for{
      exogen <- environment
      mean = exogen.mean
      feature = exogen.feature
      relatedAlphas = alphas.filter(_.feature == feature)
      void = require(relatedAlphas.nonEmpty, "MALFORMED PHASE2A&C DATA - MISSING ALPHA FOR ENV VARIABLE")
      related <- relatedAlphas
    } yield mean*related.alpha).sum

    val initialConfigurationOpt = configuration.initial_configurations.get(direction)

    if(initialConfigurationOpt.isDefined) {
      val initialConfiguration = initialConfigurationOpt.get
      @tailrec def recursiveSimulation(old: List[Double], recent: List[Double], tick: Long, partial:List[Long]): Option[Array[Long]] = {
        def pushToOld() = {
          recent.last :: old
        }

        if (tick > configuration.limit) {
          None
        } else {
          val regressivePart = (for (regressive <- alphas.filter(_.feature == configuration.output)) yield regressive.alpha * recent(regressive.lag - 1)).sum
          val output = regressivePart + exogenPart

          val levelJumped = ((direction match {
            case SimulationDirection.CHARGE => output - levels.get(partial.length-1)
            case SimulationDirection.DISCHARGE => levels.get(partial.length-1) - output
          })/configuration.levelSize(direction).get).floor.toInt

          val newPartial = if(levelJumped>0){
            def recursivePartialGeneration(partial:List[Long], remainder:Double, remainingLevels:Int):List[Long] = {
              if(remainingLevels <= 0 || partial.length >= 101) partial
              else {
                val ticks = (remainder/remainingLevels).floor
                recursivePartialGeneration(ticks.toLong :: partial, remainder-ticks, remainingLevels-1)
              }
            }
            recursivePartialGeneration(partial, (tick - partial.sum).toDouble, levelJumped)
          } else {
            partial
          }

          if(newPartial.length <= 100) {
            recursiveSimulation(pushToOld(), (output :: recent).init, tick + 1, newPartial)
          } else {
            Some((direction match {
              case SimulationDirection.CHARGE => newPartial.reverse
              case SimulationDirection.DISCHARGE => newPartial
            }).toArray)
          }
        }
      }
      recursiveSimulation(Nil, initialConfiguration.initial_values, 1, 0 :: Nil)
    } else {
      None
    }
  }

  def groupSimulations(simulations:List[(Array[Long], Double)]) = {
    val totalWeight = simulations.map(_._2).sum
    val inverted = (for(i <- 0 to 100) yield for (simulation <- simulations) yield simulation._1(i)*simulation._2).toArray
    inverted.map(x => (x.sum/totalWeight).round)
  }
}

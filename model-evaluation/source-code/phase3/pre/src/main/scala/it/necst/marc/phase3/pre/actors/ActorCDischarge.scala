package it.necst.marc.phase3.pre.actors

import akka.actor.{Props, ActorRef}
import it.necst.marc.phase3.pre.data.Configuration
import it.necst.marc.phase3.pre.data.internal.SimulationCodes
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andrea on 31/10/15.
 */

object ActorCDischarge{

  def props(simulationCodes: SimulationCodes,
            loadBalancerActorsB: ActorRef,
            configuration: Configuration): Props = Props(new ActorCDischarge(simulationCodes,loadBalancerActorsB,configuration))
}

class ActorCDischarge(simulationCodes: SimulationCodes,
                      loadBalancerActorsB: ActorRef,
                      configuration: Configuration) extends CSimulator(simulationCodes,loadBalancerActorsB,SimulationDirection.DISCHARGE,configuration){}

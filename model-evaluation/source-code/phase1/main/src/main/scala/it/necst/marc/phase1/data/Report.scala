package it.necst.marc.phase1.data

import it.necst.marc.data.{Performance, AbstractReport}

/**
 * Created by andreadamiani on 01/07/15.
 */
case class Report(content:Option[MathStats], error:Option[String]) extends AbstractReport(content, error)

case class MathStats(amended:List[Entry],
                     excluded:List[Entry],
                     out_of_bound:List[Entry],
                     granularity: List[Entry],
                     performance: Performance) extends it.necst.marc.data.MathStats

case class Entry(rule:String, rows:Long)

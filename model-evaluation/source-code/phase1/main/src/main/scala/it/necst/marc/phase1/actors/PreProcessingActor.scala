package it.necst.marc.phase1.actors

import akka.actor.{Actor, Props}
import it.necst.marc.data.support.ErrorStack
import it.necst.marc.phase1.computation.preprocessing.Preprocessing
import it.necst.marc.phase1.data.{Entry}

object PreProcessingActor {

   def props(config: it.necst.marc.phase1.data.internal.Configuration): Props = Props(new PreProcessingActor(config))

}

class PreProcessingActor(config: it.necst.marc.phase1.data.internal.Configuration) extends Actor {


  def receive: Receive = {
    case PreProcessData(vec, index,replyTo) =>
      try{
        val coherenceCorrectionResult = Preprocessing.coherenceCorrection(vec,config.coherenceRules)
        val amendedList = if(coherenceCorrectionResult._2.isDefined){
          coherenceCorrectionResult._2.get.map{x => Entry(x.internalToString,1)}
        }else{
          Nil
        }
        val afterIncoherentMaskResult = Preprocessing.incoherentMask(coherenceCorrectionResult._1, config.coherenceRules)
        if (afterIncoherentMaskResult._1.isEmpty) {
          val listIncoherents = Entry(afterIncoherentMaskResult._2.get.internalToString,1) :: Nil
          replyTo ! ProcessedComplete(None, index, amendedList,listIncoherents,Nil,Nil)
        } else {
          val afterOutOfBoundMaskResult =
            Preprocessing.outOfBoundMask(afterIncoherentMaskResult._1.get,config.featureMappings,config.bounds)
          if (afterOutOfBoundMaskResult._1.isEmpty) {
            val listOutOfBound = Entry(afterOutOfBoundMaskResult._2.get,1) :: Nil
            replyTo ! ProcessedComplete(None, index,amendedList,Nil,listOutOfBound,Nil)
          } else {
            val afterGranularityReductionResult =
              Preprocessing.granularityReduction(afterOutOfBoundMaskResult._1.get,config.reductions)
            val granularityReductions = if(afterGranularityReductionResult._2.isDefined){
              afterGranularityReductionResult._2.get.map{x => Entry(x.internalToString,1)}
            }else{
              Nil
            }
            replyTo ! ProcessedComplete(Option(afterGranularityReductionResult._1), index,amendedList,Nil,Nil,granularityReductions)
          }
        }
      } catch {
        case e:Throwable =>
          val error = ErrorStack(e)
          replyTo ! ThrowsError(error)
      }

  }
}
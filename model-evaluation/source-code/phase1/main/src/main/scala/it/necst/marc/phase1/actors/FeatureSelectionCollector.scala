package it.necst.marc.phase1.actors

import akka.actor.{Props, ActorRef, Actor}
import akka.routing.BalancingPool
import it.necst.marc.data.PartialStat
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.data.support.representability._
import it.necst.marc.phase1.computation.featureSelection.FeatureSelection
import it.necst.marc.phase1.data.internal.TupleOrdering
import it.necst.marc.phase1.data.internal.{FeatureSelectionStats, Configuration}
import it.necst.marc.phase1.data.internal.featureSelection.FusionRule

import scala.collection.SortedSet
import scalaSci.RichDouble2DArray

/**
  * Created by andrea on 10/12/15.
  */

object FeatureSelectionCollector{

  def props(dataset: RichDouble2DArray,
            numberOfActors: Int,
            internalConfiguration: Configuration): Props = Props(new FeatureSelectionCollector(dataset,numberOfActors,internalConfiguration))

}

class FeatureSelectionCollector(dataset: RichDouble2DArray,
                               numberOfActors: Int,
                                internalConfiguration: Configuration) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  def receive = initState

  private var waiting: Int = 0
  private var routerActorRef: ActorRef = _
  private var caller: ActorRef = _
  private var resultMap: Map[Int,Array[Double]] = _

  private var initTimestamp: Long = 0l
  private var firstRequestSent: Long = 0l
  private var allResponsesReceived: Long = 0l
  private var resultCreated: Long = 0l

  override def preStart() = {
    waiting = numberOfActors
    resultMap =  Map ()
  }

  def initState: Receive = {
    case StartFeatureSelection() =>
      logger.info("Received StartFeatureSelection message")
      try{
        caller = sender()
        initTimestamp = System.currentTimeMillis()

        if(internalConfiguration.fusions.isEmpty){
          //No fusion required
          val res = getFinalDataAndStats
          caller ! FeatureSelectionCompleted(res._1,res._2)
        } else {
          //Distributed parallel fusion required
          routerActorRef = context.system.actorOf(BalancingPool(numberOfActors).props(FeatureSelectionActor.props(dataset)),name = "featureSelectionActor")
          context.become(collectResultsState)
          sendComputationRequest()
        }
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          context.stop(routerActorRef)
          sender() ! ThrowsError(error)

      }

  }

  def collectResultsState: Receive = {
    case FeatureSelectionDone(result)=>
      try{
        resultMap = resultMap ++ result
        waiting = waiting - 1

        if(waiting == 0){

          val res = getFinalDataAndStats

          caller ! FeatureSelectionCompleted(res._1,res._2)
        }
      }catch{
        case e:Throwable =>
          val error = ErrorStack(e)
          context.stop(routerActorRef)
          caller ! ThrowsError(error)
      }

    case ThrowsError(error) =>
      context.stop(routerActorRef)
      caller ! ThrowsError(error)
  }

  private def sendComputationRequest() = {

    def send(toBeProcessed: List[R[FusionRule]]): Unit = {
      if(toBeProcessed.nonEmpty){
        routerActorRef ! DoFeatureSelection(toBeProcessed.head :: Nil)
        send(toBeProcessed.tail)
      }
    }
    firstRequestSent = System.currentTimeMillis()
    send(internalConfiguration.fusions)
  }

  private def getFinalDataAndStats = {
    allResponsesReceived = System.currentTimeMillis()

    val finalData = FeatureSelection(dataset,resultMap,internalConfiguration.selectedColumnRanges)
    resultCreated = System.currentTimeMillis()

    val stats = PartialStat(FeatureSelectionStats.FIRST_REQUEST_SENT.toString,firstRequestSent-initTimestamp) ::
      PartialStat(FeatureSelectionStats.ALL_RESPONSES_RECEIVED.toString,allResponsesReceived-initTimestamp) ::
      PartialStat(FeatureSelectionStats.FEATURE_SELECTION_COMPLETED.toString,resultCreated-initTimestamp) :: Nil

    (finalData, stats)
  }
}

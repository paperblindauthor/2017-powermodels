package it.necst.marc.phase1.actors

import akka.actor.{ActorSystem, Actor, Props, ActorRef}
import com.typesafe.config.Config
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.infrastructure.actors.messages.{ComputationError, InternalComputationCompleted, StartInternalComputation}
import it.necst.marc.infrastructure.support.{DataEncoder, DataCompressor, TraitInternalActor}
import it.necst.marc.phase1.computation.CategoricalExpansion
import it.necst.marc.phase1.computation.dataManipulation.DataManipulation
import it.necst.marc.phase1.data.MathStats
import it.necst.marc.phase1.data._
import it.necst.marc.data._
import it.necst.marc.phase1.data.internal.{DataManagementStats, DataManipulationStats}
import rapture.json._
import rapture.json.jsonBackends.jawn._
import scala.collection.SortedSet
import scalaSci.RichDouble2DArray
import java.net.URI


/**
 * Created by andrea on 01/06/15.
 */
object InternalWorker extends TraitInternalActor{

  def  props(currentConfiguration: String,actorToReply: ActorRef,mapData: Map[URI,(Array[Byte],Int)],configFile: Config): Props = Props(new InternalWorker(currentConfiguration,actorToReply,mapData,configFile))

}


class InternalWorker(val currentConfiguration:String,
                     val actorToReply: ActorRef,
                     val mapData:Map[URI,(Array[Byte],Int)],
                     val configFile: Config) extends Actor{


  private val system = ActorSystem("InternalWorker")

  private val logger = MarcLogger(getClass.getName)

  private var currentInternalConfiguration: it.necst.marc.phase1.data.internal.Configuration = _
  private var featuresList: List[it.necst.marc.data.Feature] = Nil
  private var startTimestamp: Long = 0l
  private var partialStats: List[PartialStat] = Nil
  private var amendedList: List[Entry] = Nil
  private var excludedList: List[Entry] = Nil
  private var outOfBoundList: List[Entry] = Nil
  private var granularityList: List[Entry] = Nil
  private var phaseConfiguration: Configuration = _
  private var timeOrdered:Boolean = false

  def receive = startComputationState

  def startComputationState: Receive = {
    case StartInternalComputation() =>
      logger.info("Start internal computation")
      try{
        startTimestamp = System.currentTimeMillis()
        phaseConfiguration = Json.parse(currentConfiguration).as[it.necst.marc.phase1.data.Configuration]
        val stringData = new String(DataCompressor.decompress(mapData.head._2._1,mapData.head._2._2))
        val dataForPhase = Json.parse(stringData).as[it.necst.marc.data.ResultExternalRedis]
        val finishDataDecompression = System.currentTimeMillis()

        partialStats = PartialStat(DataManagementStats.DATA_DECOMPRESSION.toString,finishDataDecompression-startTimestamp) :: partialStats

        val timeFeature = phaseConfiguration.features.filter(_.name.equals(phaseConfiguration.timeFeature)).head
        val newTimeFeature = it.necst.marc.data.Feature(timeFeature.name,timeFeature.column_index,Option(true),timeFeature.specs)
        featuresList = phaseConfiguration.features.filter(!_.name.equals(phaseConfiguration.timeFeature)) :+ newTimeFeature

        currentInternalConfiguration = it.necst.marc.phase1.data.internal.Configuration(featuresList,phaseConfiguration)
        startPreProcessing(dataForPhase.dataForProcessing,currentInternalConfiguration)
        context.become(preProcessingCompleteState)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          system.terminate()
          context.stop(self)
      }

    case ThrowsError(error) =>
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      system.terminate()
      context.stop(self)

  }

  def preProcessingCompleteState: Receive = {
    case PreProcessingComplete(data,preProcessingStatistic,amended,excluded,out_of_bound,granularity)=>
      logger.info("PreProcessing completed")
      try{
        featuresList = for (feat <- featuresList) yield {
          val reductionRule = phaseConfiguration.reduction.getOrElse(Nil).find(_.feature == feat.name)
          if(reductionRule.isDefined){
            val steps = reductionRule.get.steps
            val intervals = reductionRule.get.intervals
            val upperBound =
              if(steps.isDefined){
                Some((steps.get - 1).toDouble)
              } else if(intervals.isDefined) {
                Some((intervals.get.size).toDouble)
              } else {
                None
              }
            Feature(feat.name, feat.column_index, feat.is_time.getOrElse(false), FeatureType.CATEGORICAL, FeatureMonotony.NONE, (Some(0.0), upperBound))
          } else {
            feat
          }
        }
        currentInternalConfiguration = it.necst.marc.phase1.data.internal.Configuration(featuresList,phaseConfiguration,true)
        amendedList = amended
        excludedList = excluded
        outOfBoundList= out_of_bound
        granularityList = granularity
        val initTimeDataManipulation = System.currentTimeMillis()

        val datasetManipulated = DataManipulation(data,currentInternalConfiguration.manipulations,currentInternalConfiguration.features)
        partialStats = PartialStat(DataManipulationStats.DATA_MANIPULATION_COMPLETED.toString,System.currentTimeMillis()-initTimeDataManipulation) :: preProcessingStatistic

        val dataManipulated = datasetManipulated.dataset
        val newMappings = datasetManipulated.mappings
        val columnsAdded = newMappings.size - currentInternalConfiguration.features.size
        currentInternalConfiguration = it.necst.marc.phase1.data.internal.Configuration(newMappings, phaseConfiguration, columnsAdded,true)
        timeOrdered = datasetManipulated.isTimeSorted

        val numberOfActors = currentInternalConfiguration.fusions.length
        context.become(featureSelectionCompleteState)
        val collectorFeatureSelection = system.actorOf(FeatureSelectionCollector.props(dataManipulated,numberOfActors,currentInternalConfiguration),"featureSelectionCollector")
        collectorFeatureSelection ! StartFeatureSelection()

      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          system.terminate()
          context.stop(self)

      }

    case ThrowsError(error) =>
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      system.terminate()
      context.stop(self)

  }

  def featureSelectionCompleteState: Receive = {
    case FeatureSelectionCompleted(dataset,stats) =>
      try{

        val newFeatureList = currentInternalConfiguration.features.filter(isFeatureToKeep) ++ currentInternalConfiguration.fusions.map{ x => x.obj.destination}
        val updatedFeaturesStep1 = updateFeaturesIndexes(newFeatureList,currentInternalConfiguration.selectedColumnRanges)

        val dataAndFeats = if(updatedFeaturesStep1.exists(_.specs.feature_type == FeatureType.CATEGORICAL)){
          CategoricalExpansion(dataset, updatedFeaturesStep1)
        } else {
          (dataset, updatedFeaturesStep1)
        }

        val data = dataAndFeats._1
        val updatedFeatures = dataAndFeats._2

        val listData = for (line <- data.getv()) yield {
          line.toList
        }
        val timestamp = System.currentTimeMillis()

        val resultString = Json(Result(updatedFeatures,listData.toList, timeOrdered)).toString
        val resultByte = DataCompressor.compress(resultString)
        val resultDim = DataEncoder.byteLength(resultString)
        val finishDataManagementTimestamp = System.currentTimeMillis()
        val dataCompressionStats = PartialStat(DataManagementStats.DATA_COMPRESSION.toString,finishDataManagementTimestamp-timestamp)
        val performance = Performance(Some(PerfStats(timestamp-startTimestamp,Some(PartialStats((partialStats :+ dataCompressionStats):::stats)))))
        val reportString = Json(Report(Some(MathStats(amendedList,excludedList,outOfBoundList,granularityList,performance)),None)).toString
        val reportByte = DataCompressor.compress(reportString)
        val reportDim = DataEncoder.byteLength(reportString)

        actorToReply ! InternalComputationCompleted(reportByte,reportDim,resultByte,resultDim)
        system.terminate()
        context.stop(self)
      } catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          system.terminate()
          context.stop(self)

      }


    case ThrowsError(error) =>
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      system.terminate()
      context.stop(self)
  }


  private def startPreProcessing(data: RichDouble2DArray, configuration: it.necst.marc.phase1.data.internal.Configuration): Unit ={

    val phaseName = configFile.getString("name")
    val numberOfCommunicationActors = configFile.getInt(phaseName+".number_preprocessing_actors")
    val preProcessingCollector = system.actorOf(PreProcessingCollector.props(data,numberOfCommunicationActors,configuration), "PreprocessingCollector")
    preProcessingCollector ! StartCollectorComputation()

  }

  private def updateFeaturesIndexes(features: List[Feature], computedColumnRanges: SortedSet[(Int,Int)]): List[Feature] = {

    def createMapIntervalHowMuchToBeRemoved(toBeProcessed: SortedSet[(Int,Int)],
                                            previousInterval: (Int,Int),
                                            toRemove: Int,
                                            currentMap: Map[(Int,Int),Int]): Map[(Int,Int),Int] = {

      if(toBeProcessed.isEmpty) currentMap
      else{

        val dataNewIteration = if(previousInterval != null){
          val value = (toBeProcessed.head._1 - previousInterval._2 - 1) + toRemove
          (currentMap + (toBeProcessed.head -> value),value)

        }else{
          (currentMap + (toBeProcessed.head -> 0),toRemove)
        }

        createMapIntervalHowMuchToBeRemoved(toBeProcessed.tail,toBeProcessed.head,dataNewIteration._2,dataNewIteration._1)
      }
    }

    def findCurrentInterval(toBeProcessed: SortedSet[(Int,Int)], index: Int): (Int,Int)= {
      if(toBeProcessed.head._1<= index && toBeProcessed.head._2 >= index) toBeProcessed.head
      else findCurrentInterval(toBeProcessed.tail,index)
    }

    def updateIndexes(toBeProcessed: List[Feature], updatedFeatures: List[Feature], toRemove: Map[(Int,Int),Int]): List[Feature] = {
      if(toBeProcessed.isEmpty) updatedFeatures

      else{

        val interval = findCurrentInterval(computedColumnRanges,toBeProcessed.head.column_index)
        val newIndex = toBeProcessed.head.column_index - toRemove(interval)

        val newFeaturesList = updatedFeatures :+ Feature(toBeProcessed.head.name,newIndex,toBeProcessed.head.is_time,toBeProcessed.head.specs)

        updateIndexes(toBeProcessed.tail,newFeaturesList,toRemove)
      }
    }

    val toRemoveMap = createMapIntervalHowMuchToBeRemoved(computedColumnRanges,null,0,Map[(Int,Int),Int]())
    updateIndexes(features,Nil,toRemoveMap)

  }


  private def isFeatureToKeep(feature: Feature) = !(
    currentInternalConfiguration.excluded.contains(feature.name) ||
      (feature.specs.expanded_from match {
        case Some(ef) => currentInternalConfiguration.excluded.contains(ef.original_name)
        case None => false
      }))


}

package it.necst.marc.phase1.computation.dataManipulation

import it.necst.marc.data.Feature
import it.necst.marc.infrastructure.support.CorruptionException

import scala.annotation.tailrec
import scalaSci.RichDouble2DArray
import it.necst.marc.data.support.representability._

/**
 * Created by andrea on 24/08/15.
 */
object DataManipulation {

  def apply(dataset: RichDouble2DArray, manipulationsList: List[(R[Operation],Map[Operation.AttributeName,String])], features: List[Feature]): DataManipulationResult = {

    @tailrec def applyAll(dataset: RichDouble2DArray, toBeProcessed: List[(R[Operation],Map[Operation.AttributeName,String])], features: List[Feature]):(RichDouble2DArray,List[Feature]) = {
      if(toBeProcessed.isEmpty) (dataset,features)
      else {
        val result = toBeProcessed.head._1.implementation(dataset,toBeProcessed.head._2,features)
        applyAll(result._1,toBeProcessed.tail,result._2)
      }
    }

    val requiredOperations = manipulationsList.map(_._1).map(_.preconditions).fold(OperationRequirements.NONE){_ + _}
    val timeOrdered = requiredOperations.requirements.contains(TIME_SORTING)

    def applyPreconditions(resultDataset: RichDouble2DArray, resultFeatures:List[Feature], toBeProcessed:Set[OperationRequirement]):(RichDouble2DArray, List[Feature]) = {
      if(toBeProcessed.isEmpty) (resultDataset,resultFeatures)
      else {
        val currentCondition = toBeProcessed.head
        val result = currentCondition.implementation(resultDataset, Map(), resultFeatures)
        applyPreconditions(result._1, result._2, toBeProcessed.tail)
      }
    }

    val preconditionedInput = applyPreconditions(dataset, features, requiredOperations.requirements)

    val res = applyAll(preconditionedInput._1,manipulationsList,preconditionedInput._2)

    if(res._1.getv().flatten.contains(Double.NaN)) throw new CorruptionException("Data Manipulation causes dataset corruption.")
    else DataManipulationResult(res._1, res._2, timeOrdered)
  }
}

case class DataManipulationResult(dataset:RichDouble2DArray, mappings:List[Feature], isTimeSorted:Boolean)

package it.necst.marc.phase1.actors

import akka.actor.{Actor, Props}
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.phase1.computation.CategoricalExpansion

/**
  * Created by andreadamiani on 02/02/16.
  */
object CategoricalExpansionActor {
  def props():Props = Props(new CategoricalExpansionActor())
}

class CategoricalExpansionActor() extends Actor {

  private val logger = MarcLogger(getClass.getName)
  override def receive: Receive = {
    case ExpandCategorical(columnData, feature) =>
      try {
        logger.debug("Receive ExpandCategorical message")
        val result = CategoricalExpansion(columnData, feature)
        logger.debug("CATEGORICAL FINISHED")
        sender() ! CategoricalExpanded(result._1, result._2)
      } catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          throw new IllegalArgumentException(error)
          context.stop(self)
      }
  }
}

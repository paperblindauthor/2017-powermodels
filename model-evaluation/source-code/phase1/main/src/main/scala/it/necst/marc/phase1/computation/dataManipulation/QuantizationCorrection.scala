package it.necst.marc.phase1.computation.dataManipulation

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern._
import akka.util.Timeout
import it.necst.marc.data._
/*import it.necst.marc.phase1.actors.{StartQuantizationCorrection, QuantizationCorrectionCollector}*/
import it.necst.marc.phase1.computation.CategoricalExpansion
import it.necst.marc.phase1.computation.dataManipulation.Operation.AttributeName

import scala.annotation.tailrec
import scala.concurrent.Await
import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
  * Created by andreadamiani on 01/02/16.
  */
object QuantizationCorrectionImplementation extends OperationImplementation{

  final val EXCLUDED = "excluded"
  final val OUTPUT_FEATURE = "outputFeatureAttr"
  final val TIME_SPLITTING_DIFF = "timeSplittingDiffAttr"

  override def apply(dataset: RichDouble2DArray, attrs: Map[AttributeName, String], features: List[Feature]): (RichDouble2DArray, List[Feature]) = {

    val timeSplittingDiff = attrs.get(TIME_SPLITTING_DIFF) match {
      case Some(value) =>
        value.toDouble
      case None =>
        throw new IllegalArgumentException("No time splitting diff defined.")
    }

    val outputFeatureName = attrs(OUTPUT_FEATURE)
    require(outputFeatureName!=null && !outputFeatureName.isEmpty, s"OUTPUT_FEATURE ($OUTPUT_FEATURE) attribute not defined.")

    val timeFeatureIndex = features.find(_.is_time.getOrElse(false)) match {
      case Some(feature) =>
        feature.column_index
      case None =>
        throw new IllegalArgumentException("No time feature defined.")
    }

    val outputFeatureIndex = features.find(_.name == outputFeatureName) match {
      case Some(feature) =>
        feature.column_index
      case None =>
        throw new IllegalArgumentException(s"OUTPUT_FEATURE=$outputFeatureName is not a feature.")
    }
    val featureMappings = (for(f <- features if !f.is_time.getOrElse(false)) yield f.name -> f.column_index).toMap
    val excludedByName = attrs.getOrElse(EXCLUDED,Nil).toString.split(",").flatMap(f => CategoricalExpansion.obtainExpandedFeatures(f, features)).map(_.name)
    val featurePartition = featureMappings.partition{case (k,v) => excludedByName.contains(k)}

    if(dataset.numRows()>0){
      var initOutI = 0
      var initOut = dataset(initOutI, outputFeatureIndex)
      var currMonotony = FeatureMonotony.DESCENDANT
      var currentT = dataset(0, timeFeatureIndex)
      for(i <- 1 until dataset.numRows()){
        val t = dataset(i, timeFeatureIndex)
        val out = dataset(i, outputFeatureIndex)
        val monotony = dataset(i, outputFeatureIndex) - dataset(i-1,outputFeatureIndex) match{
          case x if x>0 => FeatureMonotony.ASCENDANT
          case x if x<0 => FeatureMonotony.DESCENDANT
          case _ => currMonotony
        }
        if(initOut != out){
          if(t - currentT <= timeSplittingDiff) {
            //LINEAR REGRESSION ON OUTPUT
            val delta = out - initOut
            val deltaT = dataset(i,timeFeatureIndex) - dataset(initOutI,timeFeatureIndex)
            val increment = delta / deltaT
            for (ii <- initOutI until i) {
              dataset.update(ii, outputFeatureIndex, initOut + increment * (dataset(ii,timeFeatureIndex) - dataset(initOutI,timeFeatureIndex)))
            }
          }
          initOutI = i
          initOut = out
        }
        if(t - currentT <= timeSplittingDiff && monotony == currMonotony){
          //CUMULATE
          for(j <- featurePartition._2.values){
            dataset.update(i, j, dataset(i-1,j) + dataset(i,j))
          }
        }
        currMonotony = monotony
        currentT = t
      }
    }

    val featureToNotBeTouched = featurePartition._1.keys.toList
    val newFeatures = for(f <- features) yield{
      if(featureToNotBeTouched.contains(f.name) || f.is_time.getOrElse(false)) f
      else Feature(f.name, f.column_index, f.is_time, FeatureSpec(FeatureType.CUMULATIVE, f.specs.expanded_from, FeatureMonotony.NONE, Bounds(None, None)))
    }

    (dataset, newFeatures)
  }
}
/*object QuantizationCorrectionImplementation extends OperationImplementation{
  override def apply(dataset: RichDouble2DArray, attrs: Map[AttributeName, String], features: List[Feature]):(RichDouble2DArray, List[Feature]) = {
    val featureMappings = (for(f <- features if !f.is_time.getOrElse(false)) yield f.name -> f.column_index).toMap
    val numberOfActor = 10
    val featurePartition = featureMappings.partition{case (k,v) => attrs.getOrElse("excluded",Nil).toString.split(",").flatMap(f => CategoricalExpansion.obtainExpandedFeatures(f, features)).map(_.name).contains(k)}
    val featureToNotBeTouched = featurePartition._1.keys.toList
    val featuresToBeQuantitazed = featurePartition._2.keys.toList
    val actor = ActorSystem("QuantizationActors").actorOf(QuantizationCorrectionCollector.props(dataset,featureMappings,numberOfActor))
    implicit val timeout =  Timeout(10,TimeUnit.MINUTES)
    val newDataset = try{
      Await.result(actor ? StartQuantizationCorrection(featuresToBeQuantitazed),timeout.duration).asInstanceOf[RichDouble2DArray]
    }catch {
      case e:Throwable =>
        throw e
    }
    val newFeatures = for(f <- features) yield{
      if(featureToNotBeTouched.contains(f.name) || f.is_time.getOrElse(false)) f
      else Feature(f.name, f.column_index, f.is_time, FeatureSpec(FeatureType.CUMULATIVE, f.specs.expanded_from, FeatureMonotony.NONE, Bounds(None, None)))
    }

    (newDataset, newFeatures)
  }

  /**
    * By-feature cumulation
    *
    * @param feature contains the TIME ORDERED feature column
    * @return the time ordered feature obtained by accumulation over time
    */
  def apply(feature: RichDouble1DArray): RichDouble1DArray = {
    @tailrec def recursiveApply(curr: Int, result:List[Double]):Array[Double] = {
      if(curr >= feature.length) result.reverse.toArray[Double]
      else if(curr == 0) {
        recursiveApply(curr+1, feature(curr) :: result)
      } else {
        recursiveApply(curr+1, (feature(curr) + result.head) :: result)
      }
    }

    it.necst.marc.toRichDouble1DArray(recursiveApply(0, Nil))
  }
}*/
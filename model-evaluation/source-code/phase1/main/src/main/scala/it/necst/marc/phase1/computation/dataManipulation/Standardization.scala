package it.necst.marc.phase1.computation.dataManipulation

import it.necst.marc.data.{FeatureType, Feature}

import scalaSci.RichDouble2DArray

object StandardizationImplementation extends OperationImplementation {
  //TODO: Check
  def apply(dataset:RichDouble2DArray, attrs:Map[Operation.AttributeName,String], features: List[Feature]):(RichDouble2DArray,List[Feature]) = {

    val featureMappings = (for(f <- features if f.specs.feature_type != FeatureType.CATEGORICAL) yield f.name -> f.column_index).toMap

    def getIndexFeatures(features: String): List[Int]= {
      val listFeatures = features.split(",").toList
      def generateIntList(toBeProcessed: List[String], indexList: List[Int]): List[Int] = {
        if(toBeProcessed.isEmpty) indexList
        else{
          val newList = if(!toBeProcessed.head.isEmpty){
            featureMappings.get(toBeProcessed.head) match {
              case Some(index) =>
                indexList :+ index
              case None =>
                throw new IllegalArgumentException("Feature to standardize not found (you may have asked for the standardization of a CATEGORICAL feature, which is not possible).")
            }
          }else {
            indexList
          }
          generateIntList(toBeProcessed.tail,newList)
        }
      }
      generateIntList(listFeatures,Nil)
    }

    def standardizeAll(dataset:RichDouble2DArray, toBeProcessed:List[Int]):RichDouble2DArray = {
      if (toBeProcessed.isEmpty) dataset 
      else {
        def computeMeanAndVariance(array:Array[Double]):(Double, Double) = {
          val list = array.toList
          val mean = list.sum/array.length
          val squareErrors = for (value <- list) yield math.pow(mean-value,2)
          val variance = squareErrors.sum/array.length
          (mean,variance)
        }
        val colIndex = toBeProcessed.head
        val col = dataset.getCol(colIndex)
        val mv = computeMeanAndVariance(col)
        for (i <- 0 to dataset.size()._1) {
          dataset.update(i, colIndex, (dataset(i, colIndex)-mv._1)/mv._2)
        }
        standardizeAll(dataset, toBeProcessed.tail)
      }
    }

    val featuresIndexes = getIndexFeatures(attrs("features"))

    (standardizeAll(dataset, featuresIndexes), features)

  }
}
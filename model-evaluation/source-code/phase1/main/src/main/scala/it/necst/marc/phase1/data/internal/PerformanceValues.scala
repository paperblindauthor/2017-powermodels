package it.necst.marc.phase1.data.internal

import it.necst.marc.data.support.ExtractableEnumeration

/**
  * Created by andrea on 25/11/15.
  */

object DataManagementStats extends ExtractableEnumeration{

  val DATA_DECOMPRESSION           = Value("DataDecompression")
  val DATA_COMPRESSION             = Value("DataCompression")

}

object PreProcessingStats extends ExtractableEnumeration {
    val FIRST_REQUEST_SENT         = Value("FirstRequestSent")
    val ALL_RESPONSES_RECEIVED     = Value("AllResponsesReceived")
    val PREPROCESSING_COMPLETED    = Value("PreProcessingCompleted")
    val CATEGORICAL_EXPANSION_COMPLETED = Value("CategoricalExpansionCompleted")
}

object DataManipulationStats extends ExtractableEnumeration {
  val DATA_MANIPULATION_COMPLETED = Value("DataManipulationCompleted")
}

object FeatureSelectionStats extends ExtractableEnumeration {
  val FIRST_REQUEST_SENT          = Value("FirstRequestSent")
  val ALL_RESPONSES_RECEIVED      = Value("AllResponsesReceived")
  val FEATURE_SELECTION_COMPLETED = Value("FeatureSelectionCompleted")
}
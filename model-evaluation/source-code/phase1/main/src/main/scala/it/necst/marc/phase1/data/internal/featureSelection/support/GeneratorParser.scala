package it.necst.marc.phase1.data.internal.featureSelection.support

import it.necst.marc.data.Feature

import scala.util.parsing.combinator.JavaTokenParsers
import scalaSci.RichDouble1DArray

class GeneratorParser(
    val mappings:List[Feature]
        ) extends JavaTokenParsers {
  def member:Parser[Member] =          m_term~rep("""[+]|[-]""".r~m_term)       ^^ (x => new Member(TreeBuilder(x._1, for(m_term <- x._2) yield (m_term._1, m_term._2))))
  def m_term:Parser[MTerm] =           m_factor~rep("""[*]|[/]""".r~m_factor)   ^^ (x => new MTerm(TreeBuilder(x._1, for(m_factor <- x._2) yield (m_factor._1, m_factor._2))))
  def m_factor:Parser[AnyMFactor] = (
                                      "|"~>member<~"|"                  ^^ (x => new Abs(x))
                                    | "("~>member<~")"                  ^^ (x => x)
                                    | "[IF]("~>formula~")[THEN]("~member~")[ELSE]("~member<~")"                      ^^ (x => new IfElseClause(x._1._1._1._1, x._1._1._2, x._2))
                                    | "[LOG]("~>number~","~member<~")"    ^^ (x => new Log(x._1._1, x._2))
                                    | "[POW]("~>member~","~member<~")"    ^^ (x => new Pow(x._1._1, x._2))
                                    | "[MIN]("~>rep1sep(member, ",")<~")" ^^ (x => new Min(x))
                                    | "[MAX]("~>rep1sep(member, ",")<~")" ^^ (x => new Max(x))
                                    | "-"~>member                       ^^ (x => new Neg(x))
                                    | pointer                           ^^ (x => x)
                                    | number                            ^^ (x => x)
                                    )
  def formula:Parser[Formula] =     repsep(term, "||")             ^^ (x => new Formula(x))
  def term:Parser[Term] =           repsep(factor, "&&")           ^^ (x => new Term(x))
  def factor:Parser[Factor] = (
    direct_expression                  ^^ (x => new Factor(x))
      | neg_expression                 ^^ (x => new Factor(x))
    )
  def direct_expression:Parser[AnyExpression] = (
    expression                         ^^ (x => new DirectExpression(x))
      | "("~>formula<~")"              ^^ (x => new DirectExpression(x))
    )
  def neg_expression:Parser[AnyExpression] = (
    "!"~>expression                  ^^ (x => new NegExpression(x))
      | "!("~>formula<~")"           ^^ (x => new NegExpression(x))
    )
  def expression:Parser[AnyCompareExpression] = (
        member~"="~member             ^^ (x => new AnyCompareExpression(x._1._1, _==_, x._2))
      | member~"!="~member            ^^ (x => new AnyCompareExpression(x._1._1, _!=_, x._2))
      | member~">="~member            ^^ (x => new AnyCompareExpression(x._1._1, _>=_, x._2))
      | member~"<="~member            ^^ (x => new AnyCompareExpression(x._1._1, _<=_, x._2))
      | member~">"~member             ^^ (x => new AnyCompareExpression(x._1._1, _>_,  x._2))
      | member~"<"~member             ^^ (x => new AnyCompareExpression(x._1._1, _<_,  x._2))
    )
  def pointer:Parser[Pointer] = identifier                              ^^ (x => new Pointer(x,mappings))
  def identifier:Parser[Identifier] = """[a-zA-Z_]\w*""".r              ^^ (x => {new Identifier(x)})
  def number:Parser[Number] = """(0|[1-9][0-9]*([.][0-9]+)?)""".r       ^^ (x => new Number(x.toDouble))


  def apply(in:String) = {
    parseAll(member, in.replaceAll("""\s*""",""))
  }
}

  object TreeBuilder {
    def apply(first:MTerm, remainder:List[(String,MTerm)]):MemberTreeNode = {
      if (remainder.isEmpty) new DirectMemberTreeNode(first)
      else new InnerMemberTreeNode(new DirectMemberTreeNode(first), if (remainder.head._1 == "+") _+_ else _-_, apply(remainder.head._2, remainder.tail))
    }
    def apply(first:AnyMFactor, remainder:List[(String,AnyMFactor)]):MTermTreeNode = {
      if (remainder.isEmpty) new DirectMTermTreeNode(first)
      else new InnerMTermTreeNode(new DirectMTermTreeNode(first), if (remainder.head._1 == "*") _*_ else _/_, apply(remainder.head._2, remainder.tail))
    }
  }

  class IfElseClause(val condition:Formula, val ifTrue:Member, val ifFalse:Member) extends AnyMFactor{
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray):Double = {
      if(condition(sample,prev)){
        ifTrue(sample,prev)
      } else {
        ifFalse(sample,prev)
      }
    }
  }

  class Formula(val terms: List[Term]) extends AnyInnerExpression {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean = {
      val results = for (term <- terms) yield term(sample,prev)
      results.reduce({
        _ || _
      })
    }
  }

  class Term(val factors: List[Factor]) {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean = {
      val results = for (factor <- factors) yield factor(sample,prev)
      results.reduce({
        _ && _
      })
    }
  }

  class Factor(val expression: AnyExpression) {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean = {
      expression(sample,prev)
    }
  }

  abstract class AnyExpression {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean
  }

  class DirectExpression(val inner: AnyInnerExpression) extends AnyExpression {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean = {
      inner(sample,prev)
    }
  }

  class NegExpression(val inner: AnyInnerExpression) extends AnyExpression {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean = {
      !inner(sample,prev)
    }
  }

  abstract class AnyInnerExpression {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean
  }

  class AnyCompareExpression(val lside: Member, val cmp: (Double, Double) => Boolean, val rside: Member) extends AnyInnerExpression {
    def apply(sample: RichDouble1DArray, prev:RichDouble1DArray): Boolean = {
      cmp(lside(sample,prev), rside(sample,prev))
    }
  }

  class Member(val root:MemberTreeNode) extends AnyMFactor{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      root(sample,prev)
    }
  }

  abstract class MemberTreeNode {
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double
  }

  class DirectMemberTreeNode(term:MTerm) extends MemberTreeNode{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      term(sample,prev)
    }
  }

  class InnerMemberTreeNode(lside:MemberTreeNode, op:(Double,Double)=>Double, rside:MemberTreeNode) extends MemberTreeNode{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      op(lside.apply(sample,prev),rside.apply(sample,prev))
    }
  }

  class MTerm(val root:MTermTreeNode) {
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      root(sample,prev)
    }
  }

  abstract class MTermTreeNode {
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double
  }

  class DirectMTermTreeNode(factor:AnyMFactor) extends MTermTreeNode{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      factor(sample, prev)
    }
  }

  class InnerMTermTreeNode(lside:MTermTreeNode, op:(Double,Double)=>Double, rside:MTermTreeNode) extends MTermTreeNode{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      op(lside.apply(sample,prev),rside.apply(sample,prev))
    }
  }

  abstract class AnyMFactor{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double
  }

  class Pointer(val id:Identifier, val mappings:List[Feature]) extends AnyMFactor{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      Feature.getValue(id(sample,prev), sample, prev, mappings)
    }
  }

  class Abs(val inner:Member) extends AnyMFactor{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      math.abs(inner(sample, prev))
    }
  }

  class Log(val base:Number, val arg:Member) extends AnyMFactor{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      math.log10(arg(sample,prev))/math.log10(base(sample,prev))
    }
  }

  class Pow(val base:Member, val exp:Member) extends AnyMFactor{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      math.pow(base(sample,prev), exp(sample,prev))
    }
  }

  class Neg(val inner:Member) extends AnyMFactor {
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      -inner(sample,prev)
    }
  }

  class Min(val members:List[Member]) extends AnyMFactor {
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      val values = for (member <- members) yield member(sample,prev)
      values.reduce((x,y) => if (x<y) x else y)
    }
  }

  class Max(val members:List[Member]) extends AnyMFactor {
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      val values = for (member <- members) yield member(sample,prev)
      values.reduce((x,y) => if (x>y) x else y)
    }
  }

  class Identifier(val id:String) {
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):String = {
      id
    }
  }

  class Number(val n:Double) extends AnyMFactor{
    def apply(sample:RichDouble1DArray, prev:RichDouble1DArray):Double = {
      n
    }
  }
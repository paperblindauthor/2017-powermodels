package it.necst.marc.phase1.data.internal.preprocessing

import it.necst.marc._
import it.necst.marc.data.support.ExtractableEnumeration

import scalaSci.RichDouble1DArray

object ReductionRule{
  object Mode extends ExtractableEnumeration {
    val Linear = Value("LINEAR")
    val Log    = Value("LOG")
    val Custom = Value("CUSTOM")
  }
  def apply(feature:Int, mode:Mode.Value, min:Double, max:Double, splits:Int):ReductionRule = {
    val bounds = if (min<max) (min,max) else (max,min)
    val reduction = (x:Double) => {
       if (bounds._1==bounds._2)
          0
        else if (x<=bounds._1)
          0
        else if (x>=bounds._2)
          splits
        else
          mode match {
            case Mode.Linear =>
              ((x-bounds._1)/((bounds._2-bounds._1)/splits)).floor
            case Mode.Log =>
                val q = math.pow(bounds._2 - bounds._1, 1/(splits-1))
                if (q<=0) 0 
                else {
                  val split = math.log(x+bounds._1)/math.log(q)
                  if (split < 0) 0
                  else math.pow(q, split.floor)
                }
            case Mode.Custom =>
              throw new IllegalArgumentException("Custom mode do not support automatic splitting.")
          }
    }
    return new ReductionRule(feature, reduction)
  }
  def apply(feature:Int, limits:Double*):ReductionRule = {
    val orderedLimits = collection.SortedSet(limits: _*)
    val reduction = (x:Double) => {
      if (x<=orderedLimits.head) 0
      else if (x>=orderedLimits.last) orderedLimits.size
      else (orderedLimits.map { l => if (x>=l) 1 else 0 }.sum-1).toDouble
    }
    return new ReductionRule(feature, reduction)
  }
}

class ReductionRule(
    val columnIndex:Int,
    val reduction:(Double) => Double
  ) {

  def apply(sample:RichDouble1DArray):RichDouble1DArray = {
      val head = if(columnIndex == 0) RichDouble1DArray() else
                 if(columnIndex == 1) RichDouble1DArray(sample(columnIndex-1))
                 else sample(0, columnIndex-1)
      val elem = RichDouble1DArray(reduction(sample(columnIndex)))
      val tail = if(columnIndex == sample.length-1) RichDouble1DArray() else
                 if(columnIndex == sample.length-2) RichDouble1DArray(sample(columnIndex+1))
                 else sample(columnIndex+1, sample.length-1)
      (tail :: elem) :: head
  }
}
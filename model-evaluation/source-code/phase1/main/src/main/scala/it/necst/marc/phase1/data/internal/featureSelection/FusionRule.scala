package it.necst.marc.phase1.data.internal.featureSelection

import it.necst.marc._
import it.necst.marc.data.Feature
import it.necst.marc.phase1.data.internal.featureSelection.support.Member
import scalaSci.RichDouble1DArray

class FusionRule(
    val destination:Feature,
    val generator:Member
  ) {
  def apply(line:RichDouble1DArray,prev:RichDouble1DArray):RichDouble1DArray = {
    if (destination.column_index < line.size()) {
      line(destination.column_index) = generator(line,prev)
      line
    } else {
      val list = for {
        i <- 0 to destination.column_index
        cell = if (i != destination.column_index) {
          if (i < line.size()) {
            line(i)
          } else {
            -1
          }
        } else {
          generator(line,prev)
        }
      } yield cell
      list.toArray
    }
  }

  //For parallel computation and reconstruction
  def applyWithSmallOutput(line:RichDouble1DArray,prev:RichDouble1DArray):Double = {
    generator(line,prev)
  }
}
package it.necst.marc.phase1.actors

import akka.actor.{Props, ActorRef, Actor}
import akka.routing.BalancingPool
import it.necst.marc.data.support.{ErrorStack, MarcLogger}

import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
  * Created by andrea on 2/1/16.
  */


/*object QuantizationCorrectionCollector{

  def props(dataSet: RichDouble2DArray, featureMapping: Map[String,Int],numberOfActor: Int): Props = Props(new QuantizationCorrectionCollector(dataSet,featureMapping,numberOfActor))

}


class QuantizationCorrectionCollector(var dataset: RichDouble2DArray,
                                    featureMapping: Map[String,Int],
                                    numberOfActors: Int) extends Actor{


  private val logger = MarcLogger(getClass.getName)

  private var routerRef: ActorRef = _
  private var waitingMessages: Int = _
  private var featuresQuantizated: Map[Int,RichDouble1DArray] = _
  private var callerRef: ActorRef = _

  override def preStart() = {
      featuresQuantizated = Map()
      waitingMessages = 0
      routerRef = context.system.actorOf(BalancingPool(numberOfActors).props(QuantizationCorrectionActor.props()),"quantizationCorrectionActor")
  }
  def receive = initState

  def initState: Receive = {

    case StartQuantizationCorrection(featuresToBeModified) =>
      logger.info("Received StartQuantizationCorrection message")
      try{
        callerRef = sender()
        context.become(collectResults)
        sendComputationRequests(featuresToBeModified)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          throw new IllegalArgumentException(error)
          context.stop(routerRef)
          context.stop(self)
      }


  }

  def collectResults: Receive = {

    case FeatureQuantizated(feature,index) =>
      logger.info("Received FeatureQuantizated message")
      try{
        waitingMessages = waitingMessages - 1
        featuresQuantizated = featuresQuantizated + (index -> feature)

        if(waitingMessages == 0){
          dataset = rebuildDataSet()
          callerRef ! dataset
          context.stop(routerRef)
          context.stop(self)
        }

      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          throw new IllegalArgumentException(error)
          context.stop(routerRef)
          context.stop(self)
      }

    case ThrowsError(error) =>
      throw new IllegalArgumentException(error)
      context.stop(routerRef)
      context.stop(self)

  }

  private def sendComputationRequests(toBeProcessed: List[String]): Unit = {
    if(toBeProcessed.nonEmpty){
      val index = featureMapping(toBeProcessed.head)
      waitingMessages = waitingMessages + 1
      routerRef ! CorrectQuantization(new RichDouble1DArray(dataset.getCol(index)),index)
      sendComputationRequests(toBeProcessed.tail)
    }
  }

  private def rebuildDataSet(): RichDouble2DArray = {

    def create(indexToAdd: Int, finalDataSet: RichDouble2DArray): RichDouble2DArray = {
      if(indexToAdd < 0) finalDataSet
      else{
        val feature = if(featuresQuantizated.contains(indexToAdd)) featuresQuantizated(indexToAdd).getv() else dataset.getCol(indexToAdd)
        val newDataset = if(finalDataSet == null) {
          new RichDouble2DArray(feature)
        } else {
          finalDataSet <<< feature
        }

        create(indexToAdd-1,newDataset)
      }
    }

    create(dataset.numColumns()-1,null)
  }
}*/

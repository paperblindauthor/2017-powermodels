package it.necst.marc.phase1.data

import it.necst.marc.phase1.data.internal.preprocessing.ReductionRule.Mode
import it.necst.marc.data.{AbstractConfiguration, Feature}
import it.necst.marc.phase1.computation.dataManipulation.Operation

/**
 * Created by andreadamiani on 03/07/15.
 */
case class Configuration(features:List[Feature],
                         timeFeature:String,
                         reduction:Option[List[ReductionRule]],
                         coherenceRules:Option[List[CoherenceRule]],
                         manipulations:Option[List[ManipulationOperation]],
                         fusions:Option[List[FusionRule]],
                         excluded:Option[List[Feature.ID]]) extends AbstractConfiguration{

  require(features!=null && features.nonEmpty, "No features configured")
  require(timeFeature!=null, "No time-features configured")
  require(features.map(_.name).toSet.size==features.size          //Unique feature name
    && features.map(_.column_index).toSet.size==features.size,    //Unique column index binding
    "Features name and column-index must be unique")
  require(features.exists(_.name == timeFeature), "The selected time-feature does not exsist")
}

case class ReductionRule(feature:Feature.ID, mode:Mode.Value, steps: Option[Int], intervals:Option[List[Double]]){
  require(feature!=null)
  require(mode!=null)
  require(if(intervals.isDefined) {
    mode == Mode.Custom && intervals.get.nonEmpty
  } else true)
  require((steps.isDefined && intervals.isEmpty) || (steps.isEmpty && intervals.isDefined))
}

case class CoherenceRule(test:String, actuation:String){
  require(test!=null)
  require(actuation!=null)
}

case class ManipulationOperation(function_name:Operation.ID, parameters:Option[List[ManipulationParam]]){
  require(function_name!=null)
  require(if(parameters.isDefined) parameters.get.nonEmpty else true)
}

case class ManipulationParam(name:Operation.AttributeName, value:String){
  require(name!=null)
  require(value!=null)
}

case class FusionRule(destination:Feature, generator:String){
  require(generator!=null)
}
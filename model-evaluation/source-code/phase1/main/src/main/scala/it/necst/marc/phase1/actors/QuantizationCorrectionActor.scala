package it.necst.marc.phase1.actors

import akka.actor.{Props, Actor}
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.phase1.computation.dataManipulation.QuantizationCorrectionImplementation

import scalaSci.RichDouble1DArray

/**
  * Created by andrea on 2/1/16.
  */


/*object QuantizationCorrectionActor{

  def props(): Props = Props(new QuantizationCorrectionActor())

}

class QuantizationCorrectionActor extends Actor{


  private val logger = MarcLogger(getClass.getName)


  def receive = {
    case CorrectQuantization(minimalDataset: RichDouble1DArray,index: Int) =>
      logger.debug("Received CorrectQuantization message")
      try {
        val featureQuantizated = QuantizationCorrectionImplementation(feature)
        sender() ! FeatureQuantizated(featureQuantizated, index)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }

  }

}*/

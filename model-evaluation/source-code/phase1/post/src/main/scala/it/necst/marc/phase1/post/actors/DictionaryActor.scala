package it.necst.marc.phase1.post.actors

import it.necst.marc.data.Binding
import akka.actor.{Actor, Props}
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification

/**
 * Created by andrea on 27/10/15.
 */

object DictionaryActor{

  def props(): Props = Props(new DictionaryActor)
}


class DictionaryActor extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private var dictionary: Map[SubphaseBatchIdentification.Code,List[Binding]] = _

  override def preStart() = {
    dictionary = Map[SubphaseBatchIdentification.Code,List[Binding]]()
  }

  def receive: Receive = {
    case AddEntryIntoDictionary(code,bindings) =>
      dictionary = dictionary + (code->bindings)

    case GiveMeDictionary() =>
      logger.info("Received GiveMeDictionary message")
      sender() ! TakeDictionary(dictionary)

  }

}

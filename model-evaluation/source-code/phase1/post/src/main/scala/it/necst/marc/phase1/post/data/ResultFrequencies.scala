package it.necst.marc.phase1.post.data

import it.necst.marc.phase1.post.data.support.Phase2Subphase

case class ResultFrequencies(byPhase: List[PhaseResultFrequencies]){
  require(byPhase!=null, "Frequencies by phase must be defined.")
  private val phasesList = byPhase.map(_.phase)
  require(phasesList.size == phasesList.toSet.size, "Each subphase must be described at most once.")
}

case class PhaseResultFrequencies(phase:Phase2Subphase.Value, frequencies:List[Frequency]){
  require(frequencies!=null, "frequencies must be defined.")
}

case class Frequency(configuration:List[ConfigurationCode], frequency:Double){
  require(configuration.nonEmpty, "At least one code must define the configuration.")
  require(frequency > 0 && frequency <= 1, "Relative frequency must be in (0;1].")
  private val featuresInv = for (feature <- configuration) yield feature.subphase
  require(featuresInv.size==featuresInv.toSet.size, "Every subphase must have a unique code in a configuration.")
}
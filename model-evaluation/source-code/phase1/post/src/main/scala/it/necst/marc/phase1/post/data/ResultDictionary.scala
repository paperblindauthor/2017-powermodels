package it.necst.marc.phase1.post.data

import it.necst.marc.data.Binding
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.support.Phase2Subphase

/**
  * Created by andreadamiani on 13/11/15.
  */
case class ResultDictionary(byPhase:List[Dictionary])

case class Dictionary(phase:Phase2Subphase.Value, entries:List[Entry])

case class Entry(code:Code, bindings:List[Binding])

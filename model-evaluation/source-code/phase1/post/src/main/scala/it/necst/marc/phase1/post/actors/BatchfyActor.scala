package it.necst.marc.phase1.post.actors

import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import akka.actor.{ActorRef, Props, Actor}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification
import it.necst.marc.phase1.post.data.Configuration
import it.necst.marc.data.Feature

/**
 * Created by andrea on 22/10/15.
 */

object BatchfyActor{

  def props(loadBalancerProducers: ActorRef,
            configuration: Configuration,
            mappings: List[Feature],
            internalWorkerRef: ActorRef): Props = Props(new BatchfyActor(loadBalancerProducers,configuration,mappings,internalWorkerRef))

}


class BatchfyActor(loadBalancerProducers: ActorRef,
                   configuration: Configuration,
                   mappings: List[Feature],
                   internalWorkerRef: ActorRef) extends Actor{


  private val logger = MarcLogger(getClass.getName)

  def receive: Receive = {
    case Batchfy(codifyResult) =>
      try{
        val batchfyResult = SubphaseBatchIdentification.batchify(codifyResult.sample,codifyResult.codes,codifyResult.initial, configuration,mappings)
        loadBalancerProducers ! UpdateBatches(batchfyResult)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          internalWorkerRef ! ThrowsError(error)
      }

  }





}

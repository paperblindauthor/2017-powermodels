package it.necst.marc.phase1.post.actors

import akka.actor.{Actor, ActorRef, Props}
import it.necst.marc.data.{Binding, Feature, PartialStat}
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification._
import it.necst.marc.phase1.post.data._
import it.necst.marc.phase1.post.data.internal.{BatchIdentificationStats, BatchInformation, BatchTreeNode, InnerTreeNode}
import it.necst.marc.phase1.post.data.support.Phase2Subphase

/**
 * Created by andrea on 22/10/15.
 */

object ProducerActor{

  def props(phaseName: Phase2Subphase.Value,
            configuration: Configuration,
            datasetSize: Int,
            mappings: List[Feature],
            internalWorkerRef: ActorRef,
            dictionaryActor: ActorRef): Props = Props(new ProducerActor(phaseName,configuration,datasetSize,mappings,internalWorkerRef,dictionaryActor))
}

class ProducerActor(phaseName: Phase2Subphase.Value,
                    configuration: Configuration,
                    datasetSize: Int,
                    mappings: List[Feature],
                    internalWorkerRef: ActorRef,
                    dictionaryActor: ActorRef) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private var waitingMessages = if(datasetSize>=2){datasetSize-2}else{0} //The first two rows of a dataset are consumed for codify initialization

  private val batchInformation = new BatchInformation(new InnerTreeNode(scala.collection.mutable.Map[Code, BatchTreeNode]()),
                                                      scala.collection.mutable.Map[List[Code], Int](),mappings.find(_.is_time.getOrElse(false)).get.column_index)

  private var insertionTime: Long = 0l

  private var countBatches = 0

  private var dictionary:Map[SubphaseBatchIdentification.Code,List[Binding]] = _

  def receive = collectState

  def collectState: Receive = {
    case UpdateBatch(batch) =>
      waitingMessages = waitingMessages - 1

      try{
        val start = System.currentTimeMillis()
        SubphaseBatchIdentification.incrementalBatchGeneration(batchInformation,batch,mappings)
        val finish = System.currentTimeMillis()
        insertionTime = finish-start+insertionTime
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          internalWorkerRef ! ThrowsError(error)
      }

      if(waitingMessages == 0){
        allMessagesReceived()
      }

    case InvalidSampleFound() =>
      waitingMessages = waitingMessages - 1
      if(waitingMessages == 0){
        allMessagesReceived()
      }
  }

  private def allMessagesReceived():Unit = {
    logger.info(s"Batch Generated for PHASE $phaseName")
    dictionaryActor ! GiveMeDictionary()
    context.become(createResultState)
  }
  def createResultState: Receive = {
    case TakeDictionary(dict) =>
      logger.info("Received TakeDictionary message")

      try{
        dictionary = dict
        val initDFS = System.currentTimeMillis()
        val batches = batchInformation.dfs
        val finishDFS = System.currentTimeMillis()
        val codesRules = Phase2Subphase.groupingRules(phaseName) :+ phaseName

        val modelConfigurations = for(data <- batches) yield {
          val codes = (for(i <- codesRules.indices) yield ConfigurationCode(codesRules(i),data.codes(i))).toList
          val outputMonotony = dictionary(codes.last.code).find(_.name == configuration.outputFeature).get.value.monotony
          ModelConfiguration(codes, data.data, outputMonotony)
        }

        val data = for(item <- batchInformation.mergePercentages) yield item._1 -> item._2.toMap
        val frequencies = batchInformation.createFrequencies(data)

        val stats = List(PartialStat(BatchIdentificationStats.INSERTION_TIME.toString+"-"+phaseName.toString,insertionTime),
          PartialStat(BatchIdentificationStats.DFS_TIME.toString+"-"+phaseName.toString,finishDFS-initDFS))
        internalWorkerRef ! ResultComputed(phaseName,Result(modelConfigurations,mappings),PhaseResultFrequencies(phaseName,frequencies),stats)


      }catch {
        case e: Throwable =>
          internalWorkerRef ! ThrowsError(ErrorStack(e))
      }
  }
}

package it.necst.marc.phase1.post.actors

import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.data.{FeatureMonotony, Feature}
import akka.actor.{ActorRef, Actor, Props}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.computation.{CodifyResult, SubphaseBatchIdentification}
import it.necst.marc.phase1.post.data.Configuration
import it.necst.marc._
import it.necst.marc.phase1.post.data.support.Phase2Subphase

import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
 * Created by andrea on 21/10/15.
 */

object CodingActor{

  def props(
            configuration:Configuration,
            mappings: List[Feature],
            dictionaries: Map[Phase2Subphase.Value,ActorRef],
            internalWorkerRef: ActorRef,
            loadBalancerProducers: ActorRef): Props = Props(new CodingActor(configuration,mappings,dictionaries,internalWorkerRef,loadBalancerProducers))

}

class CodingActor(
                  configuration:Configuration,
                  mappings: List[Feature],
                  dictionaries: Map[Phase2Subphase.Value,ActorRef],
                  internalWorkerRef: ActorRef,
                  loadBalancerProducers: ActorRef) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private var codesAlreadySent: Map[Phase2Subphase.Value,List[String]] = _

  private val timeFeature = mappings.find(_.is_time.getOrElse(false)) match {
    case Some(f) => f
    case _ => throw new IllegalArgumentException("Time feature is not defined.")
  }

  override def preStart() = {
    codesAlreadySent = Phase2Subphase.values.toList.map{x:Phase2Subphase.Value => x -> Nil}.toMap

  }

  def receive: Receive = {
    case StartScanDataset(dataset) =>
      try{
        scanDataSet(dataset,mappings)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          internalWorkerRef ! ThrowsError(error)
      }

  }

  private def scanDataSet(dataset: RichDouble2DArray,mappings: List[Feature]) = {

    def scan(currentSampleIndex: Int,
             previousSample: RichDouble1DArray,
             previousCodes: Map[Phase2Subphase.Value, Code],
             currentMonotony: Map[Feature.ID, FeatureMonotony.Value]):Unit = {

      if(currentSampleIndex < dataset.numRows()){
        val currentSample = toRichDouble1DArray(dataset.getRow(currentSampleIndex))

        //TODO clean
        //old version codify
        //val resultCodification = SubphaseBatchIdentification.codify(currentSample, previousSample, previousCodes, currentMonotony, configuration, mappings, timeFeature.column_index)
        val resultCodification = SubphaseBatchIdentification.codify(currentSample,previousSample,previousCodes,currentMonotony,configuration,mappings,timeFeature.column_index)
        loadBalancerProducers ! UpdateBatches(SubphaseBatchIdentification.batchify(resultCodification.sample,resultCodification.codes,resultCodification.initial,configuration,mappings))
        sendResultToDictionaries(resultCodification)
        scan(currentSampleIndex+1,currentSample,resultCodification.codes,resultCodification.monotony)
      }
    }

    if(dataset.numRows()>=2) {
      val first = SubphaseBatchIdentification.codify(toRichDouble1DArray(dataset.getRow(1)), toRichDouble1DArray(dataset.getRow(0)), Map[Phase2Subphase.Value, Code](), Map[Feature.ID, FeatureMonotony.Value](), configuration, mappings, timeFeature.column_index)
      scan(2, toRichDouble1DArray(dataset.getRow(1)), first.codes, Map[Feature.ID, FeatureMonotony.Value]())
    }else{
      //not enough data for codify initialization
      for(i<-0 until dataset.numRows()){
        loadBalancerProducers ! InvalidSampleFound()
      }
    }

  }

  private def sendResultToDictionaries(resultCodification: CodifyResult) = {

    def sendMessages(toBeProcessed: Map[Phase2Subphase.Value,ActorRef]): Unit = {
      if(toBeProcessed.nonEmpty){
        val code = resultCodification.codes.getOrElse(toBeProcessed.head._1,"")
        if(!codesAlreadySent.get(toBeProcessed.head._1).get.contains(code.toString)) {
          toBeProcessed.head._2 ! AddEntryIntoDictionary(code,resultCodification.bindings.getOrElse(toBeProcessed.head._1,Nil))
          val newList: List[String] = codesAlreadySent.get(toBeProcessed.head._1).get :+ code.toString
          codesAlreadySent = codesAlreadySent.filter(!_._1.equals(toBeProcessed.head._1)) + (toBeProcessed.head._1 -> newList)
        }
        sendMessages(toBeProcessed.tail)
      }
    }

    sendMessages(dictionaries)
  }



}

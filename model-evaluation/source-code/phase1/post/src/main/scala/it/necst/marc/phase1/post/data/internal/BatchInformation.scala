package it.necst.marc.phase1.post.data.internal

import it.necst.marc.data.support.MarcLogger
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase1.post.data.{Sample, ConfigurationCode, Frequency}

import scala.collection.mutable

/**
 * Created by andreadamiani on 21/10/15.
 */

sealed trait BatchTreeNode
class InnerTreeNode(val ptrs:scala.collection.mutable.Map[Code, BatchTreeNode]) extends BatchTreeNode
class LeafTreeNode(val data:scala.collection.mutable.ListBuffer[Sample]) extends BatchTreeNode

case class DFSExplorationResult(val codes:List[Code], val data:List[Sample])

/**
  * Holds together all the batches and related decomposition-recomposition information
  *
  * @param batchTree  The batches organized in a tree wrt their code
  * @param mergeCount The total count of samples for every code
  * @note This structure is mutable for performance concernes
  */
class BatchInformation(val batchTree:BatchTreeNode, val mergeCount: scala.collection.mutable.Map[List[Code], Int],val timeFeature: Int) {

  private val logger = MarcLogger("BatchInformation")

  def mergePercentages = {
    /**
      * Returns the frequency of appearance of each SubCode in each MainCode.
      * The border between MainCode and SubCode is defined by the level parameter, being part of MainCode all the
      * configuration codes that are at lower depth level than the one spcified in the parameter, and SubCode all the
      * remaining codes.
      *
      * @param level The last level part of the MainCode
      * @return The frequency of each SubCode in each MainCode in the form Map[ MainCode , Map[ SubCode , Frequency ] ]
      * @note Depends on correct mergeCount handling
      */
    def generate(level:Int) = {
      //Keeps mergeCounts that are at least reaching the desired depth level
      val interestedList = mergeCount.filter(_._1.length>=level)
      //Groups by mainCodes, i.e. the codes above the desired depth level
      //Generating a Map[MainCodes, Map[SubCodes, MergeCount]]
      val grouped = interestedList.groupBy{case (k,v) => k.dropRight(k.size - level)}.map{case (k1,v1) => k1 -> v1.map{case (k2,v2) => k2.drop(level) -> v2}}
      //For each MainCode obtains the total count
      val totals = grouped.map{case (k1,v1) => k1 -> v1.map{case (k2,v2) => v2}.sum}
      //For each SubCode in MainCode obtains the frequency of appearance of SubCode in MainCode
      //Map[MainCode, Map[SubCode, Frequency]]
      grouped.map{case (k1,v1) => k1 -> v1.map{case (k2,v2) => k2 -> v2.toDouble/totals(k1).toDouble}}
    }

    //Ordering: By List[Code] length on mergeCount
    //Extracts the maximum depth of configuration codes
    val maxLevel = {mergeCount max Ordering.by((_:(List[Code], _))._1.length)}._1.size

    //Recursive frequencies generation
    val percentages = for(i <- 0 to maxLevel) yield generate(i)

    //@res.length-1 => maxLevel: All frequencies are at 1
    //@res.length-2: Frequencies for model A appearing in its super-group (usually B-C)
    val res = percentages.toArray
    res(res.length-2)
  }

  def dfs: List[DFSExplorationResult] = {

    val result = mutable.ListBuffer[DFSExplorationResult]()

    def dfso(node: BatchTreeNode, currentCodes: List[Code]):Unit = {
      node match {
        case x: LeafTreeNode =>
          logger.debug(s"$currentCodes START TIME SORTING")
          val matrix:List[Sample] = x.data.sortBy(_.sample(timeFeature)).toList
          logger.debug(s"$currentCodes FINISHED TIME SORTING")

          result.prepend(DFSExplorationResult(currentCodes, matrix))
        case x: InnerTreeNode =>
          for(subTree <- x.ptrs) yield dfso(subTree._2,currentCodes :+ subTree._1)

      }
    }
    dfso(batchTree,Nil)
    result.toList
  }

  def createFrequencies(mapFrequencies: Map[List[SubphaseBatchIdentification.Code],Map[List[SubphaseBatchIdentification.Code],Double]]): List[Frequency] = {

    val seq = Phase2Subphase.groupingRules.get(Phase2Subphase.A).head :+ Phase2Subphase.A

    def scan(toBeProcessed: Map[List[SubphaseBatchIdentification.Code],Map[List[SubphaseBatchIdentification.Code],Double]],
              frequencies: List[Frequency]): List[Frequency] = {
      if(toBeProcessed.isEmpty) frequencies
      else{
        val elems = for(item <- toBeProcessed.head._2)
          yield {
            val codes = toBeProcessed.head._1 ++ item._1
            val configurationsCodes = (for(i <- codes.indices) yield ConfigurationCode(seq(i),codes(i))).toList
            Frequency(configurationsCodes,item._2)
        }
        scan(toBeProcessed.tail,frequencies ++ elems)
      }
    }

    scan(mapFrequencies,Nil)
  }
}
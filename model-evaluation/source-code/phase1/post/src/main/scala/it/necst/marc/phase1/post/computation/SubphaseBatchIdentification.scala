package it.necst.marc.phase1.post.computation

import it.necst.marc.data._
import it.necst.marc.data.support.ErrorStack
import it.necst.marc.phase1.post.data.{Sample, ConfigurationFeature, PhaseConfiguration, Configuration}
import it.necst.marc.phase1.post.data.internal.{BatchInformation, LeafTreeNode, InnerTreeNode, BatchTreeNode}
import it.necst.marc.phase1.post.data.support.Phase2Subphase

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import scalaSci.{RichDouble2DArray, RichDouble1DArray}

import it.necst.marc._

/**
 * Created by andreadamiani on 20/10/15.
 */

object SubphaseBatchIdentification {
  type Code = String
  def temporalSorting(dataset:RichDouble2DArray, timeColIndex: Int):RichDouble2DArray = toStandardDouble2DArray(dataset).sortBy(_(timeColIndex))

  //Non functional
  //Requires a functional block accepting:
  //  * split:RichDouble2DArray the split of dataset to codify
  //  * isInitialOfBatch:Boolean if true, the first sample must be defined as the beginning of a new batch due to timestamp_splitting_diff configuration
  def codifyDispatcher(dataset:RichDouble2DArray, configuration:Configuration, mappings:List[Feature], timeColIndex:Int, splitSize:Int)(messenger: RichDouble2DArray => Unit):Unit = {
    val monotonyRelatedFeats = (configuration.outputFeature :: configuration.phasesConfigs.flatMap(_.configFeatures.filter(_.is_monotony_related.getOrElse(false))).map(_.name)).toSet
    val monotonyRelatedColIndices = for(name <- monotonyRelatedFeats) yield mappings.find(_.name == name).get.column_index

    if(dataset.numRows() <= splitSize){
      messenger(dataset)
    } else {

      var count = 0

      def split(index: Int): Unit ={
        try {
          val low = index - count - 1
          val up = index
          val splitDataset = if(low == up){
            new RichDouble2DArray(dataset.getRow(up)).~
          }else{
            dataset(low,up,0,dataset.numColumns()-1)
          }
          messenger(splitDataset)
        } catch {
          case e:ArrayIndexOutOfBoundsException =>
            throw new RuntimeException(ErrorStack(e)+s"\nIndex: $index\nDataset size: ${dataset.numRows()},${dataset.numColumns()}\nCount : $count")
        }
        count = 0
      }

      for(i <- 2 until dataset.numRows()){
        count += 1
        if (i == dataset.numRows() - 1) {
          split(i)
        } else {
          if (count >= splitSize) {
            val canSplitNow = !(for (j <- monotonyRelatedColIndices) yield dataset(i, j) == dataset(i - 1, j)).contains(true)
            if (canSplitNow) {
              split(i)
            }
          }
        }
      }
    }
  }

  //TODO check and clean
  //OLD VERSION CODIFY
  /*
  def codify(sample: RichDouble1DArray, prevSample: RichDouble1DArray, prevCodes: Map[Phase2Subphase.Value, SubphaseBatchIdentification.Code], currentMonotony: Map[Feature.ID, FeatureMonotony.Value], configuration:Configuration, mappings:List[Feature], timeColIndex:Int) = {

    val outputCfgFeature = ConfigurationFeature(configuration.outputFeature, Option(true))

    val codifySpecificCfg = Configuration(configuration.phasesConfigs.map{
      (phaseCfg) => PhaseConfiguration(phaseCfg.phaseName, outputCfgFeature :: phaseCfg.configFeatures)}, configuration.outputFeature, configuration.timestamp_splitting_diff)

    val configFeatureIDsForCfg = codifySpecificCfg.phasesConfigs
      .flatMap(_.configFeatures)
      .map(_.name).toSet

    val cfgMappings = (for {
      configFeatureID <- configFeatureIDsForCfg

      mappingOpt = mappings.find(_.name == configFeatureID)
      uselessVar = require(mappingOpt.isDefined, configFeatureID + " is used as Phase 2 Configuration but is not defined.")
      mapping = mappingOpt.get
    } yield {
      configFeatureID -> mapping.column_index
    }).toMap

    val configFeatureIDsForMonotony = codifySpecificCfg.phasesConfigs
      .flatMap(_.configFeatures)
      .filter(_.is_monotony_related.getOrElse(false))
      .map(_.name).toSet

    val monotony = (for {
      configFeatureID <- configFeatureIDsForMonotony

      column_index = cfgMappings(configFeatureID)
      monotony = sample(column_index) - prevSample(column_index) match {
        case x if x>0 => FeatureMonotony.ASCENDANT
        case x if x<0 => FeatureMonotony.DESCENDANT
        case _ => currentMonotony.getOrElse(configFeatureID, FeatureMonotony.ASCENDANT)
      }
    } yield {
      configFeatureID -> monotony
    }).toMap

    val computedBindings: Map[Phase2Subphase.Value, List[Binding]] = (for {
      subphaseCfg <- codifySpecificCfg.phasesConfigs
      subphase = subphaseCfg.phaseName

      configFeatureID <- subphaseCfg.configFeatures.map(_.name)
    } yield {
        (subphase, monotony.getOrElse(configFeatureID,sample(cfgMappings(configFeatureID))) match {
          case FeatureMonotony.ASCENDANT => Binding(configFeatureID, Value(None, FeatureMonotony.ASCENDANT))
          case FeatureMonotony.DESCENDANT => Binding(configFeatureID, Value(None, FeatureMonotony.DESCENDANT))
          case x:Double => Binding(configFeatureID, Value(Some(x), FeatureMonotony.NONE))
        })
      }) groupBy {_._1} map {case (k,v:List[(Phase2Subphase.Value, Binding)]) => k -> v.map{_._2}}

    val bindings = (for (subphaseCfg <- codifySpecificCfg.phasesConfigs)
          yield subphaseCfg.phaseName -> computedBindings.getOrElse(subphaseCfg.phaseName, Nil)).toMap //ADDS DEFAULTING EMPTY BINDINGS

    val computedCodes = (for {
      subphase <- bindings.keys.toList
      binding <- bindings(subphase).sortBy(_.name)
    } yield {
        (subphase, binding.value match {
          case Value(x, FeatureMonotony.NONE) => x.get.toString
          case Value(_, FeatureMonotony.ASCENDANT) => "A"
          case Value(_, FeatureMonotony.DESCENDANT) => "D"
        })
    }) groupBy {_._1} map {case (k,v) => k -> v.map{_._2}.mkString("|")}

    val codes = (for (subphaseCfg <- codifySpecificCfg.phasesConfigs)
      yield subphaseCfg.phaseName -> computedCodes.getOrElse(subphaseCfg.phaseName, "")).toMap //ADDS DEFAULTING "" CODES

    val initialsForSwitch = (for (c <- codes) yield {
      c._1 -> (prevCodes.get(c._1) match {
        case Some(code) =>
          code != c._2
        case _ =>
          true
      })
    }).toMap

    val initialForTime = sample(timeColIndex) - prevSample(timeColIndex) > configuration.timestamp_splitting_diff

    val initials = initialsForSwitch.map{case (k,v) => k -> (v || initialForTime)}

    new CodifyResult(sample, initials, codes, bindings, monotony)
  }*/

  //Implementation requiring data supplied by codifyDispatcher
  def codify(sample: RichDouble1DArray, prevSample: RichDouble1DArray, prevCodes: Map[Phase2Subphase.Value, SubphaseBatchIdentification.Code], currentMonotony: Map[Feature.ID, FeatureMonotony.Value], configuration:Configuration, mappings:List[Feature], timeColIndex:Int) = {

    val outputCfgFeature = ConfigurationFeature(configuration.outputFeature, Option(true))

    val codifySpecificCfg = Configuration(configuration.phasesConfigs.map{
      (phaseCfg) => PhaseConfiguration(phaseCfg.phaseName, outputCfgFeature :: phaseCfg.configFeatures)}, configuration.outputFeature)

    val configFeatureIDsForCfg = codifySpecificCfg.phasesConfigs
      .flatMap(_.configFeatures)
      .map(_.name).toSet

    val cfgMappings = (for {
      configFeatureID <- configFeatureIDsForCfg

      mappingOpt = mappings.find(_.name == configFeatureID)
      uselessVar = require(mappingOpt.isDefined, configFeatureID + " is used as Phase 2 Configuration but is not defined.")
      mapping = mappingOpt.get
    } yield {
      configFeatureID -> mapping.column_index
    }).toMap

    val configFeatureIDsForMonotony = codifySpecificCfg.phasesConfigs
      .flatMap(_.configFeatures)
      .filter(_.is_monotony_related.getOrElse(false))
      .map(_.name).toSet

    val monotony = (for {
      configFeatureID <- configFeatureIDsForMonotony

      column_index = cfgMappings(configFeatureID)
      monotony = sample(column_index) - prevSample(column_index) match {
        case x if x>0 => FeatureMonotony.ASCENDANT
        case x if x<0 => FeatureMonotony.DESCENDANT
        case _ => currentMonotony.getOrElse(configFeatureID, FeatureMonotony.DESCENDANT)
      }
    } yield {
      configFeatureID -> monotony
    }).toMap

    val computedBindings: Map[Phase2Subphase.Value, List[Binding]] = (for {
      subphaseCfg <- codifySpecificCfg.phasesConfigs
      subphase = subphaseCfg.phaseName

      configFeatureID <- subphaseCfg.configFeatures.map(_.name)
    } yield {
      (subphase, monotony.getOrElse(configFeatureID,sample(cfgMappings(configFeatureID))) match {
        case FeatureMonotony.ASCENDANT => Binding(configFeatureID, Value(None, FeatureMonotony.ASCENDANT))
        case FeatureMonotony.DESCENDANT => Binding(configFeatureID, Value(None, FeatureMonotony.DESCENDANT))
        case x:Double => Binding(configFeatureID, Value(Some(x), FeatureMonotony.NONE))
      })
    }) groupBy {_._1} map {case (k,v:List[(Phase2Subphase.Value, Binding)]) => k -> v.map{_._2}}

    val bindings = (for (subphaseCfg <- codifySpecificCfg.phasesConfigs)
      yield subphaseCfg.phaseName -> computedBindings.getOrElse(subphaseCfg.phaseName, Nil)).toMap //ADDS DEFAULTING EMPTY BINDINGS

    val computedCodes = (for {
      subphase <- bindings.keys.toList
      binding <- bindings(subphase).sortBy(_.name)
    } yield {
      (subphase, binding.value match {
        case Value(x, FeatureMonotony.NONE) => x.get.toString
        case Value(_, FeatureMonotony.ASCENDANT) => "A"
        case Value(_, FeatureMonotony.DESCENDANT) => "D"
      })
    }) groupBy {_._1} map {case (k,v) => k -> v.map{_._2}.mkString("|")}

    val codes = (for (subphaseCfg <- codifySpecificCfg.phasesConfigs)
      yield subphaseCfg.phaseName -> computedCodes.getOrElse(subphaseCfg.phaseName, "")).toMap //ADDS DEFAULTING "" CODES

    val initialsForSwitch = (for (c <- codes) yield {
      c._1 -> (prevCodes.get(c._1) match {
        case Some(code) =>
          code != c._2
        case _ =>
          true
      })
    }).toMap

    val initials = initialsForSwitch.map{case (k,v) => k -> v }

    new CodifyResult(sample, initials, codes, bindings, monotony)
  }

  def batchify(sample:RichDouble1DArray, codes: Map[Phase2Subphase.Value, Code], initials: Map[Phase2Subphase.Value, Boolean], configuration:Configuration, mappings:List[Feature]):Map[Phase2Subphase.Value, BatchEntry] = {
    val orderedMappings = mappings.sortBy(_.column_index).toArray

    val samples = (for {
      subphase <- codes.keySet
      phaseConfigurationFeats = configuration.phasesConfigs.flatMap(_.configFeatures.map(_.name))
    } yield {
      subphase -> (if(Phase2Subphase.requiresSmallBatch(subphase)){
        val columnSelection = (for(i <- orderedMappings.indices) yield !phaseConfigurationFeats.contains(orderedMappings(i).name)).toArray
        toRichDouble1DArray(new RichDouble2DArray(Array{sample.getv()}).apply(::,columnSelection).getRow(0))
      } else sample)
    }).toMap

    samples.map{case (k:Phase2Subphase.Value,v) => k ->
      new BatchEntry(
        v,
        codes(k),
        for (group <- Phase2Subphase.groupingRules(k)) yield group -> codes.getOrElse(group,""),
        initials.getOrElse(k,true)
      )}
  }

  def computeSmallBatchMappings(subphase:Phase2Subphase.Value, configuration: Configuration, mappings:List[Feature]) = {
    val orderedMappings = mappings.sortBy(_.column_index).toArray

    val phaseConfigurationFeats = configuration.phasesConfigs.flatMap(_.configFeatures.map(_.name))
    if(Phase2Subphase.requiresSmallBatch(subphase)){
        val columnSelection = (for(i <- orderedMappings.indices) yield phaseConfigurationFeats.contains(orderedMappings(i).name)).toArray
        @tailrec def recursiveCorrectionComputation(partialResult: Map[Int,Int], old:Int, nextAvail:Int):Map[Int,Int] = {
          if(old<columnSelection.length){
            if(!columnSelection(old)){
              recursiveCorrectionComputation(partialResult + (old->nextAvail), old+1, nextAvail+1)
            } else {
              recursiveCorrectionComputation(partialResult, old+1, nextAvail)
            }
          } else {
            partialResult
          }
        }
        val corrections = recursiveCorrectionComputation(Map(), 0, 0)
        val batchMappings = mappings.filter(f => !columnSelection(f.column_index)).map(f => Feature(f.name, corrections(f.column_index), f.is_time, f.specs))
      batchMappings
    } else mappings
  }

  def extendSample(toUpdate: BatchEntry,mappings: List[Feature]): BatchEntry = {

    val sample = CategoricalExpansion(toUpdate.sample,mappings)
     new BatchEntry(sample,toUpdate.code,toUpdate.groupBy,toUpdate.initial)

  }

  def computeExpandedMappings(subphase:Phase2Subphase.Value, configuration: Configuration, mappings:List[Feature]) = {
    if(Phase2Subphase.requiresExpandedBatch(subphase)){
      CategoricalExpansion(CategoricalExpansion(mappings),mappings.filter(_.specs.feature_type.equals(FeatureType.CATEGORICAL)))
    } else mappings
  }

  def incrementalBatchGeneration(batchInformation: BatchInformation, entry: BatchEntry, mappings:List[Feature]): Unit = {
    @tailrec def recursiveTreeTraversal(root:BatchTreeNode, toBeProcessed: List[Code]):BatchTreeNode = {
      if (toBeProcessed.isEmpty) root //Leaf reached
      else { //Internal nodes
        val node = root match { //Look for branch
          case x: InnerTreeNode => x.ptrs.getOrElse(toBeProcessed.head, null)
          case _ => throw new Error("MALFORMED BATCH TREE")
        }
        val subtree = if(node != null){
          //Branch found
          node
        } else {
          //Branch does not exist => CREATION
          val newNode = if(toBeProcessed.tail.isEmpty) new LeafTreeNode(new ListBuffer) else new InnerTreeNode(scala.collection.mutable.Map())
          root match {
            case x: InnerTreeNode => x.ptrs += (toBeProcessed.head -> newNode)
            case _ => throw new Error("MALFORMED BATCH TREE")
          }
          newNode
        }
        recursiveTreeTraversal(subtree, toBeProcessed.tail)
      }
    }

    val codeSeq = entry.groupBy.map(_._2) ::: entry.code :: Nil

    //Obtaining the batch by searching and extending the Batch tree
    val batch = recursiveTreeTraversal(batchInformation.batchTree, codeSeq) match {
      case x:LeafTreeNode => x
      case _ => throw new Error("MALFORMED BATCH TREE")
    }

    //Updates the mergeCount for the codeSeq
    //Adding 1 to that code mergeCount before inserting the new sample in the batch
    batchInformation.mergeCount(codeSeq) = 1 + batchInformation.mergeCount.getOrElse(codeSeq,0)

    batch.data += Sample(entry.sample.getv().toList)
  }
}

class CodifyResult(val sample: RichDouble1DArray,
                   val initial: Map[Phase2Subphase.Value, Boolean],
                   val codes: Map[Phase2Subphase.Value, SubphaseBatchIdentification.Code],
                   val bindings: Map[Phase2Subphase.Value, List[Binding]],
                   val monotony: Map[Feature.ID, FeatureMonotony.Value])

class BatchEntry(val sample: RichDouble1DArray, val code: SubphaseBatchIdentification.Code, val groupBy: List[(Phase2Subphase.Value, SubphaseBatchIdentification.Code)], val initial:Boolean)
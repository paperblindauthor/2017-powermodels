package it.necst.marc.phase1.post.data.support

import it.necst.marc.data.support.{OptionalTag, ExtractableEnumeration}

/**
 * Created by andreadamiani on 07/10/15.
 */
object Phase2Subphase extends ExtractableEnumeration {
  val A = Value(OptionalTag.A.toString)
  val B = Value(OptionalTag.B.toString)
  val C = Value(OptionalTag.C.toString)

  val requiresSmallBatch    = Map(A -> true,              B -> false,         C -> true)
  val requiresExpandedBatch = Map(A -> false,             B -> false,         C -> false)
  //NOTA BENE A DEVE ASSOLUTAMENTE DIPENDERE DA C, in caso di configurazione parametrica assicurarsi che il
  //requisito sia assicurato
  //TODO esplorare mancato legame tra A e C
  val groupingRules       = Map(A -> (B :: C :: Nil),   B -> Nil,           C -> (B :: Nil))
}

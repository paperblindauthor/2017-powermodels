package it.necst.marc.phase1.post.computation

import it.necst.marc.data.{Feature,FeatureType}
import it.necst.marc._
import scalaSci.RichDouble1DArray

/**
  * Created by andrea on 08/11/15.
  */
object CategoricalExpansion {

  def apply(sample: RichDouble1DArray,features: List[Feature]): RichDouble1DArray = {


    val featureToAdd = features.filter(_.specs.feature_type.equals(FeatureType.CATEGORICAL)).sortBy(_.column_index)

    val featureToRemove = featureToAdd.sortWith((x: Feature,y:Feature) => {x.column_index > y.column_index})

    def addColumns(toBeProcessed: List[it.necst.marc.data.Feature],newRow: List[Double]):  List[Double] = {
      if(toBeProcessed.isEmpty) newRow
      else{
        val feature = toBeProcessed.head
        val rowUpdated = addColumnForFeature(feature,feature.specs.bounds.lower.get.toInt,newRow,newRow(feature.column_index).toInt)
        addColumns(toBeProcessed.tail,rowUpdated)
      }
    }

    def addColumnForFeature(feature: Feature,currentIndex: Int,newRow: List[Double],currentValue: Int): List[Double] = {
      if(currentIndex > feature.specs.bounds.upper.get.toInt) newRow
      else{
        val number = if(currentIndex == currentValue) 1.0 else 0.0
        val rowUpdated = newRow :+ number
        addColumnForFeature(feature,currentIndex+1,rowUpdated,currentValue)
      }
    }


    def removeFeatures(toBeProcessed: List[Feature], updatedSample: List[Double]): List[Double] = {
      if(toBeProcessed.isEmpty) updatedSample
      else{
        val columnIndex = toBeProcessed.head.column_index
        val (first,second) = updatedSample.splitAt(columnIndex)

        removeFeatures(toBeProcessed.tail, first ++ second.tail)
      }
    }

    val expanded = addColumns(featureToAdd,sample.getv().toList)

    toRichDouble1DArray(removeFeatures(featureToRemove,expanded).toArray)


  }

  def apply(features: List[it.necst.marc.data.Feature]): List[it.necst.marc.data.Feature] = {

    def expandFeatures(toBeProcessed: List[it.necst.marc.data.Feature],
                       newFeaturesList: List[it.necst.marc.data.Feature],
                       index: Int): List[it.necst.marc.data.Feature] = {
      if(toBeProcessed.isEmpty) newFeaturesList

      else{
        val featureToExpand = toBeProcessed.head
        val dataNextIter = if(featureToExpand.specs.feature_type.equals(FeatureType.CATEGORICAL)){
          (expandFeature(featureToExpand.specs.bounds.lower.get.toInt,
            index, featureToExpand, newFeaturesList),
            index + Math.abs(featureToExpand.specs.bounds.upper.get.toInt - featureToExpand.specs.bounds.lower.get.toInt) + 1 )
        }else{
          (newFeaturesList :+ featureToExpand,index)
        }
        expandFeatures(toBeProcessed.tail,dataNextIter._1,dataNextIter._2)

      }
    }

    def expandFeature(currentValue: Int,
                      currentIndex: Int,
                      feature: it.necst.marc.data.Feature,
                      featuresList: List[it.necst.marc.data.Feature]):List[it.necst.marc.data.Feature] = {
      if(currentValue > feature.specs.bounds.upper.get) featuresList

      else{
        val newName = createNameCategoricalExpansion(feature.name,currentValue)
        val isTime = if(feature.is_time.isDefined) true else false
        val newFeature = it.necst.marc.data.Feature(newName,currentIndex,isTime,FeatureType.INSTANTANEOUS,
          feature.name,currentValue.toDouble,feature.specs.monotony,(Some(0),Some(1)))
        val newListFeature = featuresList :+ newFeature
        expandFeature(currentValue+1,currentIndex+1,feature,newListFeature)
      }
    }
    expandFeatures(features,Nil,features.size)
  }


  def apply(featuresToEpurate: List[it.necst.marc.data.Feature],
                          categoricFeatures: List[it.necst.marc.data.Feature]): List[it.necst.marc.data.Feature] = {


    def updateMap(toBeProcessed: List[it.necst.marc.data.Feature], map: Map[Int,Int]): Map[Int,Int] = {
      if(toBeProcessed.isEmpty) map
      else{
        val mapUpdated = increaseCounter(map,Map[Int,Int](),toBeProcessed.head.column_index)
        updateMap(toBeProcessed.tail,mapUpdated)
      }
    }

    def increaseCounter(toBeProcessed: Map[Int,Int],finalMap: Map[Int,Int],currentIndex: Int): Map[Int,Int] = {
      if(toBeProcessed.isEmpty) finalMap
      else{
        val newMap = if(toBeProcessed.head._1 > currentIndex){
          finalMap + (toBeProcessed.head._1 -> (toBeProcessed.head._2 + 1))
        }else{
          finalMap + toBeProcessed.head
        }
        increaseCounter(toBeProcessed.tail,newMap,currentIndex)
      }
    }

    def updateFeatures(toBeProcessed: Map[Int,Int], features: List[it.necst.marc.data.Feature]): List[it.necst.marc.data.Feature]  = {
      if(toBeProcessed.isEmpty) features
      else{
        val feature = features.find(_.column_index == toBeProcessed.head._1).head
        val newList = features.filter(!_.name.equals(feature.name)) :+
          it.necst.marc.data.Feature(feature.name,feature.column_index - toBeProcessed.head._2,feature.is_time,feature.specs)
        updateFeatures(toBeProcessed.tail,newList)
      }
    }

    val map = (for(feature <- featuresToEpurate) yield feature.column_index -> 0).toMap[Int,Int]
    val mapUpdated = updateMap(categoricFeatures,map)
    updateFeatures(mapUpdated,featuresToEpurate)
  }

  private def createNameCategoricalExpansion(name: String,value: Double) = name + "_" + value.toInt




}

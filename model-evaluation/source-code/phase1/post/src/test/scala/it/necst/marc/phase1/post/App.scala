//TODO - Outdated code
/*package it.necst.marc.phase1.post

import it.necst.marc.data.{Binding, FeatureMonotony, Feature}
import it.necst.marc.phase1.post.computation.{BatchEntry, CodifyResult, SubphaseBatchIdentification}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.internal.{InnerTreeNode, BatchInformation}
import it.necst.marc.phase1.post.data.support.Phase2Subphase

import scalaSci.RichDouble1DArray

/**
 * Created by andreadamiani on 01/11/15.
 */
object LocalTestApp extends scala.App{
  val configuration = Configurations.cfg
  val result = DataSets.result

  val dataset = result.dataForProcessing

  var monotony:Map[Feature.ID, FeatureMonotony.Value] = Map()

  val dictionaryA = scala.collection.mutable.Map[Code, List[Binding]]()
  val dictionaryB = scala.collection.mutable.Map[Code, List[Binding]]()
  val dictionaryC = scala.collection.mutable.Map[Code, List[Binding]]()

  var i = 1

  val bucketCodA = scala.collection.mutable.Map[Code, List[RichDouble1DArray]]()
  val bucketCodB = scala.collection.mutable.Map[Code, List[RichDouble1DArray]]()
  val bucketCodC = scala.collection.mutable.Map[Code, List[RichDouble1DArray]]()
  
  var codifyResults:List[CodifyResult] = Nil

  while (i<dataset.numRows()) {
    val sample = new RichDouble1DArray(dataset.getRow(i))
    val prevSample = new RichDouble1DArray(dataset.getRow(i-1))
    val ret = SubphaseBatchIdentification.codify(sample, prevSample, monotony, configuration, result.features)
    dictionaryA += ret.codes(Phase2Subphase.A) -> ret.bindings(Phase2Subphase.A)
    dictionaryB += ret.codes(Phase2Subphase.B) -> ret.bindings(Phase2Subphase.B)
    dictionaryC += ret.codes(Phase2Subphase.C) -> ret.bindings(Phase2Subphase.C)

    monotony = ret.monotony

    bucketCodA += ret.codes(Phase2Subphase.A) -> (sample :: bucketCodA.getOrElse(ret.codes(Phase2Subphase.A), Nil))
    bucketCodB += ret.codes(Phase2Subphase.B) -> (sample :: bucketCodB.getOrElse(ret.codes(Phase2Subphase.B), Nil))
    bucketCodC += ret.codes(Phase2Subphase.C) -> (sample :: bucketCodC.getOrElse(ret.codes(Phase2Subphase.C), Nil))

    codifyResults = ret :: codifyResults
    i+=1
  }


  i = 0

  var bucketBatchA:List[BatchEntry] = Nil
  var bucketBatchB:List[BatchEntry] = Nil
  var bucketBatchC:List[BatchEntry] = Nil

  while(i<codifyResults.size) {
    val codResult = codifyResults(i)

    val ret = SubphaseBatchIdentification.batchify(codResult.sample, codResult.codes, configuration, result.features)

    bucketBatchA = ret(Phase2Subphase.A) :: bucketBatchA
    bucketBatchB = ret(Phase2Subphase.B) :: bucketBatchB
    bucketBatchC = ret(Phase2Subphase.C) :: bucketBatchC

    i+=1
  }

  //Outdated
  /*val mappingsA = SubphaseBatchIdentification.updateMappingsForPhase(Phase2Subphase.A, configuration, result.features)
  val mappingsB = SubphaseBatchIdentification.updateMappingsForPhase(Phase2Subphase.B, configuration, result.features)
  val mappingsC = SubphaseBatchIdentification.updateMappingsForPhase(Phase2Subphase.C, configuration, result.features)

  val resA = new BatchInformation(new InnerTreeNode(scala.collection.mutable.Map()), scala.collection.mutable.Map())
  val resB = new BatchInformation(new InnerTreeNode(scala.collection.mutable.Map()), scala.collection.mutable.Map())
  val resC = new BatchInformation(new InnerTreeNode(scala.collection.mutable.Map()), scala.collection.mutable.Map())

  i=0
  while(i<bucketBatchA.size){
    SubphaseBatchIdentification.incrementalBatchGeneration(resA, bucketBatchA(i), mappingsA)
    i+=1
  }

  i=0
  while(i<bucketBatchB.size){
    SubphaseBatchIdentification.incrementalBatchGeneration(resB, bucketBatchB(i), mappingsB)
    i+=1
  }

  i=0
  while(i<bucketBatchC.size){
    SubphaseBatchIdentification.incrementalBatchGeneration(resC, bucketBatchC(i), mappingsC)
    i+=1
  }

  val resAferq = resA.mergePercentages
  val resBfreq = resB.mergePercentages
  val resCferq = resC.mergePercentages*/
}
*/
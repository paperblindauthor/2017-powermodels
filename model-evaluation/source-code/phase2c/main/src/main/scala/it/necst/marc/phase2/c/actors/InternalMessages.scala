package it.necst.marc.phase2.c.actors

import akka.actor.ActorRef
import it.necst.marc.data.{Feature, PartialStat}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.{ConfigurationCode, ModelConfiguration}
import it.necst.marc.phase2.c.data.{Distribution, Value, Entry, Mean}

import scalaSci.{RichDouble1DArray, RichDouble2DArray}

sealed trait Message


//internalActor messages
case class ComputationMeansCompleted(means: List[Mean],entries: List[Entry],stats: List[PartialStat]) extends Message
case class ThrowsError(message: String) extends Message

//CollectorPhase2C messages
case class StartCollectorPhase2CComputation() extends Message
case class ValuesComputed(mean: Mean,entry: Entry) extends Message

//MeaninputActor messages
case class Compute(modelConfiguration: ModelConfiguration, sender: ActorRef) extends Message
case class BatchComputed(mean: List[Value], rows:Int, code: Code)
case class DistributionComputed(dist: Distribution, code: Code)

//MeanInputBatchActor messages
case class ComputeBatch(replyTo: ActorRef, batch:RichDouble2DArray, codes:Code) extends Message
case class ComputeDistribution(replyTo: ActorRef, id:Feature.ID, feature:RichDouble1DArray, codes:Code) extends Message
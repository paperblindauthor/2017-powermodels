package it.necst.marc.phase2.c.actors

import akka.actor.{Actor, Props}
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.phase2.c.computation.{DistributionComputation, MeanInput}

/**
  * Created by andreadamiani on 08/02/16.
  */
object MeanInputBatchActor{

  def props(features: List[it.necst.marc.data.Feature],
            indexTimeFeature: Int, indexOutputFeature: Int,
            tick: Double): Props = Props(new MeanInputBatchActor(features,indexTimeFeature,indexOutputFeature,tick))
}

class MeanInputBatchActor(features: List[it.necst.marc.data.Feature],
                     indexTimeFeature: Int, indexOutputFeature: Int,
                     tick: Double) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  def receive: Receive = {
    case ComputeBatch(replyTo, batch,code) =>
      try{
        val values = MeanInput(features, batch ,indexTimeFeature, indexOutputFeature, tick)

        replyTo ! BatchComputed(values, batch.numRows(), code)
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          replyTo ! ThrowsError(error)
      }

    case ComputeDistribution(replyTo,feature,batch,code) =>
      try{
        val values = DistributionComputation(feature,batch)

        replyTo ! DistributionComputed(values, code)
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          replyTo ! ThrowsError(error)
      }

  }

}

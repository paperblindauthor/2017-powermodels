package it.necst.marc.phase2.c.data.internal

import it.necst.marc.data.support.ExtractableEnumeration

/**
  * Created by andrea on 25/11/15.
  */
object MeanInputStats extends ExtractableEnumeration {

  val FIRST_REQUEST_SENT     = Value("FirstRequestSent")
  val ALL_RESPONSES_RECEIVED = Value("AllResponsesReceived")

}

object DataManagementStats extends ExtractableEnumeration{

  val DATA_DECOMPRESSION           = Value("DataDecompression")
  val DATA_COMPRESSION             = Value("DataCompression")

}

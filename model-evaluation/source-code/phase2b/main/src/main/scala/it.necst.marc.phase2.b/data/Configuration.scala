package it.necst.marc.phase2.b.data

import it.necst.marc.data.AbstractConfiguration
import it.necst.marc.phase1.post.data.ConfigurationFeature

/**
 * Created by andreadamiani on 14/07/15.
 */
case class Configuration(periodicity: Double, minimumBand: Double, aggregationThreshold: Double)extends AbstractConfiguration{
  require(periodicity>=0,"periodicity must be positive")
  require(minimumBand>0,"minimumBand must be strictly positive")
  require(aggregationThreshold<=1 && aggregationThreshold>=0,"aggregration threshold must be in [0,1] interval")
  require((periodicity/minimumBand) % 1 == 0,"periodicity/minimumBand must be integer")
}

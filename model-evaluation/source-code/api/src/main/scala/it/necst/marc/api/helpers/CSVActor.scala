package it.necst.marc.api.helpers

import akka.actor.{Actor, Props}
import it.necst.marc.data.Sample

/**
  * Created by andrea on 24/12/15.
  */

object CSVActor{

  def props(separator: String,timeFeatureIndex: Int): Props = Props(new CSVActor(separator,timeFeatureIndex))

}


class CSVActor(separator: String, timeFeatureIndex: Int) extends Actor{

  def receive: Receive = {
    case StartScan(data) =>

      try {
        val samples: List[Sample] = scan(data, Nil)

        val dataOrdered: scala.collection.mutable.ListBuffer[Sample] = scala.collection.mutable.ListBuffer[Sample]()
        for (entry <- samples) {
          dataOrdered.indexWhere(_.sample(timeFeatureIndex) > entry.sample(timeFeatureIndex)) match {
            case -1 => dataOrdered += entry
            case n => dataOrdered.insert(n, entry)
          }
        }
        sender() ! DataCreated(dataOrdered.toList)
      } catch {
        case e:Throwable =>
          sender() ! e
      }

    case e:Throwable =>
      sender() ! e
  }

  private def scan(toBeProcessed: List[String], result: List[Sample]):List[Sample] = {
    if(toBeProcessed.isEmpty) result
    else scan(toBeProcessed.tail,result :+ createSampleFromLineCSV(toBeProcessed.head.split(separator)))
  }
  private def createSampleFromLineCSV(numbers: Array[String]):Sample = {

    def scanAndCreate(toBeProcessed: Array[String],tmpSample: List[Double]): Sample = {
      if(toBeProcessed.isEmpty) Sample(tmpSample)
      else{
        val number = toBeProcessed.head.toDouble
        scanAndCreate(toBeProcessed.tail,tmpSample :+ number)
      }
    }
    scanAndCreate(numbers,Nil)
  }
}

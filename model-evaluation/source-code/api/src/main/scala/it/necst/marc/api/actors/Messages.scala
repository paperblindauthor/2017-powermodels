package it.necst.marc.api.actors

/**
  * Created by andrea on 15/01/16.
  */
sealed trait Message

case class StartInsertDataInMarc() extends Message
case class StartDownloadResults(uri: String) extends Message

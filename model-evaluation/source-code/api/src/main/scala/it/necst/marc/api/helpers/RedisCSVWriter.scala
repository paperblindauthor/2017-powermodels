package it.necst.marc.api.helpers

import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern._
import akka.util.Timeout
import com.redis.RedisClient
import it.necst.marc.data.support.ErrorStack
import it.necst.marc.data.{ResultExternalRedis, Sample}
import it.necst.marc.infrastructure.support.URIManager.URIManagerKeys
import it.necst.marc.infrastructure.support.{ConstantsKeys, DataCompressor, DataEncoder}
import rapture.json._
import rapture.json.jsonBackends.jawn._

import scala.concurrent.Await
import scala.io.Source

/**
  * Created by andrea on 24/12/15.
  */
object RedisCSVWriter {

  private val redis = new RedisClient(ConstantsKeys.REDIS_OUT_IP.toString, ConstantsKeys.REDIS_OUT_PORT.toString.toInt)

  def main(args : Array[String]): Unit = {
    val file = new File("./file_processed.txt")
    if(!file.exists()){
      file.createNewFile()
    }
    val pathFile = args(0)
    val nameFile = pathFile.split("/").last.replace(".csv","")

    RedisCSVWriter(pathFile,";",0,"Andreas","MPower")

    val fileWriter = new FileWriter(file.getAbsoluteFile,true)
    val bufferWriter = new BufferedWriter(fileWriter)

    bufferWriter.write(nameFile+"\n")
    bufferWriter.close()
    System.exit(0)
  }

  def apply(filePath: String,separator: String,timeFeatureIndex: Int,username: String,jobName: String) = {
    val nameFile = filePath.split("/").last.replace(".csv","")

    val key = "external/result?"+URIManagerKeys.USERNAME.toString+"="+username+"&"+URIManagerKeys.JOB_NAME.toString+"="+jobName+"&"+URIManagerKeys.DATASET.toString+"="+nameFile
    val keyDim = key + ":dim"

    var stringData = Source.fromFile(filePath).getLines().toList
    if(!stringData.head.split(separator).forall{value:String =>
        try {
          value.toDouble
          true
        } catch {
          case e:NumberFormatException => false
        }
      }){stringData = stringData.tail} //Deletes possible header line

    val actor = ActorSystem("RedisCSVWriter").actorOf(CSVCollector.props(stringData,separator,timeFeatureIndex))

    implicit val timeout =  Timeout(60,TimeUnit.MINUTES)
    try {
      val data = Await.result(actor ? StartCollector(), timeout.duration).asInstanceOf[List[Sample]]

      stringData = null

      val result = Json(ResultExternalRedis(data)).toString()

      val compressed = DataCompressor.compress(result)
      val dataDim = result.getBytes("UTF-8").length

      redis.connect
      redis.set(key,DataEncoder.encode(compressed))
      redis.set(keyDim,dataDim)
      redis.disconnect

      key
    } catch {
      case e:Throwable =>
        throw new FileImportFailedException(s"The import of file $nameFile failed.\n" +
          s"Follows the causing exception:\n" +
          s"${ErrorStack(e)}")
    }
  }


  class FileImportFailedException(s: String) extends RuntimeException

}

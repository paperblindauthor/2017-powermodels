package it.necst.marc.api.actors

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, Props}
import akka.util.Timeout
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import java.io.File

import it.necst.marc.api.helpers.RedisCSVWriter
import it.necst.marc.infrastructure.support.{EmailSender, EmailSubjects}

import scala.concurrent.ExecutionContext

/**
  * Created by andrea on 15/01/16.
  */
object LoaderActor {

  final val FTP_DIRECTORY = "/home/ftp"

  def props(directoryFiles: String,
            separator: String,
            timeFeatureIndex: Int,
            username: String,
            jobName: String,
            email:String): Props = Props(new LoaderActor(directoryFiles,separator,timeFeatureIndex,username: String, jobName,email))

}
class LoaderActor(folderName: String,
                  separator: String,
                  timeFeatureIndex: Int,
                  username: String,
                  jobName: String,
                  email:String) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private final val dataURI = "marc://redis_external.marc.docker:6379"


  def receive: Receive = {
    case StartInsertDataInMarc() =>
      logger.debug("Start insertion data")

      val d = new File(LoaderActor.FTP_DIRECTORY+"/"+folderName)
      val files = if (d.exists && d.isDirectory) {
        d.listFiles.filter(_.isFile).toList.filter(_.getName.contains(".csv"))
      } else {
        List[File]()
      }

      for(file<-files){
        val succeeded = try {
          RedisCSVWriter(file.getAbsolutePath, separator, timeFeatureIndex, username, jobName)
          true
        } catch {
          case  e:Throwable =>
            false
        }

        logger.debug(s"File ${file.getName} insertion succeeded? $succeeded")

        try {
          if (succeeded) {
            if(!file.delete()){
              logger.warn(s"Unable to delete ${file.getAbsolutePath} after successful loading.\n")
            }
          }
        } catch {
          case e:Throwable =>
            logger.warn(s"Unable to delete ${file.getAbsolutePath} after successful loading.\nFollows full cause:\n${ErrorStack(e)}\n")
        }

        implicit val ec = ExecutionContext.Implicits.global
        implicit lazy val timeout =  Timeout(5,TimeUnit.SECONDS)

        val message = if(succeeded) "Hello\nYour file "+file.getName+" have been inserted in our system" else s"Hello\nYour file ${file.getName} was NOT loaded into our system.\nPlease verify its format and retry."
        EmailSender(email,(if(succeeded)EmailSubjects.DATA_INSERTED else EmailSubjects.DATA_NOT_INSERTED).toString,message)

      }
      context.stop(self)
  }
}

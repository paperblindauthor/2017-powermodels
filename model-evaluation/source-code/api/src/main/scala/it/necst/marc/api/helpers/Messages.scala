package it.necst.marc.api.helpers

import it.necst.marc.data.{Sample}

/**
  * Created by andrea on 15/11/15.
  */
sealed trait Message


case class StartCollector() extends Message
case class StartScan(data: List[String]) extends Message
case class DataCreated(data: List[Sample]) extends Message
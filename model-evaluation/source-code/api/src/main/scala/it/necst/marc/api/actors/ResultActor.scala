package it.necst.marc.api.actors

import java.io.File
import java.nio.charset.Charset
import java.nio.file.{Paths, Path, StandardOpenOption, Files}

import akka.actor.{Props, ActorRef, ActorSelection, Actor}
import it.necst.marc.infrastructure.actors.messages.{TakeAllResults, GiveMeAllResults}
import it.necst.marc.infrastructure.support.DataCompressor


/**
  * Created by andrea on 19/01/16.
  */

object ResultActor{

  def props(loadBalancerRedisPath: String): Props = Props(new ResultActor(loadBalancerRedisPath))

}


class ResultActor(loadBalancerRedisPath: String) extends Actor{

  private var loadBalancerExternalRedis: ActorSelection = _
  private var caller: ActorRef = _

  override def preStart()  = {
    loadBalancerExternalRedis = context.system.actorSelection(loadBalancerRedisPath)
  }
  def receive: Receive = {
    case StartDownloadResults(uri) =>
      caller = sender()
      loadBalancerExternalRedis ! GiveMeAllResults(uri,self)

    case TakeAllResults(results) =>

      val outputDirectory = java.nio.file.Files.createTempDirectory(Paths.get("/run/jetty"),"result")
      val files = createFile(results,outputDirectory,Nil)
      caller ! (files,outputDirectory)


  }

  private def createFile(toBeProcessed: List[(String,(Array[Byte],Int))],outputFolder: Path, files: List[File]): List[File] = {
    if(toBeProcessed.isEmpty) files
    else{

      val outputFile = outputFolder.resolve(toBeProcessed.head._1.replace("/","-")+".json")
      val file = Files.createFile(outputFile)

      val data = new String(DataCompressor.decompress(toBeProcessed.head._2._1,toBeProcessed.head._2._2))
      val bufferWriter = Files.newBufferedWriter(outputFile,Charset.forName("UTF-8"),StandardOpenOption.WRITE,StandardOpenOption.CREATE,StandardOpenOption.TRUNCATE_EXISTING)
      bufferWriter.write(data)
      bufferWriter.close()

      createFile(toBeProcessed.tail,outputFolder,file.toFile :: files)
    }
  }

}

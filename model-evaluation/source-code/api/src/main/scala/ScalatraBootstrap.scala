import javax.servlet.ServletContext

import it.necst.marc.api.{ResultServlet, SimulationServlet, FileServlet, MainServlet}
import org.scalatra.LifeCycle
import com.typesafe.config.ConfigFactory

/**
  * Created by andrea on 14/01/16.
  */
class ScalatraBootstrap extends LifeCycle {


  override def init(context: ServletContext) {


    context mount (new MainServlet, "/*")
    context mount (new FileServlet, "/file/*")

    val config = ConfigFactory.load("api_configuration")
    val entrypointLoadBalancerPath = config.getString(config.getString("name")+".remote_addresses.entry_point")
    context mount (new SimulationServlet(entrypointLoadBalancerPath), "/simulate/*")

    val redisExternalLoadBalancer = config.getString(config.getString("name")+".remote_addresses.external")

    context mount (new ResultServlet(redisExternalLoadBalancer),"/result/*")


  }
}
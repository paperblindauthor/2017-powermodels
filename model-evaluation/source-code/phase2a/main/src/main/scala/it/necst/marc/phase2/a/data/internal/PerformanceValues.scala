package it.necst.marc.phase2.a.data.internal

import it.necst.marc.data.support.ExtractableEnumeration

/**
  * Created by andrea on 25/11/15.
  */
object ChunkIdentificationStats  extends ExtractableEnumeration{
  val ALL_RESPONSES_RECEIVED  = Value("AllResponseReceived")
  val FIRST_REQUEST_SENT      = Value("FirstRequestSent")

}


object ModelComputationStats extends ExtractableEnumeration{
  val ALL_RESPONSES_RECEIVED  = Value("AllResponseReceived")
  val FIRST_REQUEST_SENT      = Value("FirstRequestSent")
}

object DataManagementStats extends ExtractableEnumeration{

  val DATA_DECOMPRESSION           = Value("DataDecompression")
  val DATA_COMPRESSION             = Value("DataCompression")

}
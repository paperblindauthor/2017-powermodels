package it.necst.marc.phase2.a.actors.chunk_identification

import akka.actor.{Actor, ActorRef, Props}
import akka.routing.BalancingPool
import it.necst.marc.data.FeatureMonotony
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.ModelConfiguration
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase2.a.actors.model_computation.SingleModelActor
import it.necst.marc.phase2.a.actors._
import it.necst.marc.phase2.a.computation.{ChunkIdentification, ModelComputation}
import it.necst.marc.phase2.a.data.{PartialConfigurationModel, SpectralValue}
import it.necst.marc.phase2.a.data.internal.{Chunk, ChunkConfiguration, Configuration}
import it.necst.marc._

/**
 * Created by andrea on 21/09/15.
 */

object ChunkIdentificationActor{

  def props(): Props = Props(new ChunkIdentificationActor())
}

class ChunkIdentificationActor() extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private var pendingAlphas: Int = 0
  private var receivedAlphas: List[Array[Double]] = Nil
  private var outputFeatureMonotony: FeatureMonotony.Value = _
  private var code: Code = _
  private var configuration: Configuration = _
  private var collector: ActorRef = _
  private var routerActorRef: ActorRef = _
  private var modelInputs:ModelConfiguration = _


  def receive: Receive = {
    case ComputeChunks(modelCfg,modelInputs) =>
      try{
        logger.info("Received ComputeChunks message for codes " + modelInputs.codes)

        this.modelInputs = modelInputs

        collector = sender()
        outputFeatureMonotony = modelInputs.outputFeatureMonotony
        code = modelInputs.codes.find(_.subphase.equals(Phase2Subphase.A)).get.code
        configuration = modelCfg

        val result:(List[Chunk],Int) = ChunkIdentification(modelInputs,modelCfg)

        //TODO parallelize
        val conds = for(value <-for(c <- result._1) yield c.phi.conditionP2()) yield {
          if(value.isInfinity){
            SpectralValue(None)
          }else{
            SpectralValue(Some(value))
          }
        }

        val chunkConfiguration = ChunkConfiguration(result._1,code)
        val reportData = PartialConfigurationModel(code,result._1.size,result._2,conds)
        collector ! ComputationChunksComplete(reportData)

        context.become(waitingForModels)
        startModelComputation(chunkConfiguration, modelCfg)

      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          collector ! ThrowsError(error)
          context.system.stop(routerActorRef)
          context.stop(self)
      }
  }

  def waitingForModels: Receive = {
    case AlphasComputed(alphas) =>
      try {
        logger.info("Received AlphasComputed message for " + code + " result is " + alphas)
        pendingAlphas -= 1

        alphas match {
          case Some(alphasModel) =>
            receivedAlphas ::= alphasModel
          case _ => ;
        }

        if (pendingAlphas == 0) {
          val res = if(receivedAlphas.isEmpty){
            (None,None)
          } else {
            val weightedAlphas = ModelComputation.computeWeightedAlphas(receivedAlphas)
            val model = ModelComputation(weightedAlphas, outputFeatureMonotony, configuration, code)

            val squareErrors =
              Seq(ModelComputation.parallelSquareError(modelInputs.dataForProcessing, model.alphas, configuration))

            (Some(model),ModelComputation.computeMSE(squareErrors))
          }
          collector ! ModelComputed(res._1, res._2)
          context.system.stop(routerActorRef)
          context.stop(self)
        }
      } catch {
        case e:Throwable =>
          val error = ErrorStack(e)
          collector ! ThrowsError(error)
          context.system.stop(routerActorRef)
          context.stop(self)
      }

    case ThrowsError(error) =>
      collector ! ThrowsError(error)
      context.system.stop(routerActorRef)
      context.stop(self)
  }


  private def startModelComputation(chunkConfiguration: ChunkConfiguration, modelConfiguration: Configuration): Unit = {

    pendingAlphas = chunkConfiguration.chunks.size

    if(pendingAlphas == 0){
      collector ! ModelComputed(None, None)
      context.stop(self)
    } else {
      if(!context.system.isTerminated){
        routerActorRef = context.system.actorOf(BalancingPool(chunkConfiguration.chunks.size).props(SingleModelActor.props(modelConfiguration)), name = self.path.name.replace("$","1").replace("+","2a"))
        for (c <- chunkConfiguration.chunks) {
          routerActorRef ! ComputeSingleModel(c)
        }
      }

    }
  }

}

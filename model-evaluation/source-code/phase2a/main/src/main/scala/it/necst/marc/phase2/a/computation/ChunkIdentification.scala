package it.necst.marc.phase2.a.computation

import it.necst.marc.data.{FeatureType, FeatureMonotony}
import it.necst.marc.phase1.post.data.ModelConfiguration
import it.necst.marc.phase2.a.data.internal.{Configuration, Chunk}

import scala.annotation.tailrec
import scala.util.Random
import scalaSci.RichDouble2DArray

/**
 * Created by andrea on 21/09/15.
 */
object ChunkIdentification {
  def apply(input: ModelConfiguration, configuration: Configuration):(List[Chunk],Int) = {

    val outputMonotony = input.outputFeatureMonotony

    val chunkSize = configuration.chunkSize

    val chunkPreComputation = generateOutputAndPhi(input, configuration)
    val outputAndPhi = chunkPreComputation._1
    val shuffledOutputAndPhi = Random.shuffle(outputAndPhi)
    val splits = shuffledOutputAndPhi.grouped(chunkSize).toList

    def redistributeSmallSplit(splits:List[Seq[(Double, Array[Double])]], toBeProcessed: Seq[(Double, Array[Double])]): List[Seq[(Double, Array[Double])]] = {
      if(toBeProcessed.isEmpty) splits
      else {
        val currentSplit = splits.head
        val newSplit = toBeProcessed.head +: currentSplit
        val nextSplits = splits.tail :+ newSplit
        redistributeSmallSplit(nextSplits, toBeProcessed.tail)
      }
    }

    val chunks = (if(splits.isEmpty){
      Nil
    } else {
      if(splits.last.size == chunkSize){
        splits
      } else {
        splits.size match {
          case 1 =>
            Nil
          case 2 =>
            (splits(0) ++ splits(1)) :: Nil
          case _ =>
            val revSplits = splits.reverse
            redistributeSmallSplit(revSplits.tail, revSplits.head)
        }
      }
    }).map(_.unzip).map(x => (x._1.toArray, x._2.toArray))

    (for(c <- chunks) yield Chunk(it.necst.marc.toRichDouble1DArray(c._1), it.necst.marc.toRichDouble2DArray(c._2), outputMonotony), chunkPreComputation._2)
  }

  private def generateOutputAndPhi(input:ModelConfiguration, configuration: Configuration): (Seq[(Double, Array[Double])], Int) = {
    val data = input.dataForProcessing
    val outputMonotony = input.outputFeatureMonotony

    val outputColumn = data.getCol(configuration.outputColumnIndex)

    val time = data.getCol(configuration.timeFeatureIndex)

    val chunkWithoutFeaturesAndOutput = deleteFeaturesFromChunk(configuration.ToBeRemovedForModelComputation,data)

    val keptFeatureTypes = configuration.features.filterNot(configuration.ToBeRemovedForModelComputation.contains).sortBy(_.column_index).map(_.specs.feature_type).toArray

    val transposed = if(chunkWithoutFeaturesAndOutput.numColumns() != 0){
      Some(chunkWithoutFeaturesAndOutput.~)
    } else {
      None
    }

    val transposedInstantaneous = transposed match {
      case Some(fullArray) =>
        val array = fullArray.getv()
        val instantaneous = (for(i <- array.indices) yield {
          keptFeatureTypes(i) match {
            case FeatureType.INSTANTANEOUS => array(i)
            case FeatureType.CUMULATIVE => (array(i).toList :+ 0.0).sliding(2,1).map(array => array(0) - array(1)).toArray
            case _ =>
              throw new IllegalArgumentException("No CATEGORICAL features allowed into model phase")
          }
        }).toArray
        Some(new RichDouble2DArray(instantaneous))
      case None => None
    }

    val maxLag = if(configuration.lags.autoregressive >= configuration.lags.exogenous) configuration.lags.autoregressive else configuration.lags.exogenous

    val monotonyComparisonOperator: (Double, Double) => Boolean =
      outputMonotony match {
        case FeatureMonotony.ASCENDANT =>
          (l:Double, r:Double) => l>=r
        case FeatureMonotony.DESCENDANT =>
          (l:Double, r:Double) => l<=r
        case _ => throw new IllegalArgumentException("Output Feature monotony must be defined.")
      }

    def testWindowForMonotony(window:List[Double]): Boolean = window.sliding(2).forall{
      case elem1 :: elem2 :: Nil =>
        monotonyComparisonOperator(elem1,elem2)
      case _ => throw new RuntimeException("[ERROR] Sliding Window is too long.")
    }

    @tailrec def phiAndOutputRecursiveComputation(results:Seq[Option[(Double, Array[Double])]], i:Int):Seq[Option[(Double, Array[Double])]] = {
      if(i >= outputColumn.length) results
      else {
          val output = for(outLag <-  0 to -configuration.lags.autoregressive by -1) yield outputColumn(i+outLag)
          val result = if(time(i)-time(i-maxLag)>configuration.timeSplittingDiff) {
            None
          } else if(testWindowForMonotony(output.toList)){
            val curr = output.head
            val autoregressive:IndexedSeq[Double] = output.tail
            val exogenous:IndexedSeq[Double] = transposedInstantaneous match {
              case Some(transposedInstantaneous) => for {currentLine <- 0 until transposedInstantaneous.numRows()
                                                         exLag <- 0 to -configuration.lags.exogenous by -1}
                yield transposedInstantaneous.getRow(currentLine)(i + exLag)
              case None => IndexedSeq()
            }
            Some(curr, (autoregressive ++ exogenous).toArray[Double])
          } else {
            None
          }
          val newResults = results :+ result
          val newI = i + 1
          val newJumpCounter = 0
          phiAndOutputRecursiveComputation(newResults, newI)
      }
    }

    val phiAndOutput:Seq[Option[(Double, Array[Double])]] =
      phiAndOutputRecursiveComputation(Seq(), maxLag)

    (phiAndOutput.filter(_.isDefined).map(_.get), phiAndOutput.count(_.isEmpty))
  }

  /*private def computeChunks(toBeProcessed: RichDouble2DArray,
                            currentIndexRow: Int,
                            previousChunk: Chunk,
                            chunks: List[Chunk],
                            chunkSize: Int): (List[Chunk],Int) = {

    if(toBeProcessed.numRows() == currentIndexRow){
      if(previousChunk.data.numRows() == chunkSize+2){
        val reducedChunk = reduceChunk(previousChunk.data)
        val newList = chunks :+ Chunk(reducedChunk, previousChunk.outputMonotony)
        (newList,newList.size*2)
      }else{
        val removed = chunks.size * 2 + previousChunk.data.numRows()
        (chunks,removed)
      }

    }else{

      val dataForNextCall = if(previousChunk.data.numRows() == chunkSize + 2){
        val reducedChunk = reduceChunk(previousChunk.data)
        val newList = chunks :+ Chunk(reducedChunk, previousChunk.outputMonotony)
        val newChunk = Chunk(new RichDouble2DArray(Array(toBeProcessed.getRow(currentIndexRow))), previousChunk.outputMonotony)
        (newChunk,newList)

      }else {
        val newChunk = Chunk(previousChunk.data.>>(toBeProcessed.getRow(currentIndexRow)), previousChunk.outputMonotony)
        (newChunk,chunks)
      }

      computeChunks(toBeProcessed,currentIndexRow + 1,dataForNextCall._1,dataForNextCall._2,chunkSize)

    }
  }

  private def reduceChunk(currentChunk: RichDouble2DArray): RichDouble2DArray = {
    subtrackFromChunkFirstRow(currentChunk,0)
    var index = currentChunk.numRows() - 1
    def isIndexToKeep(n: Int) = if (n != index) true else false
    val newChunk = currentChunk.filterRows(isIndexToKeep)
    index = 0
    val finalChunk = newChunk.filterRows(isIndexToKeep)
    finalChunk
  }

  private def subtrackFromChunkFirstRow(chunk:RichDouble2DArray,currentRow:Int):RichDouble2DArray = {

    def updateSingleRow(chunk:RichDouble2DArray,currentRow:Int,currentColumn:Int):RichDouble2DArray = {
      if(currentColumn >= chunk.numColumns()) chunk
      else{
        chunk.update(currentRow, currentColumn, chunk.getRow(currentRow)(currentColumn)-chunk.getRow(0)(currentColumn))
        updateSingleRow(chunk,currentRow,currentColumn+1)
      }
    }

    if(currentRow >= chunk.numRows()) chunk
    else{
      updateSingleRow(chunk,currentRow,0)
      subtrackFromChunkFirstRow(chunk,currentRow+1)
    }
  }*/

  private def invertArray(arrayToInvert: Array[Double]): Array[Double] = {

    def invert(toBeProcessed: Array[Double], list: List[Double]): Array[Double] = {
      if(toBeProcessed.isEmpty) list.toArray[Double]
      else{
        val newList = toBeProcessed.head :: list
        invert(toBeProcessed.tail,newList)
      }
    }
    invert(arrayToInvert,Nil)
  }

  private def deleteFeaturesFromChunk(toBeProcessed: List[Int],chunk: RichDouble2DArray): RichDouble2DArray = {

    if(toBeProcessed.isEmpty) chunk

    else{
      val index = toBeProcessed.head
      //filter function in order to delete feature from matrix
      def isIndexToKeep(n: Int) = if (n != index) true else false
      val newBatch = chunk.filterColumns(isIndexToKeep)
      deleteFeaturesFromChunk(toBeProcessed.tail,newBatch)
    }
  }
}

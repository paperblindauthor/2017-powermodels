package it.necst.marc.phase2.a.data

import it.necst.marc.data.{AbstractConfiguration, Feature}

/**
 * Created by andreadamiani on 14/07/15.
 */
case class Configuration(output:Feature.ID, lags:LagsSpec, timeSplittingDiff:Double, chunkSize:Int) extends AbstractConfiguration{
  require(output!=null)
  require(lags!=null)
  require(chunkSize > 0)
  require(timeSplittingDiff > 0)
}

case class LagsSpec(autoregressive:Int, exogenous:Int){
  require(autoregressive>=1)
  require(exogenous>=0)
}

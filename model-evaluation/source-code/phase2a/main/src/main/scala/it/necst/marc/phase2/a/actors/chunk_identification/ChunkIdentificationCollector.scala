package it.necst.marc.phase2.a.actors.chunk_identification

import akka.actor.{ActorRef, Props, Actor}
import akka.routing.BalancingPool
import com.typesafe.config.Config
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.data.{PartialStat}
import it.necst.marc.phase1.post.data
import it.necst.marc.phase2.a.actors._
import it.necst.marc.phase2.a.data.{PartialConfigurationModel, Model, ConfigurationModel}
import it.necst.marc.phase2.a.data.internal._

/**
 * Created by andrea on 21/09/15.
 */


object ChunkIdentificationCollector{

  def props(configurations: List[data.ModelConfiguration],
            phaseCfg: Configuration,
            numberOfActors: Int,
            configFile: Config,
            mappings: List[it.necst.marc.data.Feature]) = Props(new ChunkIdentificationCollector(configurations,phaseCfg,numberOfActors,configFile,mappings))

}

class ChunkIdentificationCollector(val configurations: List[data.ModelConfiguration],
                                   val phaseCfg: Configuration,
                                   val numberOfActors: Int,
                                   val configFile: Config,
                                   val mappings: List[it.necst.marc.data.Feature]) extends Actor {

  private val logger = MarcLogger(getClass.getName)
  private var pendingReports: Int = _
  private var pendingModels: Int = _

  private var currentCaller: ActorRef = _
  private var partialReportDataList: List[PartialConfigurationModel] = Nil
  private var modelList: List[(Model,Option[Double])] = Nil

  private var chunkIdentificationActors: List[ActorRef] = Nil

  private var initTimestamp: Long = 0l
  private var firstRequestSent: Long = 0l
  private var allResponsesReceived: Long = 0l

  override def preStart(): Unit = {
    pendingReports = configurations.size
    pendingModels = configurations.size
    initTimestamp = System.currentTimeMillis()
  }

  def receive = initState

  def initState: Receive = {

    case StartChunkIdentificationComputation() =>
      logger.info("Received StartChunkIdentificationComputation message")
      try{
        currentCaller = sender()
        sendComputationMessage()
        context.become(collectResultState)
      }catch {
        case e: Throwable =>
          sender() ! ThrowsError(ErrorStack(e))
          for(actor<-chunkIdentificationActors) context.stop(actor)
          context.stop(self)
      }

  }

  private def isCompleted = pendingReports==0 && pendingModels==0
  private def onCompletion() = {
    logger.info("All models computed")
    allResponsesReceived = System.currentTimeMillis()
    val stats = List(PartialStat(ModelComputationStats.FIRST_REQUEST_SENT.toString,firstRequestSent-initTimestamp),
      PartialStat(ModelComputationStats.ALL_RESPONSES_RECEIVED.toString,allResponsesReceived-initTimestamp))

    val res = for(model <- modelList) yield {
      val partialReport = partialReportDataList.find(_.code == model._1.code) match {
        case Some(pr) => pr;
        case None => throw new IllegalArgumentException(s"Missing partial report for model ${model._1.code}")
      }

      (model._1, ConfigurationModel(partialReport, model._2))
    }

    currentCaller ! ModelComputationComplete(res.map(_._1),res.map(_._2),stats)
    for(actor<-chunkIdentificationActors) context.stop(actor)
    context.stop(self)
  }

  def collectResultState: Receive = {
    case ComputationChunksComplete(reportData) =>
      try{
        logger.info("Recieved ComputationChunksComplete message for " + reportData.code)
        pendingReports -= 1

        partialReportDataList = reportData :: partialReportDataList

        if(isCompleted){
          onCompletion()
        }
    } catch {
      case e:Throwable =>
        currentCaller ! ThrowsError(ErrorStack(e))
        for(actor<-chunkIdentificationActors) context.stop(actor)
        context.stop(self)
    }

    case ModelComputed(model,mse) =>
      try {
        logger.info("Recieved ModelComputed message for " + (if (model.isDefined) model.get.code else "nothing"))
        pendingModels -= 1

        model match {
          case Some(model) =>
            modelList ::= (model,mse)
          case _ => ;
        }

        if (isCompleted) {
          onCompletion()
        }
      } catch {
        case e:Throwable =>
          currentCaller ! ThrowsError(ErrorStack(e))
          for(actor<-chunkIdentificationActors) context.stop(actor)
          context.stop(self)
      }

    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      currentCaller ! ThrowsError(error)
      for(actor<-chunkIdentificationActors) context.stop(actor)
      context.stop(self)

  }

  //TODO clean
  /*private def initializeActors(chunkSize: Int):(List[String],List[ActorRef]) = {
    def createActors(currentIndex: Int, numberToCreate: Int, actorsName: List[String],actors: List[ActorRef]): (List[String],List[ActorRef]) = {
      if(currentIndex == numberToCreate) (actorsName,actors)
      else{
        val actorRef = system.actorOf(ChunkIdentificationActor.props(chunkSize), "chunkIdentificationActor"+currentIndex)
        val nameActor = "/user/chunkIdentificationActor"+currentIndex
        createActors(currentIndex + 1,numberToCreate,nameActor :: actorsName,actorRef :: actors)
      }
    }

    createActors(0,numberOfActors,Nil,Nil)
  }*/


  private def sendComputationMessage() ={
    def sendMessage(toBeProcessed: List[data.ModelConfiguration]):Unit = {
      if(toBeProcessed.nonEmpty){
        val actorRef = context.system.actorOf(ChunkIdentificationActor.props())
        chunkIdentificationActors ::= actorRef
        actorRef ! ComputeChunks(phaseCfg, toBeProcessed.head)
        sendMessage(toBeProcessed.tail)
      }
    }
    firstRequestSent = System.currentTimeMillis()
    sendMessage(configurations)
  }
}

package it.necst.marc.phase2.a.data

import it.necst.marc.data.{Performance, AbstractReport}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code

/**
  * Created by andreadamiani on 17/06/15.
  */

case class Report(content:Option[MathStats], error:Option[String]) extends AbstractReport(content, error)

case class MathStats(configurations:List[ConfigurationModel],
                     performance: Performance) extends it.necst.marc.data.MathStats{
  require(configurations.nonEmpty, "At least one configuration must have been found.")
  require(performance!=null,"performance must be defined")
}

case class ConfigurationModel(code:Code, chunks:Int, discarded:Int, spectral_condition_numbers:List[SpectralValue], mse:Option[Double]){
  require(chunks>=0, "Chunks found cannot be negative.")
  require(discarded>=0, "Discarded rows cannot be negative.")
  require(spectral_condition_numbers.size == chunks, "Required a condition number for each chunk.")
}
object ConfigurationModel{
  def apply(partial:PartialConfigurationModel, mse:Option[Double]):ConfigurationModel = {
    ConfigurationModel(partial.code, partial.chunks, partial.discarded, partial.spectral_condition_numbers, mse)
  }
}

case class PartialConfigurationModel(code:Code, chunks:Int, discarded:Int, spectral_condition_numbers:List[SpectralValue])

case class SpectralValue(value: Option[Double])



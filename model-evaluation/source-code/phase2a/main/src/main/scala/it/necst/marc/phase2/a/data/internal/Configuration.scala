package it.necst.marc.phase2.a.data.internal

import it.necst.marc.data.FeatureMonotony
import it.necst.marc.phase2.a.data.LagsSpec


/**
 * Created by andrea on 21/09/15.
 */

object Configuration{

  def apply(features: List[it.necst.marc.data.Feature],
            configuration: it.necst.marc.phase2.a.data.Configuration): Configuration ={

    new Configuration(features,configuration.output, configuration.timeSplittingDiff, configuration.lags, configuration.chunkSize)
  }


}
class Configuration(val features: List[it.necst.marc.data.Feature],
                    val outputFeature: it.necst.marc.data.Feature.ID,
                    val timeSplittingDiff: Double,
                    val lags: LagsSpec,
                    val chunkSize: Int){

  val outputColumnIndex = features.filter(_.name.equals(outputFeature)).head.column_index

  val timeFeatureIndex = features.filter(_.is_time.isDefined).head.column_index

  val ToBeRemovedForModelComputation = {
    val list = timeFeatureIndex :: outputColumnIndex :: Nil
    def sortFn = (A:Int, B:Int) => { A > B }
    list.sortWith(sortFn)
  }



  /*private def computeIndexFeaturesToRemove: List[Int] = {

    val ToBeRemoved = configurationFeatures :+ outputFeature :+ features.filter(_.is_time.isDefined).head.name

    def isToKeep(feature: it.necst.marc.data.Feature) =
      if(ToBeRemoved.contains(feature.name)) true else false

    val featuresToBeUsed = features.filter(isToKeep(_))
    val indexesSet: List[Int] = for(feature <- featuresToBeUsed) yield feature.column_index

    def sortFn = (A:Int, B:Int) => { A > B }
    val sortedList = indexesSet.sortWith(sortFn)
    sortedList
  }*/
}

//TODO Bypassed code - review and delete
/*package it.necst.marc.phase2.a.actors.model_computation

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import it.necst.marc.data.PartialStat
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.phase2.a.actors._
import it.necst.marc.phase2.a.data.Model
import it.necst.marc.phase2.a.data.internal.{ChunkConfiguration, Configuration, ModelComputationStats}

/**
 * Created by andrea on 22/09/15.
 */

object ModelComputationCollector{

  def props(configurations: List[ChunkConfiguration],configuration: Configuration,numberOfActors: Int): Props =
    Props(new ModelComputationCollector(configurations,configuration,numberOfActors))
}

class ModelComputationCollector(val configurations: List[ChunkConfiguration],
                                val configuration: Configuration,
                                val numberOfActors: Int) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private val system = context.system

  private var waiting: Int = _
  private var modelsComputed: List[Model] = Nil
  private var currentInvocator: ActorRef = _

  private var initTimestamp: Long = 0l
  private var firstRequestTimestamp: Long = 0l
  private var allResponsesTimestamp: Long = 0l


  override def preStart() = {
    initTimestamp = System.currentTimeMillis()
  }

  def receive = initState

  def initState: Receive = {
    case StartModelComputation() =>
      logger.info("Received StartModelComputation message")
      waiting = configurations.size
      currentInvocator = sender()
      val actors = initializeActors()
      sendComputationMessage(actors._2)
      context.become(collectResultsState)

  }

  def collectResultsState: Receive = {
    case ModelComputed(model) =>
      waiting = waiting - 1
      modelsComputed = if(model.isDefined) model.get :: modelsComputed  else modelsComputed

      if(waiting == 0){
        logger.info("All models computed")
        allResponsesTimestamp = System.currentTimeMillis()
        val stats = List(PartialStat(ModelComputationStats.FIRST_REQUEST_SENT.toString,firstRequestTimestamp-initTimestamp),
              PartialStat(ModelComputationStats.ALL_RESPONSES_RECEIVED.toString,allResponsesTimestamp-initTimestamp))
        currentInvocator ! ModelComputationComplete(modelsComputed,stats)
        system.terminate()
        context.stop(self)

      }

    case ThrowsError(error) =>
      currentInvocator ! ThrowsError(error)
      system.terminate()
      context.stop(self)

  }

  private def initializeActors():(List[String],List[ActorRef]) = {
    def createActors(currentIndex: Int, numberToCreate: Int, actorsName: List[String],actors: List[ActorRef]): (List[String],List[ActorRef]) = {
      if(currentIndex == numberToCreate) (actorsName,actors)
      else{
        val newActorRef = system.actorOf(ModelComputationActor.props(configuration), "modelComputationActor"+currentIndex)
        val nameActor = "/user/modelComputationActor"+currentIndex
        createActors(currentIndex + 1,numberToCreate,nameActor :: actorsName,newActorRef::actors)
      }
    }

    createActors(0,configurations.size,Nil,Nil)
  }


  private def sendComputationMessage(actors: List[ActorRef]) ={

    def sendMessage(toBeProcessed: List[ChunkConfiguration],actorsToUse: List[ActorRef]):Unit = {
      if(toBeProcessed.nonEmpty){
        actorsToUse.head ! ComputeModels(toBeProcessed.head)
        sendMessage(toBeProcessed.tail,actorsToUse.tail)
      }
    }
    firstRequestTimestamp = System.currentTimeMillis()
    sendMessage(configurations,actors)
  }


}
*/
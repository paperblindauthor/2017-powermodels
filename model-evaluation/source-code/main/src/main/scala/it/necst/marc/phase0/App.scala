package it.necst.marc.phase0

import akka.actor.{ActorRef, ActorSystem}
import com.typesafe.config.{ConfigValue, Config, ConfigFactory}
import it.necst.marc.data.support.{MarcLogger, NamesPhases}
import it.necst.marc.infrastructure.actors.LoadBalancer
import it.necst.marc.phase0.actors.{InternalWorker}
import scala.collection.JavaConversions._
import scala.annotation.tailrec


/**
 * Created by andrea on 06/08/15.
 */
object App {

  private val logger = MarcLogger(getClass.getName)

  def main(args : Array[String]) {
    logger.info("Set up Phase0")

    val phase0Configuration = ConfigFactory.load("phase0_configuration")

    val phaseName = phase0Configuration.getString("name")
    val remoteAddresses = phase0Configuration.getConfig(phaseName+".remote_addresses")

    val remoteAddressMap: Map[NamesPhases.Value,String] = loadMapRemoteAddresses(remoteAddresses)


    val redisClientInternalLoadBalancerPath = remoteAddressMap(NamesPhases.REDIS)
    val redisClientExternalLoadBalancerPath = remoteAddressMap(NamesPhases.EXTERNAL)

    val _systemInternalWorker = ActorSystem("InternalWorkerSystem",ConfigFactory.load("internal_worker"))

    val internalActorsData = initializeInternalActors(_systemInternalWorker,phase0Configuration,redisClientInternalLoadBalancerPath,redisClientExternalLoadBalancerPath,phaseName)

    //initialization load balancer
    val _systemLoadBalancer = ActorSystem("LoadBalancerSystem",ConfigFactory.load("loadBalancer_phase0"))
    val loadBalancer = _systemLoadBalancer.actorOf(LoadBalancer.props(internalActorsData._2),"loadBalancerPhase0")

    System.gc()

  }

  private def initializeInternalActors(actorSystem: ActorSystem,config: Config,
                                       redisClientInternalLoadBalancerPath: String,
                                       redisClientExternalLoadBalancerPath: String,
                                        phaseName: String):(List[String],List[ActorRef]) = {

    val numberInternalActor = config.getInt(phaseName+".number_internal_actor")

    val timeoutRequestWaitState = config.getInt(phaseName+".timeout_wait_state")

    val timeoutReadyState = config.getInt(phaseName+".timeout_ready_state")


    def createActors(currentActorIndex: Int,
                     namesList: List[String], actorRefList: List[ActorRef]):(List[String],List[ActorRef]) ={
      if(currentActorIndex == numberInternalActor) (namesList,actorRefList)

      else{
        val newActorRef = actorSystem.actorOf(InternalWorker.props(config,phaseName,redisClientInternalLoadBalancerPath,redisClientExternalLoadBalancerPath,timeoutRequestWaitState,timeoutReadyState))
        val actorName = "/user/internalWorker"+currentActorIndex
        val newNamesList = actorName :: namesList
        val newActorRefList = newActorRef :: actorRefList
        createActors(currentActorIndex+1,newNamesList,newActorRefList)
      }
    }

    createActors(0,Nil,Nil)
  }

  private def loadMapRemoteAddresses(configuration: Config): Map[NamesPhases.Value,String] = {

    @tailrec def scanConfiguration(toBeProcessed: java.util.Set[java.util.Map.Entry[String, ConfigValue]], currentMap: Map[NamesPhases.Value,String]):Map[NamesPhases.Value,String] = {
      if (toBeProcessed.isEmpty) currentMap

      else {
        val newMap = currentMap + (NamesPhases.withName(toBeProcessed.head.getKey) -> toBeProcessed.head.getValue.render().replace("\"",""))
        scanConfiguration(toBeProcessed.tail, newMap)
      }
    }
    scanConfiguration(configuration.entrySet(), Map[NamesPhases.Value,String]())
  }
}

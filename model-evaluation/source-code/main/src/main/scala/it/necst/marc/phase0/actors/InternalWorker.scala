package it.necst.marc.phase0.actors


import akka.actor._
import com.typesafe.config.{Config}
import it.necst.marc.data.{PartialStat, PartialStats, PerfStats, Performance}
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.infrastructure.actors.messages._
import it.necst.marc.infrastructure.support.{DataCompressor, Job}
import it.necst.marc.phase0.data.internal.Phase0Stats
import it.necst.marc.phase0.data.{Configuration, Result}
import rapture.json._
import rapture.json.jsonBackends.jawn._
import java.net.URI

import scala.concurrent.duration._
/**
  * Created by andrea on 06/08/15.
  */
object InternalWorker {


  def props(configuration: Config,phaseName: String,redisClientInternalLoadBalancerPath: String,
            redisClientExternalLoadBalancerPath: String,
            timeoutRequestWaitState: Int,
            startTimeoutReadyState:Int): Props = Props(new InternalWorker(configuration,
                                                                          phaseName,
                                                                          redisClientInternalLoadBalancerPath,
                                                                          redisClientExternalLoadBalancerPath,
                                                                          timeoutRequestWaitState,startTimeoutReadyState))

}


class InternalWorker(val configuration: Config,
                     val phaseName: String,
                     val redisClientInternalLoadBalancerPath: String,
                     val redisClientExternalLoadBalancerPath: String,
                     val timeoutRequestWaitState: Int,
                     val startTimeoutReadyState:Int) extends Actor {

  private val logger = MarcLogger(getClass.getName)

  private var currentJob: Job = _
  private var currentConfiguration: Configuration = _
  private var currentReply: ActorRef = _
  private var currentLoadBalancer: ActorRef = _
  private var timeoutReadyState: Int = 0
  private var currentDataSet: String = _
  /*private var redisClientInternalLoadBalancer: ActorSelection = _
  private var redisClientExternalLoadBalancer: ActorSelection = _*/
  private var currentPhaseTag: Option[String] = None


  private var initTimestamp: Long = 0l

  override def preStart(): Unit = {
    timeoutReadyState = startTimeoutReadyState
  }

  def receive: Receive = initState


  def initState: Receive = {
    case SendJobCFG(jobString, config, dataSet,replyTo) =>
      logger.info("Received SendJobCFG message")
      initTimestamp = System.currentTimeMillis()
      currentJob = Job(jobString)
      currentDataSet = dataSet
      currentLoadBalancer = sender()
      currentConfiguration = Json.parse(config).as[it.necst.marc.phase0.data.Configuration]
      logger.info("Sending JobCFGReceived message")
      replyTo ! JobCFGReceived()
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

  }

  def readyState: Receive = {

    case SendJobCFG(jobString, config, dataSet,replyTo) =>
      logger.info("Received SendJobCFG message")
      currentJob = Job(jobString)
      currentLoadBalancer = sender()
      currentDataSet = dataSet
      currentConfiguration = Json.parse(config).as[it.necst.marc.phase0.data.Configuration]
      logger.info("Sending JobCFGReceived message")
      replyTo ! JobCFGReceived()
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

    case SendReqComputation(jobString, computationUnit, phaseTag,replyTo) =>
      logger.info("Received SendReqComputation message")

      val job = Job(jobString)
      if(job.equals(currentJob)){
        replyTo ! StartFetchData(URI.create(currentConfiguration.dataURI))
        context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
        context.become(readyState)
        currentLoadBalancer ! ComputationComplete(Job(currentJob),computationUnit)
      }else{
        replyTo ! ErrorWorkerMsg("job not defined in communication actor")
      }


    case ReceiveTimeout =>
      logger.info("Received Timeout message")
      logger.info("Sending DeallocateActorFromConfig message")
      currentLoadBalancer ! DeallocateActorFromConfig(self)
      resetInternalState()
      context.become(initState)
  }

  /*def preambleState: Receive = {

    case GoToPreambleState() =>
      logger.info("Received GoToPreambleState - Check if data are already ready")
      logger.info("Sending AreResultAlreadyComputedConfig message")
      val jobString = Job(currentJob)
      redisClientInternalLoadBalancer ! AreResultAlreadyComputedConfig(jobString,currentComputationUnit,phaseName,None,self)

    case DataFound(uri) =>
      logger.info("Received DataFound message")
      logger.info("Sending ResultReady message")
      currentReplyTo ! ResultReady(uri)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)

    case DataNotFound() =>
      logger.info("Received DataNotFound message")
      context.setReceiveTimeout(Duration(timeoutRequestWaitState,MILLISECONDS))
      context.become(waitState)
      redisClientExternalLoadBalancer ! ReadFromURI(URI.create(currentConfiguration.dataURI),self)


  }*/

  /*def waitState: Receive = {
    case DataReady(uri,dataByte,originaDim) =>
      startTimeDataManagement = System.currentTimeMillis()
      var data = new String(DataCompressor.decompress(dataByte,originaDim))
      val result = createResultObject(Json.parse(data).as[it.necst.marc.data.ResultExternalRedis].data,currentConfiguration)
      data = null
      val resultString = Json(result).toString()

      val jobString = Job(currentJob)

      val compressed = DataCompressor.compress(resultString)
      val dataDim = resultString.getBytes("UTF-8").length
      val finishTimestamp = System.currentTimeMillis()

      val performance = Performance(Some(PerfStats(finishTimestamp-initTimestamp,Some(PartialStats(List(PartialStat(Phase0Stats.DATA_MANAGEMENT.toString,finishTimestamp-startTimeDataManagement)))))))

      logger.info("Sending WriteResult message")
      redisClientInternalLoadBalancer ! WriteResult(jobString,currentComputationUnit,phaseName,compressed,dataDim,self)
      context.become(finalState)
  }
*/
 /* def finalState: Receive = {

    case KeyResultInserted(uri,phaseTag) =>
      logger.info("Received KeyResultInserted message")
      logger.info("Sending ResultReady message")
      currentReply ! ResultReady(uri)
      context.setReceiveTimeout(Duration(timeoutReadyState, MILLISECONDS))
      context.become(readyState)
      System.gc()

  }*/
/*
  private def createResultObject(listData: List[it.necst.marc.data.Sample], configuration: Configuration): Result = {
    val timeFeature = configuration.features.filter(_.name.equals(configuration.timeFeature)).head
    val newTimeFeature = it.necst.marc.data.Feature(timeFeature.name,timeFeature.column_index,Option(true),timeFeature.specs)
    val newListFeatures = configuration.features.filter(!_.name.equals(configuration.timeFeature)) :+ newTimeFeature
    Result(listData,newListFeatures)
  }
*/
  private def resetInternalState() = {
    currentJob  = null
    currentConfiguration = null
    currentReply = null
    currentLoadBalancer = null
    currentPhaseTag = None
    System.gc()
  }


}
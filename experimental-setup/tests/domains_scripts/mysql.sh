#!/bin/bash
sudo service mysql start
sleep 60s
sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --mysql-user=root --mysql-password=asplos --max-time=300 --oltp-read-only=on --max-requests=0 --num-threads=2 run
sudo service mysql stop
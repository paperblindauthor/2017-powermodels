#!/bin/bash

BENCHMARK_FOLDER=/home/marc/benchmarks
BT=$BENCHMARK_FOLDER/bt
CG=$BENCHMARK_FOLDER/cg
DC=$BENCHMARK_FOLDER/dc
EP=$BENCHMARK_FOLDER/ep
FT=$BENCHMARK_FOLDER/ft
LU=$BENCHMARK_FOLDER/lu
SP=$BENCHMARK_FOLDER/sp
UA=$BENCHMARK_FOLDER/ua
CACHEBENCH=$BENCHMARK_FOLDER/cachebench
IOZONE=$BENCHMARK_FOLDER/iozone
CANNEAL=$BENCHMARK_FOLDER/canneal
DEDUP=$BENCHMARK_FOLDER/dedup_infinite.sh

INPUT_FOLDER=$BENCHMARK_FOLDER/inputs
TIME=5s

export OMP_NUM_THREADS=2

sleep 30s

$BT &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$CG &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$DC &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$EP &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$FT &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$LU &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$SP &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$UA &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$CACHEBENCH -r -x1 -e2 -d20 &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$CACHEBENCH -w -x1 -e2 -d20 &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$CACHEBENCH -b -x1 -e2 -d20 &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$IOZONE -a -i 0 &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$CANNEAL 1 900000000 30 $INPUT_FOLDER/400000.nets &
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s

$DEDUP
PID=$!
sleep $TIME
kill -9 $PID
free && sync && echo 3 > /proc/sys/vm/drop_caches && free
sleep 15s


#choose between 4 and 8
export PMCS_AVAILABLE="4"
export MARC_PASS=""
export CORE_PER_SOCKET=""

export REMOTE_HOST_PATH="131.175.14.63:/home/"$REMOTE_USER"/TOBECOMPLETED"

#USERS
export ASPLOS_USER="marc"
export REMOTE_USER="morphone"


#DIRECTORIES
export ASPLOS_FOLDER="/home/marc/asplos"
export WORKSPACE_FOLDER="/home/marc/asplos/workspace"
export STATE_FOLDER="/home/marc/asplos/workspace/state"
export DATA_TESTS_FOLDER="/home/marc/asplos/workspace/tests"
export TESTS_LAUNCHERS_FOLDER="/home/marc/asplos/workspace/launchers"
export MARC_SCRIPTS_FOLDER="/home/marc/asplos/workspace/scripts"
export WATTSUP_FOLDER="/home/marc/asplos/workspace/watts-up"
export VMS_FOLDER="/home/marc/asplos/workspace/vms"
export BACKUP_FOLDER="/home/marc/asplos/backup"
export XEMPOWER_FOLDER="/home/marc/xempower-asplos"

#FILES
export STATE_FILE_NAME=$STATE_FOLDER"/test_state.obj"
export LIST_TESTS_FILE=$ASPLOS_FOLDER"/test_lists.txt"
export ENV_FILE=$STATE_FOLDER"/env.sh"
export LIST_LAUNCHER_FILES=$STATE_FOLDER"/lanchers_list.txt"
export LOG_TESTS_FILE="/var/log/asplos.log"
export WATTSUP_READER_SOURCE="wattsup_reader.c"

#SCRIPTS
export LAUNCHER_TEST=$MARC_SCRIPTS_FOLDER"/launch_test.py"
export TEST_WITH_WATTSUP="1"
export START_XENTRACE_4_PM_SCRIPT=$MARC_SCRIPTS_FOLDER"/start_xentrace_4_PMC.sh"
export START_XENTRACE_8_PM_SCRIPT=$MARC_SCRIPTS_FOLDER"/start_xentrace_8_PMC.sh"
export STOP_XENTRACE_SCRIPT=$MARC_SCRIPTS_FOLDER"/stop_xentrace.sh"
export WATTSUP=wattsup
export PARSE_DATA_4_PMC_SCRIPT=$MARC_SCRIPTS_FOLDER"/parse_data_4_PMC.sh"
export PARSE_DATA_8_PMC_SCRIPT=$MARC_SCRIPTS_FOLDER"/parse_data_8_PMC.sh"
export WATTSUP_START_LOG="echo '#L,W,3,I,,1;' > "
export WATTSUP_LOW_LOAD="echo '#L,W,3,E,,3600;' >  "
export WATTSUP_GET_DATA="echo '#D,R,0;' >  "
export WATTSUP_CLEAR="echo '#R,W,0;' >  "
export WATTSUP_READER=$MARC_SCRIPTS_FOLDER"/wattsup_reader"
export EMAIL_SCRIPT=$MARC_SCRIPTS_FOLDER"/email_sender.py"
export WATTUP_DATA_PARSER=$MARC_SCRIPTS_FOLDER"/wattsup_data_parser.py"
export WATTSUP_PARSER=$MARC_SCRIPTS_FOLDER"/parse_wattsup.sh"

#DOMAINS
export MARC_1_NAME="marc_0"
export MARC_2_NAME="marc_1"
export MARC_3_NAME="marc_2"
export MARC_4_NAME="marc_3"
export MARC_5_NAME="marc_4"
export MARC_6_NAME="marc_5"
export MARC_7_NAME="marc_6"
export MARC_8_NAME="marc_7"
export MARC_TRAIN_NAME="marc_training"
export MARC_PROBE_S1_NAME="probe_socket_1"
export MARC_PROBE_S2_NAME="probe_socket_2"


export MARC_1_FOLDER=$VMS_FOLDER"/marc_1"
export MARC_2_FOLDER=$VMS_FOLDER"/marc_2"
export MARC_3_FOLDER=$VMS_FOLDER"/marc_3"
export MARC_4_FOLDER=$VMS_FOLDER"/marc_4"
export MARC_5_FOLDER=$VMS_FOLDER"/marc_5"
export MARC_6_FOLDER=$VMS_FOLDER"/marc_6"
export MARC_7_FOLDER=$VMS_FOLDER"/marc_7"
export MARC_8_FOLDER=$VMS_FOLDER"/marc_8"
export MARC_TRAINING=$VMS_FOLDER"/marc_training"
export MARC_PROBE_S1=$VMS_FOLDER"/probe_socket1"
export MARC_PROBE_S2=$VMS_FOLDER"/probe_socket2"

export MARC_CFG_1=$MARC_1_FOLDER"/conf.cfg"
export MARC_CFG_2=$MARC_2_FOLDER"/conf.cfg"
export MARC_CFG_3=$MARC_3_FOLDER"/conf.cfg"
export MARC_CFG_4=$MARC_4_FOLDER"/conf.cfg"
export MARC_CFG_5=$MARC_5_FOLDER"/conf.cfg"
export MARC_CFG_6=$MARC_6_FOLDER"/conf.cfg"
export MARC_CFG_7=$MARC_7_FOLDER"/conf.cfg"
export MARC_CFG_8=$MARC_8_FOLDER"/conf.cfg"
export MARC_CFG_TRAINING=$MARC_TRAINING"/conf.cfg"
export MARC_CFG_PROBE_S1=$MARC_PROBE_S1"/conf.cfg"
export MARC_CFG_PROBE_S2=$MARC_PROBE_S2"/conf.cfg"


export MARC_IP_1="10.0.0.2"
export MARC_IP_2="10.0.0.3"
export MARC_IP_3="10.0.0.4"
export MARC_IP_4="10.0.0.5"
export MARC_IP_5="10.0.0.6"
export MARC_IP_6="10.0.0.7"
export MARC_IP_7="10.0.0.8"
export MARC_IP_8="10.0.0.9"
export MARC_IP_TRAINING="10.0.0.10"

#GENERIC VARIABLES
export WATTSUP_USB="/dev/ttyUSB0"
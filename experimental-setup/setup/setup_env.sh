#!/bin/bash

# Check if script is launched as root
if [ $(whoami) != "root" ]; then
    echo "ERROR - Run me as root"
    exit 1
fi

# Stop if sudo not installed
which sudo 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
	echo "WARNING - sudo command not found. Installing..."
	apt-get update && apt-get install sudo 
fi

if [[ -f "./environment.sh" ]]; then

	apt-get update && apt-get install -y python-dev build-essential gettext git wget curl \
						bin86 bcc iasl uuid uuid-dev libncurses-dev zlib1g-dev openssl  \
						xorg-dev libyajl-dev libaio-dev libglib2.0-dev libpixman-1-dev \
						bison flex cmake ocaml-nox ocaml-findlib markdown udev \
						pkg-config bridge-utils iproute libcrypto++-dev libssl-dev \
						colormake  libaio1 transfig libghc-zlib-dev checkpolicy libbz2-dev \
						liblzma-dev liblzo2-dev gcc-multilib g++-multilib texinfo bc python-yaml \
						screen exfat-fuse exfat-utils

	echo "------ save current working directory ------"
	CWD=$(pwd)

	echo "------ users setup ------"
	# Create user: marc
	echo "[*] Adding user: marc"
	adduser marc --disabled-password --gecos ""
	# Setup ssh
	echo "[*] Creating .ssh directory in marc user home"
	mkdir -p /home/marc/.ssh/
	echo "[*] Writing authorized_keys file in marc .ssh"
	echo "" >> /home/marc/.ssh/authorized_keys
	echo "    matt@macbook"
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPGJPGHjEo4z/0c0lcRFHir91DJLpKGrqVsGu2oyGLA1tL0i7w5/EbL/EvYN/0YjW0UiodxE+hX3maDBCQgImsPbCr+9Xz6bDPyNHaXCs5LAddZaUAQJLqC51EVsQAaXBH5TMfT1XpU0xJLRIkqqaTR0PSofnBblqxx2x3XCKqZmUiyqRTOkLukd/1tHuqp0QPBucaPc2WSpe5sH3dcLG/eYSzqfUX+QS735skDwnw9N3vpcnDZybPTEDv4JlWnrqhZBzrfdy+ruzj3H76JIG9XTlzjYH2NYxKhRyi4JBvt9/12MTcYtEOmZdk6VJNdha0tH4EdvLZPz9/HskCSmyR matt@macbook" >> /home/marc/.ssh/authorized_keys
	echo "" >> /home/marc/.ssh/authorized_keys
	echo "    andrea.corna.ac.91@gmail.com"
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDP6kCf+RB9TZm8VVPVFtE1tWIijzlf1qijAZOHn8tOujnASTo6cyHT/pgfHlCRyJjC5x8chSZNrMzkg6mMmksTRjz44GpVU5/j/vYyXP3abXI5tlSXz6Sa7pf37L466sF0Tf99yZoawQz+2YX+JfnaUpd7It4bjLD5ZXIK9NBq+7Qin6L9mEgbSP9iiRRmQL/Ivam1sDv8Z4hYVFjPRJty8lM7YHI7SiCjrGuGUBR1sb3dxy88i1JS8L0ZPyC3mnbj55kuwZ5I/zDv+wiJN5SElwZDhAxjQXa+k5E+YF423tLhO0jkBWDJ5XM4xmV5GSYEaZR7h7t8VwH8VnHEPQb/ andrea.corna.ac.91@gmail.com" >> /home/marc/.ssh/authorized_keys
	echo "" >> /home/marc/.ssh/authorized_keys
	echo "    andreadamiani@MacBook-Pro-di-Andrea.local"
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXBcGx/G5xshEVcE3aAU9jNZ0prQDgAvvO7Zq+G3k1GnIuTPRFo1y88pXG9Q7aw4o29JlLwQhJz0XMELyJ84t8Gs/vC+5l+UYFN95wPbbtwha6GnbpW38qry8VxeQZFDMyMhn3WS8OVW5YnsmkYh6OJPtMwOaOAem2wrsLIsHHG8rk8+Q9ublNsvbdi9vxe02oGvJV4X68x08VLtaFsfVYs0WzE1MKTV83s3H78p6D/WDfRIZWmWh4PrHRecRMff7xGmZI16GW3csh8f5lPmSG4U6+acsHTTv40YZmJyD7Y8+MmdpnhK5BPVvHBg0DaJuVbl/VFsxLlqOQs0WE77p9 andreadamiani@MacBook-Pro-di-Andrea.local" >> /home/marc/.ssh/authorized_keys
	echo "" >> /home/marc/.ssh/authorized_keys
	

	# Permissions&co.
	echo "[*] Setting permissions in .ssh directory of marc"
	chmod -R 700 /home/marc/.ssh/
	chown -R marc:marc /home/marc/.ssh/
	chmod 600 /home/marc/.ssh/authorized_keys
	echo "[*] Setting sudoers policy"
	mkdir -p /etc/sudoers.d/
	echo "marc	ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers.d/marc
	echo "" >> /etc/sudoers.d/marc
	echo "[*] Setting sudoers file permissions"
	chmod 440 /etc/sudoers.d/marc
	echo "[*] Setting dialout permission to users to write on WattsUp serial"
	usermod -a -G dialout marc
	usermod -a -G dialout root
	echo ""

	echo "------ Add alias for Xen management ------"
	echo "alias xen-configure=\"sudo ./configure --enable-stubdom\"" >> /home/marc/.bashrc 
	echo "alias xen-make-schedule=\"sudo colormake -j8 xen | grep 'schedule'\"" >> /home/marc/.bashrc
	echo "alias xen-make-world=\"sudo colormake -j8 world\"" >> /home/marc/.bashrc 
	echo "alias xen-install=\"sudo colormake install\"" >> /home/marc/.bashrc 
	echo "alias xen-install-config=\"sudo ldconfig -v\"" >> /home/marc/.bashrc 	
	echo "alias xen-start=\"sudo service xencommons restart\"" >> /home/marc/.bashrc 
	echo "alias xen-make-xempowermon=\"cd /home/marc/xempower/tools/xempowermon ; sudo colormake -j8; sudo colormake install\"" >> /home/marc/.bashrc 
	echo "alias asplos-training-create=\"sudo xl create \"\$MARC_CFG_TRAINING\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-marc1-create=\"sudo xl create \"\$MARC_CFG_1\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-marc2-create=\"sudo xl create \"\$MARC_CFG_2\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-marc3-create=\"sudo xl create \"\$MARC_CFG_3\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-marc4-create=\"sudo xl create \"\$MARC_CFG_4\"\"" >> /home/marc/.bashrc
	echo "alias asplos-marc5-create=\"sudo xl create \"\$MARC_CFG_5\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-marc6-create=\"sudo xl create \"\$MARC_CFG_6\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-marc7-create=\"sudo xl create \"\$MARC_CFG_7\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-marc8-create=\"sudo xl create \"\$MARC_CFG_8\"\"" >> /home/marc/.bashrc  
	echo "alias asplos-probe-s1-create=\"sudo xl create \"\$MARC_CFG_PROBE_S1\"\"" >> /home/marc/.bashrc 
	echo "alias asplos-probe-s2-create=\"sudo xl create \"\$MARC_CFG_PROBE_S2\"\"" >> /home/marc/.bashrc 


    echo "------ Git clone xempower-asplos repo ------"
    cd /home/marc
    sudo -u marc git clone git@bitbucket.org:necst/xempower-asplos.git
    sudo chown marc:marc -R /home/marc/xempower-asplos
    cd xempower-asplos
    sudo -u marc git checkout xempower-asplos

	echo "------ environment setup ------"
	cd $CWD
    source ./environment.sh
    sudo -u marc cat ./environment.sh >> /home/marc/.bashrc
    sudo touch /var/log/asplos.log
    sudo chmod 666 /var/log/asplos.log

	echo "------ folders setup ------"
	mkdir $ASPLOS_FOLDER
	mkdir $WORKSPACE_FOLDER
	mkdir $STATE_FOLDER
	mkdir $DATA_TESTS_FOLDER
	mkdir $TESTS_LAUNCHERS_FOLDER
	mkdir $MARC_SCRIPTS_FOLDER
	mkdir $BACKUP_FOLDER
	mkdir $XEMPOWER_FOLDER
	mkdir $VMS_FOLDER
	mkdir $MARC_1_FOLDER
	mkdir $MARC_2_FOLDER
	mkdir $MARC_3_FOLDER
	mkdir $MARC_4_FOLDER
	mkdir $MARC_5_FOLDER
	mkdir $MARC_6_FOLDER
	mkdir $MARC_7_FOLDER
	mkdir $MARC_8_FOLDER
	mkdir $MARC_TRAINING
	mkdir $MARC_PROBE_S1
	mkdir $MARC_PROBE_S2

	echo "----- copy vms probe -----"
	cp -pr ./probe_env/isolinux $MARC_PROBE_S1
	cp -pr ./probe_env/isolinux $MARC_PROBE_S2

	cp -p ./probe_env/initrd.img $MARC_PROBE_S1
	cp -p ./probe_env/initrd.img $MARC_PROBE_S2

	cp -p ./probe_env/vmlinuz64 $MARC_PROBE_S1
	cp -p ./probe_env/vmlinuz64 $MARC_PROBE_S2

	cp -p ./probe_env/conf_socket1.cfg $MARC_PROBE_S1/conf.cfg
	cp -p ./probe_env/conf_socket2.cfg $MARC_PROBE_S2/conf.cfg


	echo "------ wattsup setup ------"
	mkdir $WATTSUP_FOLDER
	git clone https://github.com/mattferroni/watts-up.git $WATTSUP_FOLDER
	cd $WATTSUP_FOLDER
	make
	sudo make install

	echo "------ Generating env file -----"
	echo "#!bin/sh" > $ENV_FILE
	cat $CWD/environment.sh >> $ENV_FILE
	chmod +x $ENV_FILE

	echo "----- copy scripts -----"
	cd $CWD
	cp -p ./setupfiles/email_sender.py $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/stop_xentrace.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/parse_data_4_PMC.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/parse_data_8_PMC.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/start_xentrace_4_PMC.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/start_xentrace_8_PMC.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/disable_wattsup_usage.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/enable_wattsup_usage.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/wattsup_data_parser.py $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/parse_wattsup.sh $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/launch_test.py $MARC_SCRIPTS_FOLDER
	cp -p ./setupfiles/launcher.sh $MARC_SCRIPTS_FOLDER

	cd ./setupfiles
	gcc -o wattsup_reader wattsup_reader.c
	chmod +x wattsup_reader
	cp -p wattsup_reader $MARC_SCRIPTS_FOLDER
	cd ..


	sudo chown marc:marc -R $ASPLOS_FOLDER

else
	echo "No environment.sh file found."
fi
import smtplib, sys, os

FROM = "marc@necst.it"
TO = ["andrea.corna.ac.91@gmail.com"]
SUBJECT = "[ASPLOS] "+sys.argv[1]
TEXT = sys.argv[2]
message = """\From: %s\nTo: %s\nSubject: %s\n\n%s""" % (FROM, ", ".join(TO), SUBJECT, TEXT)

def main():
        try:
                server = smtplib.SMTP("smtp.gmail.com", 587)
                server.ehlo()
                server.starttls()
                server.login("marc@necst.it", os.environ["MARC_PASS"])
                server.sendmail(FROM, TO, message)
                server.close()
                print 'successfully sent the mail'
        except:
                print "failed to send mail"
main()
'''
Script python to be used to parse output file from get_domain_run_time.sh script
Input logs time are considered to be in milli seconds

Author Andrea Corna - andrea.corna@mail.polimi.it
'''
import os, sys

if len(sys.argv) < 2:
        print "Please specify source file"
        exit(-1)


SOURCE_FILE = sys.argv[1]

if os.path.isfile(SOURCE_FILE) == False:
        print "The file does not exist"

with open(SOURCE_FILE,"r") as input:
        lines = input.readlines()

        START_TIME = 0
        END_TIME = 0
        previous_lenght = len(lines[0].split("-")[1].replace(" ",";").split(";"))
        for i in range(0,len(lines)):
                line = lines[i]
                data = line.split("-C")
                trace = data[1].replace(" ",";").split(";")
                if len(trace) > previous_lenght:
                        START_TIME = data[0]

                if len(trace) < previous_lenght:
                        break

                END_TIME = data[0]
                previous_lenght = len(trace)
        
        execution_time = float(END_TIME) - float(START_TIME)
        print 'Execution time {} {}'.format(str(execution_time), "ms")
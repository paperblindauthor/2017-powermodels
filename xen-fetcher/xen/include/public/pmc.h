/******************************************************************************
 * include/public/pmc.h
 * 
 * General values used to trace HPC in schedule.c
 * Andrea Corna - andrea.corna.ac.91@gmail.com, andrea.corna@mail.polimi.it
 */

/*UMASK EVENT SELECT*/
#define UMASK_00                   0x0000
#define UMASK_01                   0x0100
#define UMASK_02                   0x0200
#define UMASK_04                   0x0400
#define UMASK_08                   0x0800
#define UMASK_10                   0x1000
#define UMASK_11                   0x1100
#define UMASK_12                   0x1200
#define UMASK_20                   0x2000
#define UMASK_21                   0x2100
#define UMASK_40                   0x4000 
#define UMASK_41                   0x4100
#define UMASK_42                   0x4200
#define UMASK_4F                   0x4f00
#define UMASK_81                   0x8100
#define UMASK_82                   0x8200

/*RAPL COUNTERS ADDRESSES*/
#define MSR_RAPL_POWER_UNIT        0x606
#define MSR_PKG_ENERGY_STATUS      0x611
#define MSR_PP0_ENERGY_STATUS      0x639
#define MSR_PP1_ENERGY_STATUS      0x641
#define MSR_DRAM_ENERGY_STATUS     0x619
#define ENERGY_UNIT_MASK           0x1f
#define ENERGY_UNIT_SHIFT          8


/* PMC ADDRESSES */
#define IA32_MPERF_REG             0xe7
#define IA32_APERF_REG             0xe8
#define IA32_PERF_GLOBAL_CTRL      0x38f
#define IA32_PERF_FIXED_CTR_CTRL   0x38d
#define IA32_FIXED_CTR0            0x309
#define IA32_FIXED_CTR1            0x30a
#define IA32_FIXED_CTR2            0x30b
#define IA32_PMC0                  0xc1
#define IA32_PMC1                  0xc2
#define IA32_PMC2                  0xc3
#define IA32_PMC3                  0xc4
#define IA32_PMC4                  0xc5
#define IA32_PMC5                  0xc6
#define IA32_PMC6                  0xc7
#define IA32_PMC7                  0xc8
#define IA32_PERFEVTSEL0           0x186
#define IA32_PERFEVTSEL1           0x187
#define IA32_PERFEVTSEL2           0x188 
#define IA32_PERFEVTSEL3           0x189 
#define IA32_PERFEVTSEL4           0x18a
#define IA32_PERFEVTSEL5           0x18b
#define IA32_PERFEVTSEL6           0x18c
#define IA32_PERFEVTSEL7           0x18d

/*GENERAL VALUES*/
#define ZERO_VALUE                 0x00
#define ENABLE_PMC_0_3             0x0f
#define ENABLE_PMC_4_7             0xf0
#define ENABLE_CTR_0_2             0x07   
#define ENABLE_CTR_0_2_USER_MODE   0x222 


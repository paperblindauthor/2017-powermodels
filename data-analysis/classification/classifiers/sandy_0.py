#!/usr/bin/env python

'''
ASPLOS Classifier

Parses a HPC (UN_CORE_CYCLE, LLC_REF, L1_HIT) + RAPL trace, generating
1. Power classes 	[edit KDE_VALUES for boundaries]
2. HPC classes 		[edit CLASSIFIER for classification routine]

Computes missclassification stats in the end.
'''

from scipy import stats
from scipy.signal import argrelextrema
import pandas as pd
import numpy as np
import sys,os.path

if len(sys.argv) < 4:
	print "ERROR - Please specify source file, time column and rapl column"
	exit(-1)

if os.path.exists(sys.argv[1]) == False:
    print "ERROR - The file specified does not exist"
    exit(-1)

'''
EDIT TO CHANGE CLASSIFIER
'''
KDE_VALUES = [44809, 52164]

def CLASSIFIER(row):
	l1_hit			= row["L1_HIT"]
	inst_ret        = row["INST_RET"]

	if 0 <= inst_ret and inst_ret < 1167500000:
		return 0
	elif 1167500000 <= inst_ret and inst_ret < 2824620000:
		if 0 <= l1_hit and l1_hit < 193920000:
			return 1
		elif 193920000 <= l1_hit and l1_hit < 394630000:
			return 2
		else:
			return 1
	elif 2824620000 <= inst_ret and inst_ret < 4745300000:
		return 1
	else:
		return 2

'''
BEGIN OF SCRIPT
'''
path = sys.argv[1]
time_col = sys.argv[2]
power_column = sys.argv[3]

data = pd.read_csv(path, sep=',', header=0).dropna()

data.replace('', np.nan, inplace=True)
data.dropna(inplace=True)

# data['Dt'] = data[time_col].diff()
# data['Dy'] = data[rapl_col].diff()

# power_column = '__POWER__'

# data[power_column] = data['Dy'] / data['Dt']

# data = data.drop('Dt', axis=1).drop('Dy', axis=1)

data.replace('', np.nan, inplace=True)
data.dropna(inplace=True)

def oracle(row):
	value = row[power_column]
	power_class = 0
	for item in KDE_VALUES:
		if value > (item / 1000):
			power_class = power_class + 1
		else:
			break
	
	return power_class

data['power_class'] = data.apply(oracle, axis=1)

def compute_factory(min, max, steps):
	return (lambda value: min + (((max - min) / steps) * value))


data['class'] = data.apply(CLASSIFIER, axis=1)

data.to_csv('./CLASSIFIED.csv', sep=',', index=False, mode='w+')

classifications = {}
rows = data.iterrows()

for val in np.unique(data['power_class']):
	classifications[val] = {
		'miss': 0,
		'ok': 0
	}

for i, row in rows:
	clazz = row['power_class']
	if clazz == row['class']:
		classifications[row['power_class']]['ok'] = classifications[row['power_class']]['ok'] + 1
	else:
		classifications[row['power_class']]['miss'] = classifications[row['power_class']]['miss'] + 1

tot_ok = 0
tot_miss = 0

print "Classification report:"
print "----------------------"
for key, value in classifications.iteritems():
	print "CLASS " + str(key)
	tot_ok = tot_ok + value['ok']
	tot_miss = tot_miss + value['miss']
	missed = float(value['miss']) / (value['miss'] + value['ok'])
	print "misclassification: " + str(missed * 100) + '% (missed: ' + str(value['miss']) + ')'

missed = float(tot_miss) / (tot_miss + tot_ok)
print "OVERALL misclassification: " + str(missed * 100) + '% (missed: ' + str(tot_miss) + ')'
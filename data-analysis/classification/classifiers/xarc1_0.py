#!/usr/bin/env python

'''
ASPLOS Classifier

Parses a HPC (INST_RET, MEM_LOAD_UOPS_RET_L1_HIT) + WATTSUP POWER trace, generating
1. Power classes 	[edit KDE_VALUES for boundaries]
2. HPC classes 		[edit CLASSIFIER for classification routine]

Computes missclassification stats in the end.
'''

from scipy import stats
from scipy.signal import argrelextrema
import pandas as pd
import numpy as np
import sys,os.path

if len(sys.argv) < 3:
	print "ERROR - Please specify source file and wattsup power column"
	exit(-1)

if os.path.exists(sys.argv[1]) == False:
    print "ERROR - The file specified does not exist"
    exit(-1)

'''
EDIT TO CHANGE CLASSIFIER
'''
KDE_VALUES = [42709, 57165]

def CLASSIFIER(row):
	inst_ret        = row["INST_RET"]
	l1_hit			= row["MEM_LOAD_UOPS_RET_L1_HIT"]

	if 0 <= inst_ret and inst_ret < 1235000000:
		return 0
	elif 1235000000 <= inst_ret and inst_ret < 3610000000:
		if 0 <= l1_hit and l1_hit < 236362000:
			return 1
		elif 236362000 <= l1_hit and l1_hit < 567200000:
			return 2
		else:
			return 1
	elif 3610000000 <= inst_ret and inst_ret < 5580000000:
		return 1
	else:
		return 2

'''
BEGIN OF SCRIPT
'''
path = sys.argv[1]
power_col = sys.argv[2]

data = pd.read_csv(path, sep=',', header=0).dropna()

data.replace('', np.nan, inplace=True)
data.dropna(inplace=True)

def oracle(row):
	value = row[power_col]
	power_class = 0
	for item in KDE_VALUES:
		if value > (item / 1000):
			power_class = power_class + 1
		else:
			break
	
	return power_class

data['power_class'] = data.apply(oracle, axis=1)

def compute_factory(min, max, steps):
	return (lambda value: min + (((max - min) / steps) * value))


data['class'] = data.apply(CLASSIFIER, axis=1)

data.to_csv('./CLASSIFIED.csv', sep=',', index=False, mode='w+')

classifications = {}
rows = data.iterrows()

for val in np.unique(data['power_class']):
	classifications[val] = {
		'miss': 0,
		'ok': 0
	}

for i, row in rows:
	clazz = row['power_class']
	if clazz == row['class']:
		classifications[row['power_class']]['ok'] = classifications[row['power_class']]['ok'] + 1
	else:
		classifications[row['power_class']]['miss'] = classifications[row['power_class']]['miss'] + 1

print "Classification report:"
print "----------------------"

tot_ok = 0
tot_miss = 0

for key, value in classifications.iteritems():
	print "CLASS " + str(key)
	tot_ok = tot_ok + value['ok']
	tot_miss = tot_miss + value['miss']
	missed = float(value['miss']) / (value['miss'] + value['ok'])
	print "misclassification: " + str(missed * 100) + '% (missed: ' + str(value['miss']) + ')'

missed = float(tot_miss) / (tot_miss + tot_ok)
print "OVERALL misclassification: " + str(missed * 100) + '% (missed: ' + str(tot_miss) + ')'
from scipy import stats
from scipy.signal import argrelextrema
import pandas as pd
import numpy as np
import sys,os.path

path = sys.argv[1]
data_file = path + "/combined.csv"
rapl_file = path + "/rapl_wattsup.csv"

list_columns_data = ['time','dom','UNCLOCK_CYCLE_CORE','INST_RET','LLC_REF','MEM_LOAD_UOPS_RET_L1_HIT','ctr1','ctr2','ctr3','wattsup_power','wattsup_energy']
list_columns_rapl = ['rapl_pkg','rapl_pp0','rapl_pp1','rapl_dram']

dictionary = dict()
dictionary['MEM_LOAD_UOPS_RET_L1_HIT'] = 'L1_HIT'

data_bench = pd.read_csv(data_file, sep=',', header=0).dropna()
data_bench.replace('', np.nan, inplace=True)
data_bench.dropna(inplace=True)

data_rapl = pd.read_csv(rapl_file, sep=',', header=0).dropna()
data_rapl.replace('', np.nan, inplace=True)
data_rapl.dropna(inplace=True)

new_data = pd.DataFrame()

for item in list_columns_data:
	if item in dictionary:
		new_data[dictionary[item]] =  data_bench[item]
	else:
		new_data[item] =  data_bench[item]

for item in list_columns_rapl:
	new_data[item] =  data_rapl[item]

new_data.to_csv('./CLASSIFIED_SPECIFIC.csv', sep=',', index=False, mode='w+')
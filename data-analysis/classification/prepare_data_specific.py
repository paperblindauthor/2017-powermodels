import socket,os,time,logging,subprocess,sys,tarfile,shutil,yaml
from datetime import datetime, timedelta
from bash_executor import BashExecutor
from email_sender import EmailSender


LOG_FILE = "./classification.log"
NOW = time.strftime("%Y-%m-%d")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(LOG_FILE)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)
sh.setFormatter(formatter)
logger.addHandler(sh)

#base structure
 # one folder for test

def main():
	logger.info("Loading test configuration..")

	configuration_file_path = os.path.dirname(os.path.abspath(__file__))+'/config.yml'

	if os.path.isfile(configuration_file_path) is False:
		logger.error("Configuration file {} not found".format(configuration_file_path))
		exit(-1)

	with open(configuration_file_path, 'r') as f:
		config = yaml.load(f)


	SOURCE_FOLDER = "{}/{}".format(os.path.dirname(os.path.abspath(__file__)),config['SOURCE_FOLDER'])
	DOMAIN_START_INDEX = int(config['DOMAIN_START_INDEX'])
	BACKUP_FOLDER = config['BACKUP_FOLDER']

	CLASSIFIER_SCRIPT = "{}/classifiers/{}".format(os.path.dirname(os.path.abspath(__file__)),config['CLASSIFIER_SCRIPT'])
	POWER_COLUMN_NAME = config['POWER_COLUMN_NAME']
	TIME_COLUMN_NAME = config['TIME_COLUMN_NAME']
	TRAINING_PERCENTAGE = float(config['TRAINING_PERCENTAGE'])
	REMOTE_SCP_FOLDER = config['REMOTE_SCP_FOLDER']
	FILE_KEY_MARC = "{}/{}".format(os.path.dirname(os.path.abspath(__file__)),config['FILE_KEY_MARC'])
	FILE_KEY_MARC_REMOTE = config['FILE_KEY_MARC_REMOTE']
	CURRENT_MACHINE = config['CURRENT_MACHINE']
	HT_ENABLED = config['HT_ENABLED']
	
	sender = EmailSender(["andrea.corna.ac.91@gmail.com"])

	bash_executor = BashExecutor(LOG_FILE)
	
	logger.info("Starting processing...")
	try:
		os.chdir(SOURCE_FOLDER)
		
		logger.info("Loading files list...")
		onlyfiles = [os.path.join(SOURCE_FOLDER, f) for f in os.listdir(SOURCE_FOLDER) if os.path.isfile(os.path.join(SOURCE_FOLDER, f))]
		files_to_upload = [] 
		for file in onlyfiles:
			if ".tar.gz" in file:
				directory = file.replace(".tar.gz","")
				cmd = "mkdir {}".format(directory)
				bash_executor.execute(cmd)
				cmd = "tar -xvf {} -C {}".format(file,directory)
				bash_executor.execute(cmd)
				cmd = "rm {}".format(file)
				bash_executor.execute(cmd)
				
				logger.info("Current directory: {}".format(directory))
				#os.chdir(directory)
				#get test name
				test_name = directory.split("/")[-1]
				list_tests = []

				
				logger.info("Omogeneous test: {}".format(test_name))
				#case omogeneous test
				components = test_name.split("_")
				print components
				last_underscore_index = test_name.rfind("_")
				test_name_no_date = test_name[:last_underscore_index]
				if "page" in components[0]:
					benchmark_name = components[0] + "_" + components[1]
					total_tests = int(components[2]) + int(components[3])
				else:
					benchmark_name = components[0]
					total_tests = int(components[1]) + int(components[2])
				for i in range(0,total_tests):
					list_tests.append("{}".format(benchmark_name))

				logger.info("Compute list of domains")

				print list_tests

				files_to_be_analized = []

				logger.info("Check all domains files are present")

				lower_bound = DOMAIN_START_INDEX
				upper_bound = int(len(list_tests)+DOMAIN_START_INDEX)

				logger.info("File indexes from {} to {}".format(lower_bound,upper_bound))
				for index in range(lower_bound,upper_bound):

					file_name = "{}/{}.csv".format(directory,index)
					#check file exist
					if os.path.isfile(file_name) is False:
						logger.error("File {} not found. Please check".format(file_name))
						raise ValueError('No domain file {} found'.format(file_name))
					else:
						#change name from index to benchmark
						test_index = index-DOMAIN_START_INDEX
						new_file_name = "{}/{}".format(directory,"{}{}.csv".format(list_tests[test_index],test_index))
						cmd = "mv {} {}".format(file_name,new_file_name)
						bash_executor.execute(cmd)
						files_to_be_analized.append(new_file_name)

				logger.info("Check completed")

				#here all domain file are changed from index to name benchmark

				logger.info("Phase name correction completed. Now start classification")
				for file in files_to_be_analized:
					logger.info("Starting classification of {}".format(file))
					classification_script_cmd = "python {} {} {} {}".format(CLASSIFIER_SCRIPT,file,TIME_COLUMN_NAME,POWER_COLUMN_NAME)
					bash_executor.execute(classification_script_cmd)
					logger.info("Classification performed of {}".format(file))

					info = "{}ON{}".format(CLASSIFIER_SCRIPT.split("/")[-1].split("_")[0],CURRENT_MACHINE)
					print info
					classified_file = file.replace(".csv","") + "-SEP-{}-SEP-classified-{}-SEP-{}-SEP-{}.csv".format(test_name_no_date,info,HT_ENABLED,POWER_COLUMN_NAME)
					mv_cmd = "mv ./CLASSIFIED_SPECIFIC.csv {}".format(classified_file)
					bash_executor.execute(mv_cmd)

					all_file = "{}-SEP-all.csv".format(classified_file.replace(".csv",""))
					copy_cmd = "cp {} {}".format(classified_file,all_file)
					bash_executor.execute(copy_cmd)

					logger.info("Create local train and test set")
					with open(all_file,"r") as input:
						content = input.readlines()

						labels = content[0]

						rows_training = int(TRAINING_PERCENTAGE * len(content))
						training_file_name = "{}-SEP-training.csv".format(classified_file.replace(".csv",""))
						testing_file_name = "{}-SEP-testing.csv".format(classified_file.replace(".csv",""))

						logger.info("Writing training file {}".format(training_file_name))
						with open(training_file_name,"w+") as output:

							for index in range(0,rows_training):
								output.write(content[index].strip("\n")+"\n")

						logger.info("Writing testing file {}".format(training_file_name))
						with open(testing_file_name,"w+") as output:

							output.write(labels.strip("\n")+"\n")

							for index in range(rows_training,len(content)):
								output.write(content[index].strip("\n")+"\n")
								
						files_to_upload.append(all_file)
						files_to_upload.append(training_file_name)
						files_to_upload.append(testing_file_name)

					#here in folder there are:
					#	benchName_testname_classified_$MACHINE_$HTENABLE_$powercol_all_.csv
					#	benchName_testname_classified_$MACHINE_$HTENABLE_$powercol_training.csv
					#	benchName_testname_classified_$MACHINE_$HTENABLE_$powercol_testing.csv

		logger.info("Creating list of files to be upload...")
		plain_list_files_to_upload = ""
		marc_keys = []
		for item in files_to_upload:
			plain_list_files_to_upload = item + " " + plain_list_files_to_upload
			marc_keys.append(item.split("/")[-1])

		with open(FILE_KEY_MARC,"a+") as key_file:
			for key in marc_keys:
				key_file.write("{}\n".format(key.strip("\n")))

		mv_cmd = "cp {} {}".format(plain_list_files_to_upload,BACKUP_FOLDER)
		bash_executor.execute(mv_cmd)

		mv_cmd = "cp {} {}".format(FILE_KEY_MARC,BACKUP_FOLDER)
		bash_executor.execute(mv_cmd)

		copy_cmd = "cp {} {}".format(plain_list_files_to_upload,"../tmp")
		bash_executor.execute(copy_cmd)

		compress_cmd = "tar -czvf to_load.tar.gz ../tmp"
		bash_executor.execute(compress_cmd)

		scp_cmd = "scp {} {}".format("to_load.tar.gz",REMOTE_SCP_FOLDER)
		bash_executor.execute(scp_cmd)

		scp_key_file = "scp {} {}".format(FILE_KEY_MARC,FILE_KEY_MARC_REMOTE)
		bash_executor.execute(scp_key_file)

		rm_key_file = "rm {}".format(FILE_KEY_MARC)
		bash_executor.execute(rm_key_file)
		

		sender.send("Data Classification","Data are completed classified")
	except Exception, e:
		logger.error(e)
		sender.send("Data Classification","Error in classifing data\n {}".format(e))
		

main()
		


















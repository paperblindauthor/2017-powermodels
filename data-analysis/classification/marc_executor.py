import socket,os,time,logging,subprocess,sys,tarfile,shutil,yaml
from datetime import datetime, timedelta
from bash_executor import BashExecutor
from email_sender import EmailSender


LOG_FILE = "./marc_executor.log"
NOW = time.strftime("%Y-%m-%d")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(LOG_FILE)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)
sh.setFormatter(formatter)
logger.addHandler(sh)

def main():
	logger.info("Loading test configuration..")

	configuration_file_path = os.path.dirname(os.path.abspath(__file__))+'/config.yml'

	sender = EmailSender(["andrea.corna.ac.91@gmail.com"])

	bash_executor = BashExecutor(LOG_FILE)

	try:
		if os.path.isfile(configuration_file_path) is False:
			logger.error("Configuration file {} not found".format(configuration_file_path))
			raise ValueError('No configuration file found')

		with open(configuration_file_path, 'r') as f:
			config = yaml.load(f)

		FILE_KEY_MARC = config['FILE_KEY_MARC']

		EXECUTOR_ACTOR_COMMAND = config['EXECUTOR_ACTOR_COMMAND']

		loader_cmd = "{} {}".format(EXECUTOR_ACTOR_COMMAND,FILE_KEY_MARC)
		bash_executor.execute(loader_cmd)

		sender.send("Marc Uploader","Data are completely load in MARC")
	except Exception, e:
		sender.send("Marc Uploader","Error in loading data\n {}".format(e))

main()
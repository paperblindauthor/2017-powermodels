#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

mkdir tests

sudo apt-get update && sudo apt-get -y install python-dev python-virtualenv \
										python-pip python-yaml python-numpy \
										python-scipy python-pandas


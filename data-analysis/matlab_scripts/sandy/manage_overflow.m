function [array_without_overflow] = manage_overflow(source)
    counter = 0;
    array_without_overflow(:,1) = source(:,1);
    array_without_overflow(1,2) = 0;
    for i=2:length(source(:,2))
        if source(i,2)>=source(i-1,2),
            array_without_overflow(i,2)=source(i,2)-source(i-1,2)+counter;
        else
            array_without_overflow(i,2)=source(i,2)-source(i-1,2)+(2^32)-1+counter;
        end
        counter = array_without_overflow(i,2);
    end
    array_without_overflow(:,2)=array_without_overflow(:,2);

end
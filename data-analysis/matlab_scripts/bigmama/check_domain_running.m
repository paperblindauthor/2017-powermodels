function [boolean_result] = check_domain_running(pmc_struct,time)
   counter = 0;
   for i=1:length(pmc_struct)
       counter = pmc_struct(i).pmc_ts.data(time) + counter;
   end
   if(counter == 0)
       boolean_result = false;
   else
       boolean_result = true;
   end
end